// This file is part of the CWA toolbox
// Copyright (C) 2014 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function builder_c()
  src_c_path = get_absolute_file_path("builder_c.sce");
  src_files_wfdb=["annot.c", "signal.c","wfdbio.c"];
  inc_files_wfdb=["ecgcodes.h"  "ecgmap.h"  "wfdb.h"  "wfdblib.h"];
  src_files_levkov=["levkov.c","levkov_f.c"];
  src_files_ecgsyn="ecgsyn_ode.c"
  src_files_lomb="fasper.c"
  src_files_SampEn="sampEn.c"
  src_files=[src_files_wfdb src_files_levkov src_files_lomb src_files_SampEn];
  inc_files=inc_files_wfdb;
  CFLAGS = ilib_include_flag(src_c_path)
  if newest(src_c_path+['date_build','builder_c.sce',src_files,inc_files src_files_ecgsyn])<>1 then
    curdir=pwd();
    try
      if getos()=="Windows" then
	libs=SCI+"/bin/fileio";
      else
	libs=[]
      end	
      tbx_build_src("CWA", src_files,"c",src_c_path,libs,"",CFLAGS);
      tbx_build_src("ecgsyn_ode",src_files_ecgsyn,"c",src_c_path,[],"","","","","","loader_ecgsyn.sce")
      mputl(sci2exp(getdate()),src_c_path+'date_build');
    catch
      cd(curdir);
      mprintf("%s\n",lasterror());
    end           
  end

endfunction

builder_c();
clear builder_c; // remove builder_c
