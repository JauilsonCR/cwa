#include <math.h>
void ecgsyn_ode(int *n,double *t,double *x,double *dxdt)
{
  double twopi=6.28318530718;
  double ta,a0,zbase;
  double w0=x[*n];
  double aresp=x[*n+1];
  double fresp=x[*n+2];
  double *ti=x+*n+3;
  double *ai=ti+5;
  double *bi=ai+5;
  double dti; 
  double two=2.0;
  int k;
   
  ta = atan2(x[1],x[0]);
  a0 = 1.0 - sqrt(pow(x[0],two) + pow(x[1],two));

  dxdt[0]=a0*x[0]-w0*x[1];
  dxdt[1]=w0*x[0]+a0*x[1];
  dxdt[2]=-x[2]+aresp*sin(twopi*fresp*(*t));
  for (k=0;k<5;k++) {
    dti = fmod(ta - ti[k],twopi);
    dxdt[2]=dxdt[2]-ai[k]*dti*exp(-0.5*pow(dti/bi[k],two));
  }
}
