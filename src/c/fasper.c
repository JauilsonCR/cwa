/* file: lomb.c		G. Moody	12 February 1992
   Last revised:	  27 July 2010
   -------------------------------------------------------------------------------
   lomb: Lomb periodogram of real data
   Copyright (C) 1992-2010 George B. Moody

   This program is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free Software
   Foundation; either version 2 of the License, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
   PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   this program; if not, write to the Free Software Foundation, Inc., 59 Temple
   Place - Suite 330, Boston, MA 02111-1307, USA.

   You may contact the author by e-mail (george@mit.edu) or postal mail
   (MIT Room E25-505A, Cambridge, MA 02139 USA).  For updates to this software,
   please visit PhysioNet (http://www.physionet.org/).
   _______________________________________________________________________________

   The default input to this program is a text file containing a sampled time
   series, presented as two columns of numbers (the sample times and the sample
   values).  The intervals between consecutive samples need not be uniform (in
   fact, this is the most significant advantage of the Lomb periodogram over other
   methods of power spectral density estimation).  This program writes the Lomb
   periodogram (the power spectral density estimate derived from the input time
   series) on the standard output.

   The original version of this program was based on the algorithm described in
   Press, W.H, and Rybicki, G.B., Astrophysical J. 338:277-280 (1989).  It has
   been rewritten using the versions of this algorithm presented in Numerical
   Recipes in C (Press, Teukolsky, Vetterling, and Flannery;  Cambridge U. Press,
   1992).

   This version agrees with 'fft' output (amplitude spectrum up to the Nyquist
   frequency with total power equal to the variance);  thanks to Joe Mietus.
*/

#include <math.h>
#include <malloc.h>
#ifdef _MSC_VER
 #include <windows.h>
#endif

static long lmaxarg1, lmaxarg2;
#define LMAX(a,b) (lmaxarg1 = (a),lmaxarg2 = (b), (lmaxarg1 > lmaxarg2 ? \
						   lmaxarg1 : lmaxarg2))
static long lminarg1, lminarg2;
#define LMIN(a,b) (lminarg1 = (a),lminarg2 = (b), (lminarg1 < lminarg2 ? \
						   lminarg1 : lminarg2))
#define MOD(a,b)	while (a >= b) a -= b
#define MACC 4
#define SIGN(a,b) ((b) > 0.0 ? fabs(a) : -fabs(a))
static double sqrarg;
#define SQR(a) ((sqrarg = (a)) == 0.0 ? 0.0 : sqrarg*sqrarg)
#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr

void fasper(double *x, double *y, unsigned long n, double ofac, double hifac,  double *frq, double* power, unsigned long maxout,int *ierr)

{ 
  double *Wk1=NULL;
  double *Wk2=NULL;

  void avevar(), realft(), spread();
  unsigned long j, k, ndim, nfreq, nfreqt,nout;
  double ave, ck, ckk, cterm, cwt, den, df, effm, expy, fac, fndim, hc2wt,
    hs2wt, hypo, sterm, swt, var, xdif, xmax, xmin;
  *ierr=0;
   
  nout = (unsigned long)(0.5*ofac*hifac*n);
  nfreqt = (unsigned long)(ofac*hifac*n*MACC);
  nfreq = 64;
  while (nfreq < nfreqt) nfreq <<= 1;
  ndim = nfreq << 1;

  if ((Wk1 = (double *)malloc(ndim * sizeof(double))) == NULL ||
      (Wk2 = (double *)malloc(ndim * sizeof(double))) == NULL) {
    if (Wk1) free(Wk1);
    *ierr=1;
    return;
  }

  for (j = 0; j < ndim; j++) Wk1[j] = Wk2[j] = 0.0;

  avevar(y, n, &ave, &var);
  xmax = xmin = x[0];
  for (j = 1; j < n; j++) {
    if (x[j] < xmin) xmin = x[j];
    if (x[j] > xmax) xmax = x[j];
  }
  xdif = xmax - xmin;
  fac = ndim/(xdif*ofac);
  fndim = ndim;
  for (j = 1; j <= n; j++) {
    ck = (x[j-1] - xmin)*fac;
    MOD(ck, fndim);
    ckk = 2.0*(ck++);
    MOD(ckk, fndim);
    ++ckk;
    /* take care spread supposes second argument first element start at index 1*/
    spread(y[j-1] - ave, Wk1-1, ndim, ck, MACC);
    spread(1.0, Wk2-1, ndim, ckk, MACC);
  }
  /* take care realft supposes first argument first element start at index 1*/
  realft(Wk1-1, ndim, 1);
  realft(Wk2-1, ndim, 1);

  df = 1.0/(xdif*ofac);
  for (k = 2, j = 0; j < (nout); j++, k += 2) {
    hypo = sqrt(Wk2[k]*Wk2[k] + Wk2[k+1]*Wk2[k+1]);
    hc2wt = 0.5*Wk2[k]/hypo;
    hs2wt = 0.5*Wk2[k+1]/hypo;
    cwt = sqrt(0.5+hc2wt);
    swt = SIGN(sqrt(0.5-hc2wt), hs2wt);
    den = 0.5*n + hc2wt*Wk2[k] + hs2wt*Wk2[k+1];
    cterm = SQR(cwt*Wk1[k] + swt*Wk1[k+1])/den;
    sterm = SQR(cwt*Wk1[k+1] - swt*Wk1[k])/(n - den);
    Wk1[j] = (j+1)*df;
    Wk2[j] = (cterm+sterm)/(2.0*var);
  }
  for (j = 0; j < maxout; j++) {
    frq[j]=Wk1[j];
    power[j]=Wk2[j]/(nout/(2.0*var));
  }
  free(Wk1);
  free(Wk2);
}


void avevar(data, n, ave, var)
     double data[];
     unsigned long n;
     double *ave, *var;
{
  unsigned long j;
  double s, ep;

  for (*ave = 0.0, j = 0; j < n; j++) *ave += data[j];
  *ave /= n;
  *var = ep = 0.0;
  for (j = 0; j < n; j++) {
    s = data[j] - (*ave);
    ep += s;
    *var += s*s;
  }
  *var = (*var - ep*ep/n)/(n-1);
}

void spread(y, yy, n, x, m)
double y, yy[];
unsigned long n;
double x;
int m;
{
    int ihi, ilo, ix, j, nden;
    static int nfac[11] = { 0, 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880 };
    double fac;
    /* commented out because spread is called with m=MACC=4
       void error();
       if (m > 10)
     error("factorial table too small");
    */
   
    ix = (int)x;
    if (x == (double)ix) yy[ix] += y;
    else {
	ilo = LMIN(LMAX((long)(x - 0.5*m + 1.0), 1), n - m + 1);
	ihi = ilo + m - 1;
	nden = nfac[m];
	fac = x - ilo;
	for (j = ilo + 1; j <= ihi; j++) fac *= (x - j);
	yy[ihi] += y*fac/(nden*(x - ihi));
	for (j = ihi-1; j >= ilo; j--) {
	    nden = (nden/(j + 1 - ilo))*(j - ihi);
	    yy[j] += y*fac/(nden*(x - j));
	}
    }
}

void realft(data, n, isign)
double data[];
unsigned long n;
int isign;
{
    void four1();
    unsigned long i, i1, i2, i3, i4, np3;
    double c1 = 0.5, c2, h1r, h1i, h2r, h2i;
    double wr, wi, wpr, wpi, wtemp, theta;

    theta = 3.141592653589793/(double)(n>>1);
    if (isign == 1) {
	c2 = -0.5;
	four1(data, n>>1, 1);
    } else {
	c2 = 0.5;
	theta = -theta;
    }
    wtemp = sin(0.5*theta);
    wpr = -2.0*wtemp*wtemp;
    wpi = sin(theta);
    wr = 1.0 + wpr;
    wi = wpi;
    np3 = n+3;
    for (i = 2; i <= (n>>2); i++) {
	i4 = 1 + (i3 = np3 - (i2 = 1 + (i1 = i + i - 1)));
	h1r =  c1*(data[i1] + data[i3]);
	h1i =  c1*(data[i2] - data[i4]);
	h2r = -c2*(data[i2] + data[i4]);
	h2i =  c2*(data[i1] - data[i3]);
	data[i1] =  h1r + wr*h2r - wi*h2i;
	data[i2] =  h1i + wr*h2i + wi*h2r;
	data[i3] =  h1r - wr*h2r + wi*h2i;
	data[i4] = -h1i +wr*h2i + wi*h2r;
	wr = (wtemp = wr)*wpr - wi*wpi + wr;
	wi = wi*wpr + wtemp*wpi + wi;
    }
    if (isign == 1) {
	data[1] = (h1r = data[1]) + data[2];
	data[2] = h1r - data[2];
    } else {
	data[1] = c1*((h1r = data[1]) + data[2]);
	data[2] = c1*(h1r - data[2]);
	four1(data, n>>1, -1);
    }
}

void four1(data,nn,isign)
double data[];
unsigned long nn;
int isign;
{
    unsigned long n, mmax, m, j, istep, i;
    double wtemp, wr, wpr, wpi, wi, theta;
    double tempr, tempi;

    n = nn << 1;
    j = 1;
    for (i = 1; i < n; i += 2) {
	if (j > i) {
	    SWAP(data[j], data[i]);
	    SWAP(data[j+1], data[i+1]);
	}
	m = n >> 1;
	while (m >= 2 && j > m) {
	    j -= m;
	    m >>= 1;
	}
	j += m;
    }
    mmax = 2;
    while (n > mmax) {
	istep = mmax << 1;
	theta = isign*(6.28318530717959/mmax);
	wtemp = sin(0.5*theta);
	wpr = -2.0*wtemp*wtemp;
	wpi = sin(theta);
	wr = 1.0;
	wi = 0.0;
	for (m = 1; m < mmax; m += 2) {
	    for (i = m;i <= n;i += istep) {
		j = i + mmax;
		tempr = wr*data[j] - wi*data[j+1];
		tempi = wr*data[j+1] + wi*data[j];
		data[j] = data[i] - tempr;
		data[j+1] = data[i+1] - tempi;
		data[i] += tempr;
		data[i+1] += tempi;
	    }
	    wr = (wtemp = wr)*wpr - wi*wpi + wr;
	    wi = wi*wpr + wtemp*wpi + wi;
	}
	mmax = istep;
    }
}
