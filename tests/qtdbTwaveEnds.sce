mode(-1);
// This script compares the T wave end detections obtained using the
// Qinghua Zhang algorithm with the  T wave end annotations made by the
// first cardiologist over the qtdb physionet database
// (http://physionet.org/physiobank/database/qtdb) and generate
// statistics.
//
// This scripts requires  the qdtb database files (with extensions .dat,
// .hea and .q1c) have been previously downloaded in the tests/qtdb
// directory.
//
//The qt database can be downloaded 
//either using the Scilab script qtdbGet.sce which creates a qtdb
//  subdirectory where the  qtdbGet.sce file lies
//or using the shell instruction 
//  wget -r -np http://www.physionet.org/physiobank/database/qtdb/
//  described in http://www.physionet.org/physiotools/utilities/wget/
//  this instruction creates the hierarchy www.physionet.org/physiobank/database/qtdb/

// The original Qinghua Zhang Matlab function returns:
// Auto to Cardio1,3542 beats. Result: 1.4729+/-20.1056  weighted: 1.7211+/-20.8634
// Auto to Cardio1, unique set: 1.7211+/-41.2705

//This script returns
// Auto to Cardio1,3542 beats. Result: 1.46ms ±20.12ms weighted: 1.71ms ±20.88ms
// Auto to Cardio1, unique set:  1.71ms ±41.28ms
// using  S = filtbyfft(S-mean(S),fe, [0.5 250]); as pretreatment

// Auto to Cardio1,3542 beats. Result: 1.23ms ±20.00ms weighted: 1.54ms ±20.78ms
// Auto to Cardio1, unique set:  1.54ms ±41.02ms
// using S=ECGDetrend(S,fe,4);S=ECGSubstractPLI(S,fe,50,0.09); as pretreatment



datapath=get_absolute_file_path("qtdbTwaveEnds.sce");
if ~isdir(datapath+"qtdb/") then
  if exec(datapath+"qtdbGet.sce","errcatch")<>0 then
    error("Downloading qtdb database failed")
  end
end
datapath=datapath+"qtdb/"
dnames=gsort(strsubst(ls(datapath+"*.q1c"),".q1c",""),'g','i');
Nnames=size(dnames,'*');
meanvec1 = zeros(1,Nnames);
stdvec1 = zeros(1,Nnames);
ncycvec1 = zeros(1,Nnames);
Err=list();
kTe_ref=list();
kTe=list();

for kd=1:Nnames
  name = dnames(kd);
  if and(fileparts(name,"fname")<>['sel35','sel37']) then
    mprintf("n°%d: %s\n",kd,name)
    err1=ECGTwaveEnds_vs_annot(name,1,"q1c",%f,%f)
    err2=ECGTwaveEnds_vs_annot(name,2,"q1c",%f,%f)
    //select the channel which gives smallest error
    if norm(err1)<norm(err2) then
      Err(kd)=err1
    else
      Err(kd)=err2
    end
    //statistics
    meanvec(kd) = mean(Err(kd));
    stdvec(kd) = stdev(Err(kd));
    ncycvec(kd) = length(Err(kd));
  else
    Err(kd)=[];
  end
end
umean = mean(list2vec(Err));
ustd=stdev(list2vec(Err));

mmean = mean(meanvec);
mstd = mean(stdvec); 
wmean = sum(meanvec.*ncycvec)/sum(ncycvec);
wstd = sum(stdvec.*ncycvec)/sum(ncycvec);
ncyc = sum(ncycvec);
mprintf("Auto to Cardio1,%d beats. Result: %.2fms ±%.2fms weighted: %.2fms ±%.2fms\n",ncyc,mmean,mstd,wmean,wstd)
mprintf("Auto to Cardio1, unique set:  %.2fms ±%.2fms\n",umean,ustd)
Names=[];
for kd=1:Nnames
  Names=[Names;fileparts(dnames(kd),"fname")];
end

version_numbers = getversion('scilab')
new_datatypes=version_numbers(1:3)*[1;100;10000]>=5+500+10000
if new_datatypes then
function str=namedisplay(h),
  h.interp_mode ="off";
  h.background=addcolor([255 255 238]/255);
  c=h.parent;
  [m,i]=min(abs(c.data(:,1)-h.data(1)))
  str=c.display_function_data(i)
endfunction
else
 function str=namedisplay(c,pt,i),str=c.user_data.names(i);endfunction 
end
pause
clf;subplot(211),
plot(meanvec);c=gce();c=c.children;
ax=gca();ax.tight_limits="on";
ax.grid=ones(1,2)*color("lightgray");
ax.margins(4)=0.1;
ax.x_ticks.labels=emptystr(ax.x_ticks.labels);
ylabel("mean(err) (ms)")

if new_datatypes then
  c.display_function_data=Names;
  c.display_function="namedisplay";
else
  ud=struct();ud.names=Names;
  c.user_data=ud;
  datatipSetDisplay(c,namedisplay)
end
title("T wave end errors between annotations and detection")

subplot(212),
plot(stdvec);c=gce();c=c.children;
ax=gca();ax.tight_limits="on";
ax.margins(3)=0.01;
ax.margins(4)=0.18;
ax.grid=ones(1,2)*color("lightgray");
ylabel("stdev(err) (ms)")
xlabel("qtdb file index")
if new_datatypes then
  c.display_function_data=Names;
  c.display_function="namedisplay";
else
  ud=struct();ud.names=Names;
  c.user_data=ud;
  datatipSetDisplay(c,namedisplay)
end
datatipManagerMode('on')
