CurPath=get_absolute_file_path("testSPWVDpars3.sce");
// Create a frequency modulated signal

R=[];
C=[];
stacksize(2d8)

f_ech=4;//hz
t=0:0.25:600; //4hz sampling
N=size(t,'*');
tt=1:N;

 disp(N)
FMIN= [0.005 0.01 0.03 0.1];
Lpwl=[115 135 155 175 195 215 235];
Fwl= 71:2:81;
Twl= [17 27 37 57 77];
Nf=2^[5:9];
res=[];

for S=1:2
  select S
  case 1 then //low frequency
    Fmin=(0.09-0.01)/f_ech;Fmax=(0.09+0.01)/f_ech;

    [LF,IFreq_ref]=fmsin(N,Fmin,Fmax,150,0,(Fmin+Fmax)/2);
    IFreq_ref=IFreq_ref';
    // amplitude modulation
    IampLF=4;
    IAmp_ref=IampLF*ones(1,N);
    sig=IampLF*real(LF)';
  case 2 then
      Fmin=(0.25-0.01)/f_ech;Fmax=(0.25+0.01)/f_ech;
      [HF,IFreq_ref]=fmsin(N,Fmin,Fmax,80,0,(Fmin+Fmax)/2);
      IFreq_ref=IFreq_ref';
      // amplitude modulation
      k1=find(t<280);k1=k1($);
      k=find(305<=t&t<=340);k2=k(1);k3=k($);
      k4=find(t>=370);k4=k4(1);
      IampHF=[ones(1,k1-1),...
              linspace(1,0.1,k2-k1+1),...
              0.1*ones(1,k3-k2-1),...
              linspace(0.1,10,k4-k3+1) ...
              10*ones(1,N-k4)];
      sig=IampHF.*real(HF)';
      IAmp_ref=IampHF;
  end
  sig=sig+grand(1,N,"nor",0,0.5);
  for fmin=FMIN
    for lpwl=Lpwl
      LP=wfir("lp",lpwl,[fmin 0],"hm",[0 0]);
      clear options;
      options.lowpass=LP;
      for fwl=Fwl
        options.frequencywindowlength=fwl;
        for twl=Twl
          options.timewindowlength=twl;
          for nf=Nf
            options.frequencybins=nf;
            // Call the SPWVD demodulation
            [T,IFreq,IAmp,IPow,IDisp,dly]=TimeMoments(sig,options);
            // Take sampling frequency into account
            //IFreq=IFreq*f_ech;

            
            //scf(0);clf;plot(tt,IFreq_ref,'b',tt(1:$-dly),IFreq(dly+1:$),'r');
            //scf(1);clf;plot(tt,IAmp_ref ,'b',tt(1:$-dly),IAmp(dly+1:$) ,'r');
            //if S==2 then halt,end
            
            nd=100; //to remove head  for error computation
            sela=nd:N;
            if S==1 then
              self=nd:N; //to remove head  for error computation
            else
              self=[nd:305*f_ech 370*f_ech+nd:N];
            end
            ef=norm((IFreq(self(dly+1:$))-IFreq_ref(self(1:$-dly)))./IFreq_ref(self(1:$-dly)),'inf');
            ea=norm((IAmp (sela(dly+1:$))-IAmp_ref (sela(1:$-dly)))./IAmp_ref (sela(1:$-dly)),'inf');
            r=[S,fmin, lpwl, twl,  fwl, nf, ef,ea];
            res=[res; r];
            mprintf("S=%d;fmin=%.4f;lpwl=%d;twl=%d;fwl=%d;nf=%d;ef=%.4f;ea=%.4f\n",r);
          end
        end
      end
    end
  end

end
save(CurPath+'testSPWDpars3.dat',FMIN,Lpwl,Fwl,Twl,Nf,res)
