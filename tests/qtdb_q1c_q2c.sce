// This script compares the T wave end detections given by the two
// cardilogists over the qtdb physionet database
// (http://physionet.org/physiobank/database/qtdb) and generate
// statistics.
//
// This scripts requires  the qdtb database files (with extensions .dat,
// .hea, .q1c, .q2c) have been previously downloaded in the tests/qtdb
// directory (one can use the qtdbGet.sce script).

//cardio1 to Cardio2,402 beats. Result: -2.15ms ±22.42ms weighted: -5.84ms ±24.46ms
//cardio1 to Cardio2, unique set:  -5.84ms ±39.89ms
mode(-1);
datapath=get_absolute_file_path("qtdb_q1c_q2c.sce");

if ~isdir(datapath+"qtdb/") then
  if exec(datapath+"qtdbGet.sce","errcatch")<>0 then
    error("Downloading qtdb database failed")
  end
end
datapath=datapath+"qtdb/"
dnames=gsort(strsubst(ls(datapath+"*.q2c"),".q2c",""),'g','i');
Nnames=size(dnames,'*');
meanvec1 = zeros(1,Nnames);
stdvec1 = zeros(1,Nnames);
ncycvec1 = zeros(1,Nnames);
Err=list();

for kd=1:Nnames
  name = dnames(kd);
  [path, fname, extension] = fileparts(name)
  ann1=wfdbReadAnnotations(name,"q1c");
  ann2=wfdbReadAnnotations(name,"q2c");
  kRp_1=find(ann1(2,:)==1);
  kRp_2=find(ann2(2,:)==1);
  Rp_ann1=ann1(1,kRp_1);
  Rp_ann2=ann2(1,kRp_2);
  
  //the annotated T wave ends locations (index)
  kTe_1=find(ann1(2,:)==40&ann1(5,:)==2);
  kTe_2=find(ann2(2,:)==40&ann2(5,:)==2);

  Te_ann1=ann1(1,kTe_1);
  Te_ann2=ann2(1,kTe_2);
  if fname=="sel102" then
    Rp_ann1= Rp_ann1(1:3);
    Te_ann1=Te_ann1(1:2);
  elseif fname=="sel213" then
    Rp_ann1(27)=[];
    Te_ann1(27)=[];
    Te_ann1(52)=[];
  end
  Err(kd)=(Te_ann1-Te_ann2)*1000;
  meanvec(kd) = mean(Err(kd));
  stdvec(kd) = stdev(Err(kd));
  ncycvec(kd) = length(Err(kd));
end
umean = mean(list2vec(Err));
ustd=stdev(list2vec(Err));

mmean = mean(meanvec);
mstd = mean(stdvec); 
wmean = sum(meanvec.*ncycvec)/sum(ncycvec);
wstd = sum(stdvec.*ncycvec)/sum(ncycvec);
ncyc = sum(ncycvec);
mprintf("cardio1 to Cardio2,%d beats. Result: %.2fms ±%.2fms weighted: %.2fms ±%.2fms\n",ncyc,mmean,mstd,wmean,wstd)
mprintf("cardio1 to Cardio2, unique set:  %.2fms ±%.2fms\n",umean,ustd)
Names=[];
for kd=1:Nnames
  Names=[Names;fileparts(dnames(kd),"fname")];
end

version_numbers = getversion('scilab')
new_datatypes=version_numbers(1:3)*[1;100;10000]>=5+500+10000
if new_datatypes then
function str=namedisplay(h),
  h.interp_mode ="off";
  h.background=addcolor([255 255 238]/255);
  c=h.parent;
  [m,i]=min(abs(c.data(:,1)-h.data(1)))
  str=c.display_function_data(i)
endfunction
else
 function str=namedisplay(c,pt,i),str=c.user_data.names(i);endfunction 
end

clf;subplot(211),
plot(meanvec);c=gce();c=c.children;
ax=gca();ax.tight_limits="on";
ax.grid=ones(1,2)*color("lightgray");
ax.margins(4)=0.1;
ax.x_ticks.labels=emptystr(ax.x_ticks.labels);
ylabel("mean(err) (ms)")

if new_datatypes then
  c.display_function_data=Names;
  c.display_function="namedisplay";
else
  ud=struct();ud.names=Names;
  c.user_data=ud;
  datatipSetDisplay(c,namedisplay)
end
title("T wave end errors between q1c and q2c")

subplot(212),
plot(stdvec);c=gce();c=c.children;
ax=gca();ax.tight_limits="on";
ax.margins(3)=0.01;
ax.margins(4)=0.18;
ax.grid=ones(1,2)*color("lightgray");
ylabel("stdev(err) (ms)")
xlabel("qtdb file index")
if new_datatypes then
  c.display_function_data=Names;
  c.display_function="namedisplay";
else
  ud=struct();ud.names=Names;
  c.user_data=ud;
  datatipSetDisplay(c,namedisplay)
end
datatipManagerMode('on')
