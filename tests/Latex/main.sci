function main(name)
kTe=ECGTwaveEnds_vs_annot("tests/qtdb/"+name,1,"q1c",%f,%f);
clf();plot(kTe);
xgrid();ax=gca();ax.font_style=8;
xlabel("Annotation index");ylabel("distance:#samples");
title(name+":  distance between annotated and detetcted T wave ends");xs2pdf(0,"tests/Latex/img/"+name+".pdf")
endfunction
