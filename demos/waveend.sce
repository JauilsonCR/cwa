function waveend(S,fs,kRp)
  RR=kRp(2)-kRp(1);
  ptwin = ceil(0.016*fs)
  fratio = 4;
  swin = 0.128*fs;
  N=length(S);
  leftbound = kRp(1)+floor(0.089*sqrt(RR*fs)+0.139*fs);
  rightbound = kRp(1)+ceil(0.719*sqrt(RR*fs)-0.191*fs);
  clf;set(gcf(),"axes_size", [650,430]);
  t=(0:(size(S,'*')-1))/fs;
  plot(t,S);h=gce();L="ECG";H=[h.children];
  xlabel(_("Time (s)"))
  plot(t(kRp),S(kRp),'+r');h=gce();L=[L,"R peaks"];H=[H h.children];
  //plot([leftbound,rightbound],S([leftbound,rightbound]),'*g');
  ax=gca();
  ax.margins(3)=0.23;
  
  ymin=-300;
  ymax=400;
  
  //ax.zoom_box=[leftbound-swin*2 ymin rightbound+swin*2 ymax -1 1];

  count=0;
  xpoly(t(leftbound-swin+1)*ones(2,1),[min(S);max(S)]);l1=gce(); ...
      l1.foreground=3;L=[L,"Area bounds"];H=[H l1];  
  xpoly(t(leftbound)*ones(2,1),[min(S);max(S)]);l2=gce();l2.foreground=3;
  xpoly(t([1;N])',[0;0]);c=gce();c.foreground=3;
  xfpoly([]',[],5);e=gce();e.foreground=5;L=[L,"Area"]; ;H=[H e];
  xpoly([],[]);p=gce();p.foreground=22;L=[L,"Indicator function"];H=[H p];  

  leg=legend(H,L,'upper_caption')
  realtimeinit(0.1);
  E=[];
  for kT=leftbound:rightbound
    cutlevel = mean(S((kT-ptwin):(kT+ptwin)));
    count=count+1;
    realtime(count);
    drawlater()
    if E<>[] then delete(E);E=[];end
    l1.data(:,1)=t(kT-swin+1)*ones(2,1);
    l2.data(:,1)=t(kT)*ones(2,1);
    c.data(:,2)=[cutlevel;cutlevel];
    
    ki=find((S((kT-swin+1):kT)-cutlevel).*(S((kT-swin):kT-1)-cutlevel)< 0);
    ki=(kT-swin+1)+ki-1;
    
    if ki==[] then
      e.data=[t([kT-swin+1,(kT-swin+1):kT,kT]), [cutlevel;S((kT-swin+1):kT);cutlevel]]
    else
      ki=unique([kT-swin+1 ki kT]);
      k=1
      e.data=[t([ki(k),ki(k):(ki(k+1)-1),ki(k+1)-1])',[cutlevel;S(ki(k):(ki(k+1)-1));cutlevel]];
      for k=2:size(ki,'*')-1
        xfpoly(t([ki(k),ki(k):(ki(k+1)-1),ki(k+1)-1])',[cutlevel;S(ki(k):(ki(k+1)-1));cutlevel],5);
        ek=gce();ek.foreground=5;
        E=[E,ek];
      end
    end
    drawnow()
    corsig = S((kT-swin+1):kT) -  cutlevel;
    p.data=[p.data
            t(kT) sum(corsig)*2/100];
  end
  [mx,kmx]=max(p.data(:,2));
  [mn,kmn]=max(-p.data(:,2));
  if kmx<kmn  then // t'<t''
    leftind = kmx;
    rightind = kmn;
    leftdum = mx; // |A(t')|
    rightdum = mn; // |A(t'')|
  else //  t''<t'
    leftind = kmn;
    rightind = kmx;
    leftdum = mn; // |A(t'')|
    rightdum = mx;  //  |A(t')|
  end
  mthrld=5
  if leftdum>mthrld*rightdum   then
    // |A(t')| differs to |A(t'')|  ==> monophasic T wave
    kTe = leftind + leftbound - 1;
  else
    // |A(t')| is similar to |A(t'')| ==> biphasic T wave
    kTe = rightind+ leftbound - 1;
  end
  xpoly(t([kTe;kTe])',[min(S);max(S)]);te=gce();te.foreground=1;te.line_style=2;
  leg=legend([H te],[L,"Tend"],'upper_caption')

endfunction
s=[608.62;740.33;794.48;710.18;504.55;249.79;99.56;67.69;52.08;36.46;30.1;20.63;20.44;21.74;20.15;14.51;11.53;8.54;5.88;3.75;2.21;1.08;0.4;-0.03;-0.56;-1.29;-2.15;-3.26;-4.56;-5.95;-7.45;-9.04;-10.81;-12.74;-14.9;-17.4;-20.38;-23.75;-27.35;-31.09;-35.33;-40.2;-45.87;-52.45;-60.21;-68.99;-78.43;-88.19;-97.68;-105.52;-109.81;-108.3;-98.65;-77.89;-43.61;4.01;61.81;124.49;184.57;233.49;264.44;273.68;260.68;228.49;182.78;131.27;81.23;37.93;4.24;-19.24;-34.03;-42.59;-47.05;-49.32;-50.93;-52.62;-55.18;-58.75;-63.51;-69.32;-75.92;-82.7;-89.43;-95.76;-101.33;-105.9;-109.59;-112.3;-114.23;-115.59;-116.8;-117.63;-118;-117.92;-117.4;-116.47;-115.29;-113.69;-111.51;-108.93;-106.1;-103.12;-100.21;-97.4;-94.86;-92.5;-90.17;-88.34;-87.09;-85.94;-85.01;-84.61;-84.34;-84.12;-84.3;-84.64;-84.94;-84.9;-84.59;-84.03;-83.38;-82.73;-82.52;-82.33;-81.78;-81;-80.16;-78.8;-77.3;-76.09;-75.5;-75.54;-76.62;-78.33;-80.36;-81.9;-82.61;-81.75;-79.15;-74.7;-68.91;-62.47;-56.56;-51.98;-48.97;-47.74;-47.83;-48.6;-49.44;-49.93;-50.1;-49.92;-49.45;-49.33;-50.02;-50.93;-51.77;-52.21;-51.52;-49.57;-46.61;-43.03;-39.42;-36.11;-33.36;-31.08;-29.16;-27.39;-25.97;-24.46;-23.33;-22.69;-22.29;-22.24;-22.69;-23.02;-23.22;-23.54;-23.67;-23.46;-23.2;-22.82;-22.65;-22.93;-23.6;-24.07;-24.18;-23.71;-22.86;-22.22;-21.97;-22.15;-23.11;-24.78;-26.54;-28.45;-29.83;-29.8;-28.44;-26.02;-22.96;-20.55;-19.3;-19.51;-21.68;-26.17;-33.09;-42.17;-51.98;-60.54;-65.37;-64.82;-58.92;-49.01;-37.22;-26.13;-17.16;-11.49;-9.38;-9.65;-10.41;-9.79;-6.96;-1.21;6.92;15.16;21.74;27.27;31.63;34.64;37.1;39.06;40.48;40.89;39.83;38.5;37.99;39.53;40.6;38.91;24.89;-44.3;-186;-346.64;-384.15;-274.47;-102.22;101.02;314.16;503.4]:


fe=250;
krp=[2,size(s,'*')];
waveend(s,fe,krp);
