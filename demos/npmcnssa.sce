// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Tmax=90;
freq_sampling=20;
f0=0.5;
// Time instants
t=0:1/freq_sampling:Tmax;N=size(t,'*');
n=round(N/2)
//output signal
f1=1.2*f0;
sig1=[2*sin(2*%pi*f0*t(1:n)) sin(2*%pi*f1*t(n+1:$))]+2d-2*rand(t); 
// Input signal
f2=1.4*f0;
sig2=[sin(2*%pi*f0*t(1:n)) sin(2*%pi*f2*t(n+1:$))];//+2d-2*rand(t); 
// Draw results
f=scf(100001);clf;f.figure_name="Non stationary signal analysis";
demo_viewCode("npmcnssa.sce");


sl=300;
swl=5;
clear options
options.sectionlength=sl;
options.sectionstep=sl/2; //50% overlaping
options.smoothwindowlength=swl;
options.minimalcoherence=0.5;
frequency_bands=[0.5*f0 2.5*f0]/freq_sampling;

[energy,dispersion,inout_gains,inout_coherences]=np_mc_nss_analysis([sig1;sig2],frequency_bands,options);
//energy=energy*freq_sampling;
energy=energy*2;
nr=size(energy,2);
tr=(1:nr)*sl/(2*freq_sampling);
nplot=7;kplot=1;

//for comparison with expected values
reso=freq_sampling/sl/2;
section_window=window("hm",sl);
e11=sum((sig1(1:sl).*section_window)^2)
e12=sum((sig1(n+1:n+sl).*section_window)^2)
e21=sum((sig2(1:sl).*section_window)^2)
e22=sum((sig2(n+1:n+sl).*section_window)^2)


f.axes_size = [860,650];
g=addcolor([1 1 1]*0.94);
b=color('blue');
sda();
da=gda();
da.grid(1:2)=[g g];
da.tight_limits="on";
da.margins(4)=0.05;
o=0.05; //reserve space for title
h=(1-2*o)/nplot;
  


drawlater()
a=newaxes();a.axes_bounds=[0,o+(kplot-1)*h,1,h];
plot(t',[sig1',sig2'])
legend(["ouput","input"]);
ylabel("signals")
kplot=kplot+1;


a=newaxes();a.axes_bounds=[0,o+(kplot-1)*h,1,h];
plot(tr',energy');
plot([t(1),t(n) t(n) t($)]',[e11 e11 e12 e12;e21 e21 e22 e22]','--')

ylabel("energy")
a.data_bounds(:,2)=[0;250];
a.data_bounds(:,1)=[t(1);t($)];
kplot=kplot+1;

a=newaxes();a.axes_bounds=[0,o+(kplot-1)*h,1,h];
xfpoly([t(1),t(n) t(n) t($)  t($)  t(n) t(n) t(1)],[[f0 f0 f1 f1]-reso ...
                    [f1 f1 f0 f0]+reso]);
e=gce();e.background=g;e.foreground=g;
xfpoly([t(1),t(n) t(n) t($)  t($)  t(n) t(n) t(1)],[[f0 f0 f2 f2]-reso ...
                    [f2 f2 f0 f0]+reso]);
e=gce();e.background=g;e.foreground=g;

plot(tr',dispersion.fmin'*freq_sampling);

ylabel("fmin (Hz)")
a.data_bounds(:,2)=[0.4;0.9];
a.data_bounds(:,1)=[t(1);t($)];
kplot=kplot+1;

a=newaxes();a.axes_bounds=[0,o+(kplot-1)*h,1,h];
plot(tr',dispersion.pmin');
ylabel("dsp(fmin)")
a.data_bounds(:,2)=[0;45];
a.data_bounds(:,1)=[t(1);t($)];
kplot=kplot+1;

a=newaxes();a.axes_bounds=[0,o+(kplot-1)*h,1,h];
plot(tr',dispersion.dmin');
ylabel("disp(fmin)")
a.data_bounds(:,2)=[0;0.015];
a.data_bounds(:,1)=[t(1);t($)];
kplot=kplot+1;

a=newaxes();a.axes_bounds=[0,o+(kplot-1)*h,1,h];
plot(tr',inout_gains','m');
ylabel("inout gain");
a.data_bounds(:,2)=[0;20];
a.data_bounds(:,1)=[t(1);t($)];
kplot=kplot+1;

a=newaxes();a.axes_bounds=[0,o+(kplot-1)*h,1,h];
plot(tr',inout_coherences','m');
a.data_bounds(:,2)=[0;1.2];
a.data_bounds(:,1)=[t(1);t($)];
ylabel("coherence");
drawnow()
da.margins(4)=0.05;
sda();
