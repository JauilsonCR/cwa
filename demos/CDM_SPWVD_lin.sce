// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



//test with 
//  - Piecewise constant amplitude
//  - sinusoidal modulation frequency
//---------------------------------

//Create a frequency modulated signal
f0=11;Tmax=10;
freq_sampling=f0*55;//f0*55
fw=0.3; 

// Time instants
t=0:1/freq_sampling:Tmax;N=size(t,'*');

// Frequency modulation
ft=(f0+fw*(-1+2*t/Tmax)).*t

// Amplitude modulation
n1=round(N/3);
A=ones(1,n1);A(n1+1:2*n1)=2;A($+1:N)=1; 

// Input signal
sig=A.*cos(2*%pi*ft); 

//Instantaneous frequency : diff(ft,t)
IFreq_ref=2*(fw/Tmax)*t+f0+fw*(-1+2*t/Tmax);

BPdelay=0; //no band pass filter applied

// Frequency reference given by a signal
f_ref=cos(2*%pi*f0*t);



//use same lowpass filter for both methods to make the delays equal
LPwl=195; //Low pass filter window length 
LP=wfir("lp",LPwl,[0.01 0],"hm",[0 0]);
options=[];options.lowpass=LP;

// Call the complex demodulation
[IFreq_CDM,IAmp_CDM,IPhase,delay_CDM,f0e]=ComplexDemodulation(sig,f_ref,LP);
// Call the SPWVD demodulation
[T,IFreq_SPWVD,IAmp_SPWVD,IPow,IDisp,delay_SPWVD]=TimeMoments(sig,options);

delay=delay_SPWVD;
// Take sampling frequency into account
t=T/freq_sampling;
IFreq_CDM=IFreq_CDM*freq_sampling;
IFreq_SPWVD=IFreq_SPWVD*freq_sampling;


// Draw results
f=scf(100001);clf;f.figure_name="CDM_SPWVD";
demo_viewCode('CDM_SPWVD_lin.sce');

timebounds=[t(BPdelay+delay);t($)];

d1=BPdelay/freq_sampling;

drawlater();clf();

// Given signal
subplot(211);
plot(d1+t(delay+1:$),sig,"m",...
     t,IAmp_CDM,'b',...
     t,IAmp_SPWVD,'g');
a=gca();
a.data_bounds(:,2)=[-3;3];
a.data_bounds(:,1)=timebounds;
a.tight_limits="on";
a.margins(4)=0.2;
a.grid(1:2)=color("gray");
ylabel("Signal & IAmp");xlabel("t (s)")
legend(["signal","IAmp CDM","IAmp SPWVD"],"in_lower_right");

// Instantaneous frequency
subplot(212);
plot(d1+t(delay+1:$),IFreq_ref,"m",...
     t,IFreq_CDM,"b",...
     t,IFreq_SPWVD,'g');

a=gca();
a.margins(4)=0.2;
a.data_bounds(:,2)=[5;15];
a.data_bounds(:,1)=timebounds;
a.tight_limits="on";
ylabel("IF (Hz)");xlabel("t (s)")
a.grid(1:2)=color("gray");
legend(["Data" "CDM","SPWVD"],"in_lower_right");
drawnow()

