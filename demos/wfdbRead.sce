// This file is part of the wfdb toolbox
// Copyright (C) 2013 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//The twa00 signals and annotations comes from
//http://physionet.org/pn3/twadb/

record=CWApath()+"demos/datafiles/twa00"
a=wfdbReadAnnotations(record,"qrs",[0,30]);
[sig,fe,n]=wfdbReadSamples(record,%f,[0,30]);
[nvoie,nt]=size(sig);
t=(0:(nt-1))/fe;

f=scf(100001);clf;f.figure_name="twa00";
demo_viewCode('wfdbRead.sce');
plot(t',sig')
Na=size(a,2)
mx=max(sig(1,:));
drawlater()
plot(a(1,:),mx*ones(1,Na),'r+')
for k=1:Na
  xstring(a(1,:),mx,wfdbAnnot2String(a(2,:)))
end
drawnow()

clear sig t a s
