// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mode(-1)
subdemopath = get_absolute_file_path("CV_TimeFrequency.dem.gateway.sce");
subdemolist = [
    "Free breathing (RR&Vt)" "TF_free_breathing.sce"
    "decubitus_03 (RR&Vt)" "TF_decubitus_03.sce";
    "apnea  (RR&Vt)"  "TF_apnea.sce"];
subdemolist(:,2) = subdemopath + subdemolist(:,2);
clear subdemopath
