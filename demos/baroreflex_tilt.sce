// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//test with cardiovalscular RR signal
scriptfilename = "baroreflex_tilt.sce";
datapath   = get_absolute_file_path(scriptfilename);
datafile   = datapath+"DATA/tilt_divers";
Title      = "Tilt";
d=read(datafile,-1,5)';

freq_sampling=2;
shift=600;
RR= d(1,shift:$);
SBP=d(2,shift:$);

clear options
options.sectionlength=64;
fbands=[0.2 0.3
       0.04 0.15];
BPfbounds=[0.02 0.4];
BPflen=255;
result=Baroreflex_Analysis(RR,SBP,freq_sampling,fbands,BPfbounds,BPflen,options)
f=scf(100001);clf;f.figure_name=Title;
PlotBaroreflexAnalysis(result,"Tilt")
demo_viewCode(scriptfilename); 

