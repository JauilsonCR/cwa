// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//patient with controlled ergocycle

scriptfilename = "CDM_SPWVD_ergocycle.sce";
datapath   = get_absolute_file_path(scriptfilename);
datafile   = datapath+"DATA/cycle_spont";
Title      = "Ergocycle rrpa-8Hz";


d=read(datafile,-1,3);
freq_sampling=8;
RR=d(:,1);

//narrow band filter
filterlength=355;
frequency_bounds=[0.35 1.5];
f=scf(100001);clf;f.figure_name=Title;
res=CDM_SPWVD_Analysis(RR,0.7,freq_sampling,frequency_bounds,filterlength) 
PlotSpectralAnalysis(res,_("RR"),Title)

demo_viewCode(scriptfilename);


