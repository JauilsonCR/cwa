 CWA - Scilab Cardiovascular Waves Analysis Toolbox
===================================================

This toolbox replaces and extends the Cardiovascular Toolbox
(http://atoms.scilab.org/toolboxes/Cardiovascular)

Most of the Cardiovascular Toolbox functions are left unchanged
Functions added to manage ECG records and detections.

