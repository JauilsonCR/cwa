Cardiovascular Wave Analysis toolbox.

Most parts of this  toolbox are released under the terms of the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

The HRVAS tool for Scilab has been derived from the Matlab version
developped by 
    John T. Ramshur
    University of Memphis
    Department of Biomedical Engineering
    Memphis, TN
    jramshur@gmail.com
    jramshur@memphis.edu

    and is published under the GNU General Public License (see
    macros/HRVAS/Readme.txt for details)


The wavelet.sci file is derived from the Matlab version developped by
Christopher Torrence and Gilbert P. Compo (see macros/wavelet.sci
comments for details)
