<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2012 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xml:lang="en" xml:id="PlotSpectralAnalysis">
  <info>
    <pubdate>$LastChangedDate: 19-10-2011 $</pubdate>
  </info>
  <refnamediv>
    <refname>PlotSpectralAnalysis</refname>
    <refpurpose>Visualization of the data computed by the
    CDM_Analysis, SPWVD_Analysis or CDM_SPWVD_Analysis
    functions.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>PlotSpectralAnalysis(result [,InputNames [,Title]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>result</term>
        <listitem>
          The <literal>result</literal> data structure returned by
          <link linkend="CDM_Analysis">CDM_Analysis</link>, <link
          linkend="SPWVD_Analysis">SPWVD_Analysis</link> or <link
          linkend="CDM_SPWVD_Analysis">CDM_SPWVD_Analysis</link>
          functions.
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>InputNames</term>
        <listitem>
         a vector of one or two character strings that will be used to
         set the labels of the input signals. The default value is
         <literal>["RR","Vt"]</literal>.
        </listitem>
      </varlistentry>
     
      <varlistentry>
        <term>Title</term>
        <listitem>
         a character string that will be used to set the title of the graph.
        </listitem>
      </varlistentry>
      
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      <literal>PlotSpectralAnalysis</literal> plots the data computed
      by <link linkend="CDM_Analysis">CDM_Analysis</link>, <link
      linkend="SPWVD_Analysis">SPWVD_Analysis</link> or <link
      linkend="CDM_SPWVD_Analysis">CDM_SPWVD_Analysis</link>
      functions.
    </para>
    <para> The <link linkend="AddChannelPlot">AddChannelPlot</link>
    can be used to add some other signals subpots.
    </para>
  </refsection>
  <refsection>
    <title>Menus</title>
    <itemizedlist>
      <listitem>
        <term>Scroll</term> menu allows to manage scrolling over
       the time. The <literal>Set scroll length</literal> submenu
       allows to specify the time width of the scrolling window, the
       scroll length default value is a tenth of the total time
       range. The <literal>Scroll on</literal> submenu enables the
       scrolling mode: to scroll over the time use the left and right
       directional arrows, to go back to full view hit the
       <literal>q</literal> key. The <literal>Scroll off</literal>
       submenu disable the scrolling mode.
       <para>
         Remark: If datatips tool is used while in Scroll mode, user
         needs to set the Scroll mode on for further scrolling.
       </para>
      </listitem>
      <listitem>
        <term>Options</term> menu allows to show/hide the curve
        legends and to specify the units of the phase display.
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
d=read(CWApath()+"demos/DATA/free_breathing_4Hz",-1,4)'; //read the data
freq_sampling=4;
RR=d(1,:);//RR signal
Vt=d(3,:);//Tidal Volume  signal

//narrow band filter
filterlength=355;
frequency_bounds=[0.25 0.35];

result=CDM_SPWVD_Analysis(RR,Vt,freq_sampling,frequency_bounds,filterlength) 

PlotSpectralAnalysis(result,["RR","Vt"],"Free Breathing CDM and SPWVD")  // Draw results
    ]]></programlisting>
    <para>Produces:</para>
  <scilab:image><![CDATA[
d=read(CWApath()+"demos/DATA/free_breathing_4Hz",-1,4)'; //read the data
freq_sampling=4;
RR=d(1,:);//RR signal
Vt=d(3,:);//Tidal Volume  signal

//narrow band filter
filterlength=355;
frequency_bounds=[0.25 0.35];

result=CDM_SPWVD_Analysis(RR,Vt,freq_sampling,frequency_bounds,filterlength) 

PlotSpectralAnalysis(result,["RR","Vt"],"Free Breathing CDM and SPWVD")  // Draw results

    ]]></scilab:image>
   
    <para>Hiding legends, setting the phase units to degree, using Scroll mode and datatips one can obtain:</para>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/CDM_SPWVD2.svg"/>
        </imageobject>
      </inlinemediaobject>
    </para>

  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member><link linkend="CDM_Analysis">CDM_Analysis</link></member>
      <member><link linkend="SPWVD_Analysis">SPWVD_Analysis</link></member>
      <member><link linkend="CDM_SPWVD_Analysis">CDM_SPWVD_Analysis</link></member>
      <member><link linkend="MultiChannelPlot">MultiChannelPlot</link></member>
      <member><link linkend="AddChannelPlot">AddChannelPlot</link></member>

    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
     <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
 
  
</refentry>
