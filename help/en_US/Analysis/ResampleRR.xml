<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2012 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xml:lang="en" xml:id="ResampleRR">
  <info>
    <pubdate>$LastChangedDate: 25-08-2011 $</pubdate>
  </info>
  <refnamediv>
    <refname>ResampleRR</refname>
    <refpurpose>Resample a RR cardiovascular signal</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[tn,RRn] = ResampleRR(RR,step)
</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>

      <varlistentry>
        <term>RR</term>
        <listitem>
          <para>
            a real vector. The initial RR signal values (a sequence of time intervals).
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>step</term>
        <listitem>
          <para>
            a real positive scalar. The  discretisation time step
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>tbounds</term>
        <listitem>
          <para>
            an optional real vector with 2 elements
            <literal>[tmin, tmax]</literal>, which can be used to
            restrict the time interval for re-sampling. The default
            value is <literal>[0,sum(x)]</literal>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>tn</term>
        <listitem>
          <para>
            a real vector. The time discretization points.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>xn</term>
        <listitem>
          <para>
            a real vector. The signal values at discretization points: the interpolated interval lengths.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>The RR signal is sequence of time delay between two peaks in
    the ECG signal.  <literal>ResampleRR(x,step)</literal> is
    equivalent to
    <literal>Resample([0,cumsum(x(1:$-1))],step)</literal>.

    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/twa00.ecgs");
S=ECGDetrend(S(1:10000,1));
kRp=ECGRpeaks(S);
RR1=diff(kRp)/S.fs;//in seconds
t1=kRp(1:$-1)/S.fs;
[t,RR]=ResampleRR(RR1,1/4);
clf;plot(t1,RR1,"+");plot(t+t1(1),RR,"r")
    ]]></programlisting>
  <scilab:image><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/twa00.ecgs");
S=ECGDetrend(S(1:10000,1));
kRp=ECGRpeaks(S);
RR1=diff(kRp)/S.fs;//in seconds
t1=kRp(1:$-1)/S.fs;
[t,RR]=ResampleRR(RR1,1/4);
clf;plot(t1,RR1,"+");plot(t+t1(1),RR,"r")
    ]]></scilab:image>

  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link type="scilab" linkend="scilab.help/splin">splin</link>
      </member>
      <member>
        <link linkend="Resample">Resample</link>
      </member>
      <member>
        <link type="scilab" linkend="scilab.help/interp">interp</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Alexandro Monti, INRIA</member>
    </simplelist>
  </refsection>
   <refsection>
     <title>Used Functions</title>
       <para>
         <link linkend="Resample">Resample</link>
       </para>
      <para>
        <link linkend="scilab.help/cumsum" type="scilab">cumsum</link>
       </para>
     </refsection>
</refentry>
