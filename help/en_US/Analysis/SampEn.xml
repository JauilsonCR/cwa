<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2015 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="SampEn">
  <refnamediv>
    <refname>SampEn</refname>
    <refpurpose>Measure of complexity using sample entropy.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>e = SampEn(y [,M [,r]])</synopsis>
    <synopsis>[e,A,B] = SampEn(y [,M [,r]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>y</term>
        <listitem>
          <para>
            A 1D real array, the time serie to analyze.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>M</term>
        <listitem>
          <para>
            A scalar with positive integer value, the maximum value of
            the embedding dimensions m. The default value is 5.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>r</term>
        <listitem>
          <para>
            A positive scalar, tolerance used to decide is two sets of
            simultaneous data points of length m are close. The
            threshold used by the decision alorithm is r*stdev(y). The
            default value is 0.2.
          </para>
        </listitem>
      </varlistentry>
       <varlistentry>
        <term>e</term>
        <listitem>
          <para>
            A real 1D column array, e(i) contains the sample entropy for embedding dimension m equal to i-1.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>A</term>
        <listitem>
          <para>
            A real 1D column array, A(m) contains then number of matches for m=1,...,M.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>B</term>
        <listitem>
          <para>
            A real 1D column array, B(m) contains then number of matches for m=0,...,M-1.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
    Sample entropy is a measure of complexity that can be used to analyse physiological signals like RR.</para>
    <para>For m=0, e can be interpreted as the negative logarithm of
    the probability of a match of length 1.</para>
    <para>
    <latex>
      \begin{eqnarray}{l}
       \text{Let  the sets: }\\
          Y_m(i)=\left\lbrace y_i, y_{i+1} \ldots y_{i+m-1}\right\rbrace,\\
          \text{a distance } d(Y_m(i),Y_m(j)),
           A, \text{ the count of set pairs } (i,j) \text{ of length } m \text{ such as }  d(Y_m(i),Y_m(j))\lt r\times  stdev(y),\\
           B, \text{ the count of set pairs } (k,l) \text{  of length } m+1 \text{ such as }  d(Y_{m+1}(k),Y_{m+1}(l))\lt r\times stdev(y),\\
\text{then }\\
           e_m=-\log\frac{A}{B}
    \end{eqnarray}
    </latex>
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
S=readEcgsFile("demos/datafiles/ecg1.ecgs");
kRp=ECGRpeaks(S(:,1));
RR=diff(kRp)/S.fs;
e=SampEn(RR)
]]> </programlisting>

 
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="timeDomainHRV" >timeDomainHRV</link>
      </member>
      <member>
        <link linkend="DFA" >DFA</link>
      </member>
      
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>DK Lake, JR Moorman and Cao Hanqing (original Matlab version)</member>
      <member>Serge Steer, INRIA, port to Scilab</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
 <ulink url="http://ajpheart.physiology.org/cgi/content/abstract/278/6/H2039"> Richman, J. S. and J. R. Moorman.
    Physiological time series analysis using approximate entropy and sample entropy. Am J Physiol 2000; 278(6):H2039-H2049; </ulink>
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
 
</refentry>
