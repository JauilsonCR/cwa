<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2012 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xml:lang="en" 
          xml:id="np_mc_nss_analysis">
  <info>
    <pubdate>$LastChangedDate: 19-10-2011 $</pubdate>
  </info>
  <refnamediv>
    <refname>np_mc_nss_analysis</refname>
    <refpurpose>Multi channel non stationnary signal analysis using short term fft</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[signals_energy,signals_dispersion,inout_gains,inout_coherences]
    = np_mc_nss_analysis(signals,frequency_bands [,options])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>signals</term>
        <listitem>
          <para>
            a <literal>N</literal> by <literal>Nt</literal> array with
            as many rows as signals.  The first row must be the
            output signal  and the next ones
            the inputs.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>frequency_bands</term>
        <listitem>
          <para>
            a <literal>M</literal> by 2 array, each row <literal>[fmin,
            fmax]</literal> specifies the frequency range to be
            studied.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>options</term>
        <listitem>
          <para>
             a struct with fields:
             <itemizedlist mark="bullet">
               <listitem>
                   <term>sectionlength</term>: an integer, the signal
                   section length to be used for power and cross
                   spectrum computation.
               </listitem>

               <listitem>
                   <term>sectionstep</term>: an integer, the step to
                   be applied for one signal section to the next
                   one. <literal>sectionstep</literal> must be less or
                   equal to <literal>sectionlength</literal>. Overlap
                   is <literal>sectionlength-sectionstep</literal>.

               </listitem>

               <listitem>
                   <term>smoothwindowlength</term>: an integer, the
                   length of the smoothing window (Hanning)
               </listitem>

               <listitem>
                   <term>minimalcoherence</term>: a positive number in
                   ]0 1].
               </listitem>
             </itemizedlist>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>signals_energy</term>
        <listitem>
          <para>
            a <literal>N</literal> by <literal>P</literal> by <literal>M</literal> 3D
            array. <literal>signals_energy(k,:,i)</literal> gives the
            energy of the signal <literal>signals(k,:)</literal> in the frequency
            band <literal>[frequency_bands(i,1)
            frequency_bands(i,2)]</literal>.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>signals_dispersion</term>
        <listitem>
          <para>
            a tlist with fields:
            <itemizedlist mark="bullet">
               <listitem>
                 <term>fmin</term>: a <literal>N</literal> by
                 <literal>P</literal> by <literal>M</literal> 3D
                 array. <literal>signals_dispersion.fmin(k,:,i)</literal>
                 gives the frequency around which the dispersion is
                 minimal for the signal
                 <literal>signals(k,:)</literal> in the frequency band
                 <literal>[frequency_bands(i,1)
                 frequency_bands(i,2)]</literal>.
               </listitem>

               <listitem>
                 <term>pmin</term>: a <literal>N</literal> by
                 <literal>P</literal> by <literal>M</literal> 3D
                 array. <literal>signals_dispersion.pmin(k,:,i)</literal>
                 gives the value of the SPSD at the frequency of
                 minimal dispersion for the signal
                 <literal>signals(k,:)</literal> in the frequency band
                 <literal>[frequency_bands(i,1)
                 frequency_bands(i,2)]</literal>.
               </listitem>

               <listitem>
                 <term>dmin</term>: a <literal>N</literal> by
                 <literal>P</literal> by <literal>M</literal> 3D array
                 which values are in
                 <literal>[0;1]</literal>. <literal>signals_dispersion.dmin(k,:,i)</literal>
                 gives the value of the minimal dispersion for the
                 signal <literal>signals(k,:)</literal> in the
                 frequency band <literal>[frequency_bands(i,1)
                 frequency_bands(i,2)]</literal>.  Dispersion value is
                 zero when the signal is merely a sinusoid, only one
                 components, whereas is 1 if the spectra is completely
                 spread, so there isn't a principal component.
               </listitem>

            </itemizedlist>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>inout_gains</term>
        <listitem>
            a <literal>N-1</literal> by <literal>P</literal> by
            <literal>M</literal> 3D
            array. <literal>inout_gains(k,:,i)</literal> gives the
            <literal>signals(k+1,:)</literal> to
            <literal>signals(1,:)</literal> transfer function gain in
            the frequency band <literal>[frequency_bands(i,1)
            frequency_bands(i,2)]</literal>.
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>inout_coherences</term>
        <listitem>
            a <literal>N-1</literal> by <literal>P</literal> by
            <literal>M</literal> 3D
            array. <literal>inout_gains(k,:,i)</literal> gives the
            <literal>signals(k+1,:)</literal> to
            <literal>signals(1,:)</literal> coherence in
            the frequency band <literal>[frequency_bands(i,1)
            frequency_bands(i,2)]</literal>.
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      This function performs non parametric multi channel non
      stationary signal analysis using short-time Fourier transform
      (STFT) by which the time record is multiplied by a sliding
      window and the FT of the consecutive windowed segments are
      computed resulting in a series of local spectra. 
    </para>
    <para>
    This analysis can be done simultaneously on several frequency ranges. 
    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <para>
      Create signals
    </para>
    <programlisting role="example"><![CDATA[
Tmax=90;//final time
fs=20;//sampling frequency Hz
f0=0.5;//base frequency Hz
// Time instants
t=0:1/fs:Tmax;N=size(t,'*');
n=round(N/2)
//output signal
f1=1.2*f0;
out=[2*sin(2*%pi*f0*t(1:n)) sin(2*%pi*f1*t(n+1:$))]+2d-2*rand(t); 
// Input signal
f2=1.4*f0;
in=[sin(2*%pi*f0*t(1:n)) sin(2*%pi*f2*t(n+1:$))];
    ]]></programlisting>
    <para>
      Perform analysis
    </para>
    <programlisting role="example"><![CDATA[
sl=300; //section length
clear options
options.sectionlength=sl;
options.sectionstep=sl/2; //50% overlaping
options.smoothwindowlength=5;
options.minimalcoherence=0.5;
fbands=[0.5*f0 2.5*f0]/fs;
[energy,dispersion,inout_gain,inout_coherence]=np_mc_nss_analysis([out;in],fbands,options);
nr=size(energy,2);
tr=(1:nr)*sl/(2*fs);

    ]]></programlisting>
    <para>
      Drawings
    </para>
    <programlisting role="example"><![CDATA[
o=0.05; //reserve space for title
h=1/3;
clf;
a=newaxes();a.axes_bounds=[0,0,1,h];a.margins(3)=0.25;
plot(t',[in;out]');legend(['in','out']);title("signals")
a=newaxes();a.axes_bounds=[0,h,1,h];;a.margins(3)=0.25;
plot(tr,inout_gain); title("in -> out gain")
a=newaxes();a.axes_bounds=[0,2*h,1,h];a.margins(3)=0.25;
plot(tr,inout_coherence); xlabel('time');title("in/out coherence")

    ]]></programlisting>
  <scilab:image><![CDATA[
Tmax=90;//final time
fs=20;//sampling frequency Hz
f0=0.5;//base frequency Hz
// Time instants
t=0:1/fs:Tmax;N=size(t,'*');
n=round(N/2)
//output signal
f1=1.2*f0;
out=[2*sin(2*%pi*f0*t(1:n)) sin(2*%pi*f1*t(n+1:$))]+2d-2*rand(t); 
// Input signal
f2=1.4*f0;
in=[sin(2*%pi*f0*t(1:n)) sin(2*%pi*f2*t(n+1:$))];
sl=300; //section length
clear options
options.sectionlength=sl;
options.sectionstep=sl/2; //50% overlaping
options.smoothwindowlength=5;
options.minimalcoherence=0.5;
fbands=[0.5*f0 2.5*f0]/fs;
[energy,dispersion,inout_gain,inout_coherence]=np_mc_nss_analysis([out;in],fbands,options);
nr=size(energy,2);
tr=(1:nr)*sl/(2*fs);
o=0.05; //reserve space for title
h=1/3;
clf;
a=newaxes();a.axes_bounds=[0,0,1,h];a.margins(3)=0.25;
plot(t',[in;out]');legend(['in','out']);title("signals")
a=newaxes();a.axes_bounds=[0,h,1,h];;a.margins(3)=0.25;
plot(tr,inout_gain); title("in -> out gain")
a=newaxes();a.axes_bounds=[0,2*h,1,h];a.margins(3)=0.25;
plot(tr,inout_coherence); xlabel('time');title("in/out coherence")
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Alexandro Monti, INRIA</member>
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         Rapport de recherche INRIA RR-4427:"Short term control of the
         cardivascular system: modelling and signal analysis"
         Alessandro Monti, Claire Médigue, Michel Sorine,2002 pp 50-
       </para>
     </refsection>
  <refsection>
     <title>Used Functions</title>
       <para>
        Mainly based on windowed fft
       </para>
     </refsection>
</refentry>
