<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2012 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xml:lang="en" xml:id="ExportBaroreflexAnalysis">
  <info>
    <pubdate>$LastChangedDate: 26-03-2012 $</pubdate>
  </info>
  <refnamediv>
    <refname>ExportBaroreflexAnalysis</refname>
    <refpurpose>Export spectral analysis results into a csv or text file.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>ExportBaroreflexAnalysis(res [,filetype [,filename]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>res</term>
        <listitem>
          A Scilab data structure returned by <link
          linkend="Baroreflex_Analysis">Baroreflex_Analysis</link>.
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>filetype</term>
        <listitem>
          a character string with possible values:
          <literal>csv</literal> or <literal>txt</literal>. The
          default value is <literal>txt</literal>.
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>filename</term>
        <listitem>
          a character string with possible values:
          <literal>csv</literal> or <literal>txt</literal>. If this
          argument is omitted, a file selection dialog will open.
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      <literal>ExportBaroreflexAnalysis</literal> can be used to export
      the result produced by <link
      linkend="Baroreflex_Analysis">Baroreflex_Analysis</link>
      into formatted files for use in other tools like Excel,
      LibreOffice,...
    </para>
    <para>
      For reuse into Scilab it is better to save these results in
      binary mode using the <link type="scilab"
      linkend="scilab.help/save">save</link> function.
    </para>
    <para>
      <literal>ExportBaroreflexAnalysis(res,"txt" [,filename])</literal>, 
      generates a text file formatted into
      columns. The first element of each column contains the field
      name of the corresponding <literal>res</literal> structure
      followed by the formatted values of the field.
    </para>
    <para>
      <literal>ExportBaroreflexAnalysis(res,"csv"
      [,filename])</literal>, generates a csv file formatted into
      columns. The first line contains the field names of the
      corresponding <literal>res</literal> structure and the following
      lines contains the formatted values of the fields. Line elements
      are separated by tabs. The decimal separator for numbers is the
      comma (,). cvs files can be read into Excel or LibreOffice tools.
    </para>
    <para>
      In both cases the numbers are formatting using the current
      Scilab formatting. This formatting can be controlled through the
      <link type="scilab" linkend="scilab.help/format">format</link> function.
    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <para>Signals acquisition and filtering</para>
    <programlisting role="example"><![CDATA[
    d=read(CWApath()+"demos/DATA/tilt_divers",-1,5)';//[RR SBP DBP Vt time] 
    freq_sampling=2;
    shift=600;
    RR= d(1,shift:$);
    SBP=d(2,shift:$);
    //Band pass filtering
    BP_n=355;
    BPdelay=(BP_n-1)/2;
    BP=wfir("bp",BP_n,[0.04 0.4]/freq_sampling,"hn",[0 0]);
    RR=filter(BP,1,RR);
    SBP=filter(BP,1,SBP);
    ]]></programlisting>
    <para>Analysis</para>
    <programlisting role="example"><![CDATA[
    clear options
    sl=64;
    options.sectionlength=sl;
    st=sl/2;
    options.sectionstep=st; //50% overlaping
    options.smoothwindowlength=5;
    options.minimalcoherence=0.5;
    fbands=[0.2 0.3];
    result=Baroreflex_Analysis(RR,SBP,freq_sampling,fbands,options)
    ]]></programlisting>
    <para>
      Drawing
    </para>
    <programlisting role="example"><![CDATA[
    ExportBaroreflexAnalysis(result,"txt",TMPDIR+"/foo.txt")
    editor(TMPDIR+"/foo.txt")
    // get it back
    [M,header] = fscanfMat(TMPDIR+"/foo.txt")
    ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member><link type="scilab" linkend="scilab.help/save">save</link></member>
      <member><link type="scilab" linkend="scilab.help/write_csv">write_csv</link></member>
      <member><link type="scilab" linkend="scilab.help/fscanfMat">fscanfMat</link></member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
</refentry>
