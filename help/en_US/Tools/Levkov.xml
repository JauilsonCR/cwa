<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="Levkov" xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>Levkov</refname>

    <refpurpose>Removes power line interference in a single channel
    ECG</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>ecg_out = Levkov(ecg_in,fe,f,Threshold)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>ecg_in</term>

        <listitem>
          <para>a real vector, the signal samples of the ECG.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>fe</term>

        <listitem>
          <para>a positive scalar, the sampling frequency in Hz.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>f</term>

        <listitem>
          <para>a positive scalar, the power line frequency in Hz.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Threshold</term>

        <listitem>
          <para>a positive scalar, a threshold used to localize the
          iso-electric part of the signals.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>ecg_out</term>

        <listitem>
          <para>a real vector contains the filtered signal samples .</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function removes power line interference in single channel ECG
    using the C. Levkov algorithm</para>
  </refsection>

  <refsection>
    <title>More information</title>

    <important>
      <para>Sampling frequency fe must be a multiple of the PLI frequency
      f.</para>
    </important>

    <tip>
      <para>It is recommended to apply this function to detrended ECG (see
      <link linkend="ECGDetrend">ECGDetrend</link>).</para>
    </tip>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">
    S=readEcgsFile(CWApath()+"demos/datafiles/ecg1.ecgs");
    Sd=ECGDetrend(S);
    Sf=Levkov(Sd.sigs,Sd.fs,50,0.05);
    clf;plot(Sd.sigs(200:3000));plot(Sf(200:3000),'r');
    legend(["Sd","Sf"]);
    ax1=newaxes();
    plot(Sd.sigs(600:1500));plot(Sf(600:1500),'r');
    ax1.axes_bounds=[0.18 0.125 0.35 0.45];ax1.axes_visible='off';
    ax1.data_bounds(:,2)=[-0.02;0.02];
    title("zoom")
    </programlisting>

    <scilab:image>
    S=readEcgsFile(CWApath()+"demos/datafiles/ecg1.ecgs");
    Sd=ECGDetrend(S);
    Sf=Levkov(Sd.sigs,Sd.fs,50,0.05);
    clf;plot(Sd.sigs(200:3000));plot(Sf(200:3000),'r');
    legend(["Sd","Sf"]);
    ax1=newaxes();
    plot(Sd.sigs(600:1500));plot(Sf(600:1500),'r');
    ax1.axes_bounds=[0.18 0.125 0.35 0.45];ax1.axes_visible='off';
    ax1.data_bounds(:,2)=[-0.02;0.02];
    title("zoom")
</scilab:image>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="ECGSubstractPLI">ECGSubstractPLI</link></member>

      <member><link linkend="ECGDetrend">ECGDetrend</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>"Substraction of 50Hz interference from the electrocardiogram",
    C.levkov, G. Micov, R.Ivanov and I. K. Daskalov</para>

    <para>Medical and Biological Engineering and Computing July 1984, Volume
    22, Issue 4, pp 371-373</para>

    <para><ulink url="http://link.springer.com/article/10.1007%2FBF02442109#page-1">Subtraction of 50 Hz interference from the electrocardiogram</ulink>.</para>
  </refsection>

  <refsection>
    <title>History</title>

    <revhistory>
      <revision>
        <revnumber>0.0</revnumber>

        <revdescription>Function Levkov added</revdescription>
      </revision>
    </revhistory>
  </refsection>
</refentry>
