<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="dsearch2D" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>dsearch2D</refname>

    <refpurpose>Classification of 2D sets.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>occ = dsearch2D(xy,dx,dy [,opt])</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>xy</term>

        <listitem>
          <para>a two column array. Each row contains the coordinates of a
          point in the set.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>dx</term>

        <listitem>
          <para>A 1D array. Defines the classification along the abscissa. The
          values in dx must be in strictly increasing order</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>dy</term>

        <listitem>
          <para>A 1D array. Defines the classification along the abscissa. The
          values in dy must be in strictly increasing order</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>opt</term>

        <listitem>
          <para>An optional character string, with admissible values "c" or
          "d". If opt=="c" then dx and dy defines intervals, If opt=="c" then
          dx and dy defines values,</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>occ</term>

        <listitem>
          <para>A 2D array. If opt=="c" then occ(i,j) contains the number of
          points which lie in the 2D region [dx(i) dx(i+1)[ X [dy(j) dy(j+1[.
          If opt=="d" then occ(i,j) contains the number of points such as
          x==dx(i) and y==dy(j).</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function performs classification of 2D sets</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[
//Create 2D set of points
rand('normal')
XY=[1.3,1.5,1.8,2.1,2.3,2.5,2.9,3,3.1,1.6,1.6,2,2.3,2.5,2.8,2.9,1.3;
    1.4,1.7,2.2,2.8,3.5,3.8,3.9,3.9,3.9,2,1.3,1.4,1.4,1.5,1.8,2.3,3.8]*30;
rx= [3,1.3,2.4,0.9,0.3,1.1,2.7,2.5,1.4,2.1,1.4,2.9,2.1,0.3,0.2,1,0.5];
ry=[1.7,1,0.5,1,0.1,0.2,0.3,2.4,2.1,0.5,2.4,0.8,2.8,0.5,1.2,2.1,0.2]*3;
x=[];y=[];
for k=1:17,
  x=[x;XY(1,k)+rx(k)*rand(1000,1)];
  y=[y;XY(2,k)+ry(k)*rand(1000,1)];
end
clf();f=gcf();
subplot(211);ax=gca();
plot(x,y,'x')
ax.tight_limits="on";
mnx=min(x);mxx=max(x);
mny=min(y);mxy=max(y);
dx=linspace(mnx,mxx,40);//40 bins along x
dy=linspace(mny,mxy,30);//30 bins along y
occ=dsearch2D([x,y],dx,dy);
f.color_map=jetcolormap(max(occ));
subplot(212);ax=gca();
grayplot(dx(1:$-1)+(dx(2)-dx(1))/2,dy(1:$-1)+(dy(2)-dy(1))/2,occ)
ax.tight_limits="on";
   ]]></programlisting>

    <scilab:image><![CDATA[
//Create 2D set of points
rand('normal')
XY=[1.3,1.5,1.8,2.1,2.3,2.5,2.9,3,3.1,1.6,1.6,2,2.3,2.5,2.8,2.9,1.3;
    1.4,1.7,2.2,2.8,3.5,3.8,3.9,3.9,3.9,2,1.3,1.4,1.4,1.5,1.8,2.3,3.8]*30;
rx= [3,1.3,2.4,0.9,0.3,1.1,2.7,2.5,1.4,2.1,1.4,2.9,2.1,0.3,0.2,1,0.5];
ry=[1.7,1,0.5,1,0.1,0.2,0.3,2.4,2.1,0.5,2.4,0.8,2.8,0.5,1.2,2.1,0.2]*3;
x=[];y=[];
for k=1:17,
  x=[x;XY(1,k)+rx(k)*rand(1000,1)];
  y=[y;XY(2,k)+ry(k)*rand(1000,1)];
end
clf();f=gcf();
subplot(211);ax=gca();
plot(x,y,'x')
ax.tight_limits="on";
mnx=min(x);mxx=max(x);
mny=min(y);mxy=max(y);
dx=linspace(mnx,mxx,40);//40 bins along x
dy=linspace(mny,mxy,30);//30 bins along y
occ=dsearch2D([x,y],dx,dy);
f.color_map=jetcolormap(max(occ));
subplot(212);ax=gca();
grayplot(dx(1:$-1)+(dx(2)-dx(1))/2,dy(1:$-1)+(dy(2)-dy(1))/2,occ)
ax.tight_limits="on";
     ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="scilab.help/dsearch"
      type="scilab">dsearch</link></member>

      <member><link linkend="scilab.help/hist3d"
      type="scilab">hist3d</link></member>

      <member><link linkend="scilab.help/grayplot"
      type="scilab">grayplot</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>History</title>

    <revhistory>
      <revision>
        <revnumber>0.0</revnumber>

        <revdescription>Function dsearch2D added</revdescription>
      </revision>
    </revhistory>
  </refsection>

  <refsection>
    <title>Used Functions</title>

    <para><link linkend="scilab.help/dsearch"
    type="scilab">dsearch</link></para>
  </refsection>
</refentry>
