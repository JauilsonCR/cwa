<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="Levkov_f" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>Levkov_f</refname>

    <refpurpose>Internal built-in used by ECGSubstractPLIBatch</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>Levkov_f(in,out,fe,f,Threshold,Nchannels,Nsamples)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>in</term>

        <listitem>
          <para>a positive integer, the input file descriptor as returned by
          <link linkend="scilab.help/mopen" type="scilab">mopen</link>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>out</term>

        <listitem>
          <para>a positive integer, the output file descriptor as returned by
          <link linkend="scilab.help/mopen" type="scilab">mopen</link>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>fe</term>

        <listitem>
          <para>a positive scalar, the sampling frequency in Hz.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>f</term>

        <listitem>
          <para>a positive scalar, the power line frequency in Hz.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Threshold</term>

        <listitem>
          <para>a vector of positive scalars giving the thresholds for each
          channels.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Nchannels</term>

        <listitem>
          <para>a positive integer, must be equal to the number of ECG
          channels stored in the input file.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Nsamples</term>

        <listitem>
          <para>a positive integer, The number of samples to take into account
          starting from the current file pointer.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This built-in is only intended to be used by <link
    linkend="ECGSubstractPLIBatch">ECGSubstractPLIBatch</link>.</para>
  </refsection>

  <refsection>
    <title>More information</title>

    <important>
      <para>Sampling frequency fe must be a multiple of the PLI frequency
      f.</para>
    </important>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="ECGSubstractPLI">ECGSubstractPLI</link></member>

      <member><link
      linkend="ECGSubstractPLIBatch">ECGSubstractPLIBatch</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>"Substraction of 50Hz interference from the electrocardiogram",
    C. Levkov, G. Micov, R.Ivanov and I. K. Daskalov</para>

    <para>Medical and Biological Engineering and Computing July 1984, Volume
    22, Issue 4, pp 371-373</para>

    <para><ulink url="http://link.springer.com/article/10.1007%2FBF02442109#page-1">Subtraction of 50 Hz interference from the electrocardiogram</ulink>.</para>
  </refsection>

  <refsection>
    <title>History</title>

    <revhistory>
      <revision>
        <revnumber>0.0</revnumber>

        <revdescription>Function Levkov_f added</revdescription>
      </revision>
    </revhistory>
  </refsection>
</refentry>
