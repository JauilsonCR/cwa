<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2012 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xmlns:scilab="http://www.scilab.org" 
          xml:lang="en" xml:id="wfir_gui">
  <info>
    <pubdate>$LastChangedDate: 30-11-2011 $</pubdate>
  </info>
  <refnamediv>
    <refname>wfir_gui</refname>
    <refpurpose>Graphical user interface that can be used to interactively design wfir filters</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[ftype,forder,cfreq,wtype,fpar] = wfir_gui()</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>ftype</term>
        <listitem>
          <para>
            a string: the selected filter type.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>forder</term>
        <listitem>
          <para>
            a scalar with positive integer value: the selected filter order
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>cfreq</term>
        <listitem>
          <para>
            a 2 vector: the cut-off frequencies in normalized frequencies
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>wtype</term>
        <listitem>
          <para>
            a string: the selected window type.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>fpar</term>
        <listitem>
          <para>
            2-vector of window parameters.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      This function proposes a graphical user interface to allow user
      interactively select the design parameters of windowed
      finite impulse response filters (see wfir). It is called by
      <literal>wfir</literal> when no input arguments are given.
    </para>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/wfir_gui_dialog.png"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>
      If requested, the frequency response of the filter is
      automatically updated according to the parameters set in the
      dialog window:
    </para>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/wfir_gui_view.svg"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        wfir
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Used Functions</title>
    <para>
      Based on uicontrol functions.
    </para>
  </refsection>

</refentry>
