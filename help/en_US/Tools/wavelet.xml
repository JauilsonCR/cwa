<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright (C) 2014 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="wavelet">
  <refnamediv>
    <refname>wavelet</refname>
    <refpurpose>1D Wavelet transform with optional significance testing.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[wave,period,Scale,coi] = wavelet(Y,dt [,pad [,dj [,s0 [,J1 [,mother [,param]]]]]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>Y</term>
        <listitem>
          <para>
             a real 1D array, the time series of length N.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>dt</term>
        <listitem>
          <para>
            a positive scalar, the sampling period of the time series.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>pad</term>
        <listitem>
          <para>
            a boolean, if set to %t (default is %f), pad time series with enough
            zeroes to get N up to the next higher power of 2. This
            prevents wraparound from the end of the time series to the
            beginning, and also speeds up the FFT's used to do the
            wavelet transform.  This will not eliminate all edge
            effects (see COI below). </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>dj</term>
        <listitem>
          <para>
            a positive scalar, the spacing between discrete scales. Default is 0.25.
            A smaller value will give better scale resolution.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>s0</term>
        <listitem>
          <para>
             a positive scalar, the smallest scale of the wavelet.  Default is 2*dt.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>J1</term>
        <listitem>
          <para>
            a positive scalar with integer value, the number of scales
            minus one. Scales range from s0 up to s0*2^(j1*dj), to
            give a total of (j1+1) scales. Default is j1 = (LOG2(N
            dt/s0))/dj
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>mother</term>
        <listitem>
          <para>
            a character string, the mother wavelet function.
             The possible choices are "MORLET" (the default), "PAUL", or "DOG". 
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>param</term>
        <listitem>
          <para>
            the mother wavelet parameter:</para>
           <para> For "MORLET" this is k0 (wavenumber), default is 6.</para>
           <para>For "PAUL" this is m (order), default is 4.</para>
           <para>For "DOG" this is m (m-th derivative), default is 2.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>wave</term>
        <listitem>
          <para>
            the wavelet transform of y. This is a complex array of
            dimensions (N,J1+1).</para>
            <para>
              abs(wave) gives the wavelet
            amplitude, atan(imag(wave),real(wave)) gives the wavelet
            phase.  The wavelet power spectrum is abs(wave)^2.  Its
            units are sigma^2 (the time series variance).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>period</term>
        <listitem>
          <para>
            the array of "Fourier" periods (in time units) that
            corresponds to the SCALEs.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Scale</term>
        <listitem>
          <para>
            the vector of scale indices, given by s0*2^(j*dj),
            j=0...j1 where j1+1 is the total number of scales.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>coi</term>
        <listitem>
          <para>
            A 1D array of size N, The Cone-of-Influence, contains the
            maximum period of useful information at that particular
            time.  Periods greater than this are subject to edge
            effects. </para>
         
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      Computes 1D Wavelet transform with optional significance testing.
    </para>
    
  </refsection>
 
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
load(CWApath()+"demos/datafiles/RR_free.dat");
RR=RR(1:600);
t=cumsum(RR);
//resample the RR signal
dt=1/2;
t2 = t(1):dt:t($); //time values for interp.
RR2=interp1(t,RR,t2,'spline')';
dj=1/64;
[wave,period,Scale,coi] = wavelet(RR2,dt,%t,dj,2*dt,7/dj);
f=1.0./period(:,$:-1:1); //frequency in ascending order
cwtpower = abs(wave($:-1:1,:)).^2 ; // compute wavelet power spectrum  and flip it according to frequencies
clf;set(gcf(),"color_map",jetcolormap(128));grayplot(t2,f,cwtpower')
    ]]></programlisting>
    <scilab:image><![CDATA[
load(CWApath()+"demos/datafiles/RR_free.dat");
RR=RR(1:600);
t=cumsum(RR);
//resample the RR signal
dt=1/2;
t2 = t(1):dt:t($); //time values for interp.
RR2=interp1(t,RR,t2,'spline')';
dj=1/64;
[wave,period,Scale,coi] = wavelet(RR2,dt,%t,dj,2*dt,7/dj);
f=1.0./period(:,$:-1:1); //frequency in ascending order
cwtpower = abs(wave($:-1:1,1:2:$)).^2 ; // compute wavelet power spectrum  and flip it according to frequencies
clf;set(gcf(),"color_map",jetcolormap(128));grayplot(t2(1:2:$),f,cwtpower')
    ]]></scilab:image>
  </refsection>
  
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>C. Torrence and G. Compo</member>
      <member>Serge Steer, INRIA, Scilab version</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         Torrence, C. and G. P. Compo, 1998: "A Practical Guide to
         Wavelet Analysis." <ulink
         url="http://paos.colorado.edu/research/wavelets">Bull. Amer. Meteor. Soc.,
         79, 61-78.</ulink>
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function added</revdescription>
        </revision>
      </revhistory>
    </refsection>
 
</refentry>
