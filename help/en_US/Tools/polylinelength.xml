<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="polylinelength" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>polylinelength</refname>

    <refpurpose>Compute the length of ND polyline.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>s = polylinelength(d)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>d</term>

        <listitem>
          <para>An array of size np x nd, where np is the number of points of
          the polyline and nd the number of dimensions. Each row contains the
          Cartesian coordinates of a point.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>s</term>

        <listitem>
          <para>A scalar, the polyline length.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Compute the length of ND polyline given by the Cartesian coordinates
    of its points.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[
t=linspace(0,10*%pi,1000)';
d=[sin(t),cos(t)];
l=polylinelength(d)
    ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link
      linkend="polylinecumlength">polylinecumlength</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>History</title>

    <revhistory>
      <revision>
        <revnumber>0.0</revnumber>

        <revdescription>Function polylinelength added </revdescription>
      </revision>
    </revhistory>
  </refsection>
</refentry>
