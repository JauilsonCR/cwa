<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2015 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="syntheticRRsignal">
  <refnamediv>
    <refname>syntheticRRsignal</refname>
    <refpurpose>Short term RR representative synthetic signal generator.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[t,RR] = syntheticRRsignal(options)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>options</term>
        <listitem>
          <para>
            A sequence of pairs (key,value) where the possible keys are
          </para>
          <itemizedlist>
            <listitem>
              <para>
                "fs": the sampling frequency of the RR signal in Hz (default=4Hz)
              </para>
            </listitem>
            <listitem>
              <para>
                "tmax": the length of the generated signal in seconds (default=600s)
              </para>
            </listitem>
            <listitem>
              <para>
                "RRmean": the mean of the RR intervals in milli-seconds (default=1000ms)
              </para>
            </listitem>
            <listitem>
              <para>
                "std": the standard deviation of the gaussian noise in milli-seconds (default=0.5 ms)
              </para>
            </listitem>

            <listitem>
              <para>
                "f_VLF" :A two elements array, the frequencies in Hz of the very low frequency term(default=[0.005 0.03] Hz)
              </para>
            </listitem>
            <listitem>
              <para>
                "ampl_VLF" :A two elements array, the  amplitudes in ms of the very low frequency term  (default=[0.5 0.6] ms)
              </para>
            </listitem>
            <listitem>
              <para>
                "f_LF" :A three elements array, the frequencies in Hz
                of the low frequency term: f_LF(1:2) are the min and
                max instantaneous frequencies and f_LF(3) is the
                modulation frequency (default=[0.08 0.1 0.027] ms)
              </para>
            </listitem>
            <listitem>
              <para>
                "ampl_LF": A scalar or an array with the same size as
                t=0:1/fs:tmax, the instantaneous amplitude (ms) of the
                low frequency term (default=4ms)
              </para>
            </listitem>
            <listitem>
              <para>
                "f_HF":A three elements array, the frequencies in Hz
                of the high frequency term: f_HF(1:2) are the min and
                max instantaneous frequencies and f_HF(3) is the
                modulation frequency (default=[0.24 0.26 0.05] ms)
              </para>
            </listitem>
            <listitem>
              <para>
                "ampl_HF": A scalar or an array with the same size as
                t=0:1/fs:tmax, the instantaneous amplitude (ms) of the
                low frequency term (the default is an array)
              </para>
            </listitem>

          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>t</term>
        <listitem>
          <para>
            A real array, the sampling instants in seconds.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>RR</term>
        <listitem>
          <para>
            A real array the regularly sampled RR signal in milli seconds.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
        This function generates short term RR representative synthetic signal of the following form:
    <latex>
      \begin{array}{l}
      RR=RR_{mean}+VLF+LF+HF+w\\
      \text{Where}\\
      VLF=A_{VLF_1} \sin(2*\pi f_{VLF_1} t)+A_{VLF_2} \sin(2\pi f_{VLF_2} t)\\
      LF= A_{LF} cos(2\pi f_{LF} t- A_{LF_m}\cos(2\pi t f_{LF_m}))\\
      HF= A_{HF}(t) cos(2\pi f_{HF} t- A_{HF_m}\cos(2\pi t f_{HF_m}))\\
      w \text{ a white gaussian noise}\\
      RR_{mean} \text{ The mean value of the RR intervals}
      \end{array}
    </latex>
    </para>
  
  </refsection>
 
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
       [t,RR]=syntheticRRsignal_n("ampl_VLF",[0.8 0.9],"std",4);

       clf();plot(t,RR)
    ]]></programlisting>
    <scilab:image><![CDATA[
       [t,RR]=syntheticRRsignal_n("ampl_VLF",[0.8 0.9],"std",4);
       clf();plot(t,RR)

    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="ecgsyn">ecgsyn</link>
      </member>

    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
      <member>Alessandro Monti, INRIA (LARY_CR version)</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         <ulink url="http://www.physionet.org/physiotools/ecgsyn">http://www.physionet.org/physiotools/ecgsyn</ulink>
       </para>
       <para>McSharry PE, Clifford GD, Tarassenko L, Smith L. A dynamical model for generating synthetic electrocardiogram signals. IEEE Transactions on Biomedical Engineering 50(3): 289-294; March 2003. 
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
  <refsection>
     <title>Used Functions</title>
       <para>
         <link linkend="fmsin">fmsin</link>
       </para>
     </refsection>
</refentry>
