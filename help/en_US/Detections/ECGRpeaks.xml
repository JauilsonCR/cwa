<?xml version="1.0" encoding="utf-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="ECGRpeaks" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>ECGRpeaks</refname>

    <refpurpose>find the locations of the R peaks of a single channel
    ECG.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>kRp = ECGRpeaks(S)</synopsis>
    <synopsis>kRp = ECGRpeaks(s,fs)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>S</term>
        <listitem>
          <para>A single channel <link linkend="sciecg">sciecg</link> data structure.</para>
        </listitem>
      </varlistentry>
    
      <varlistentry>
        <term>s</term>
        <listitem>
          <para> a real vector: the ECG samples.</para>
        </listitem>
      </varlistentry>
   
      <varlistentry>
        <term>fs</term>
        <listitem>
          <para> a positive scalar, the sampling frequency of the ECG.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>kRp</term>
        <listitem>
          <para>a vector of indexes: the locations of the R peaks.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function finds the locations of the R peaks of a single channel
    ECG using parabolic fitting.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <para>The following instructions generates the graph below:</para>

    <programlisting role="example"><![CDATA[
 S=readEcgsFile(CWApath()+"demos/datafiles/twa00.ecgs");
 S=ECGDetrend(S(1:10000,1));
 kRp=ECGRpeaks(S);
 clf();
 t=(0:size(S,1)-1)/S.fs;
 plot(t',S.sigs(:,1),'b',t(kRp)',S.sigs(kRp,1),'+r')
 xlabel(_("Time (s)"));ylabel("mV")
    ]]></programlisting>
 <scilab:image><![CDATA[
 S=readEcgsFile(CWApath()+"demos/datafiles/twa00.ecgs");
 S=ECGDetrend(S(1:10000,1));
 kRp=ECGRpeaks(S);
 clf();
 t=(0:size(S,1)-1)/S.fs;
 plot(t',S.sigs(:,1),'b',t(kRp)',S.sigs(kRp,1),'+r')
 xlabel(_("Time (s)"));ylabel("mV")
set(gcf(),"axes_size", [630,260])
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="ECG">ECG</link></member>

      <member><link linkend="ECGDetrend">ECGDetrend</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Qinghua Zhang,INRIA: the original Matlab algorithm</member>

      <member>Serge Steer, INRIA: translation to Scilab</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>History</title>

    <revhistory>
      <revision>
        <revnumber>0.0</revnumber>

        <revdescription>Function  added</revdescription>
      </revision>
    </revhistory>
  </refsection>
</refentry>
