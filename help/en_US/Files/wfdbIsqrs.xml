<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright (C) 2014 - INRIA - Serge Steer
 This file must be used under the terms of the CeCILL.
 This source file is licensed as described in the file COPYING, which
 you should have received as part of this distribution.  The terms
 are also available at
 http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="wfdbIsqrs">
  <refnamediv>
    <refname>wfdbIsqrs</refname>
    <refpurpose>Search annotations corresponding to R peaks locations.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>ann_out = wfdbIsqrs(ann_in)</synopsis>
    <synopsis>ann_out = wfdbIsqrs(path)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>ann_in</term>
        <listitem>
          <para>
           <para>A floating point array, with 5 rows (see <link
           linkend="wfdbReadAnnotations">wfdbReadAnnotations</link>
           function). The first row contains the annotation dates, the
           second row contains the annotation type code .</para>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>path</term>
        <listitem>
          <para>
            A character string the path of an annotation file.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ann</term>
        <listitem>
          <para>
            A floating point array, the annotations corresponding to R peaks.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      This function search Physionet annotations corresponding to R peaks locations in a set of annotations.
    </para>

  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
A=wfdbIsqrs(CWApath()+"demos/datafiles/sel100.pu1");
//previous instruction is equivalent to
//ann=wfdbReadAnnotations(CWApath()+"demos/datafiles/sel100","pu1");
//A=wfdbIsqrs(ann);

[sig,fe]= wfdbReadSamples(CWApath()+"demos/datafiles/sel100");
tmax=70;
kRp=A(1,:)*fe;
t=(0:size(sig,2)-1)/fe;
clf;subplot(211)
plot(t,sig(2,:),"b",t(kRp),sig(2,kRp),"+r");
set(gca(),"zoom_box",[10 4.5 100 6 -1 1]);
subplot(212)
plot(A(1,1:$-1),diff(A(1,:)))
xlabel(_("time (s)"))
ylabel(_("RR (s)"))
    ]]></programlisting>
    <scilab:image><![CDATA[
A=wfdbIsqrs(CWApath()+"demos/datafiles/sel100.pu1");
//previous instruction is equivalent to
//ann=wfdbReadAnnotations(CWApath()+"demos/datafiles/sel100","pu1");
//A=wfdbIsqrs(ann);

[sig,fe]= wfdbReadSamples(CWApath()+"demos/datafiles/sel100");
tmax=70;
kRp=A(1,:)*fe;
t=(0:size(sig,2)-1)/fe;
clf;subplot(211)
plot(t,sig(2,:),"b",t(kRp),sig(2,kRp),"+r");
set(gca(),"zoom_box",[10 4.5 100 6 -1 1]);
subplot(212)
plot(A(1,1:$-1),diff(A(1,:)))
xlabel(_("time (s)"))
ylabel(_("RR (s)"))

set(gcf(),"axes_size", [610,460])

    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="wfdbReadAnnotations">wfdbReadAnnotations</link>
      </member>
      <member>
        <link linkend="wfdbAnnot2String">wfdbAnnot2String</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         This function is derived from <ulink
         url="http://www.physionet.org/physiotools/matlab/wfdb-subset/old/isqrs.m">http://www.physionet.org/physiotools/matlab/wfdb-subset/old/isqrs.m</ulink>
         due to Salvador Olmos.
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
  
</refentry>
