<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the CWA  toolbox
Copyright (C) 2014 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="wfdbReadAnnotations" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 20-04-2012 $</pubdate>
  </info>

  <refnamediv>
    <refname>wfdbReadAnnotations</refname>

    <refpurpose>reads the annotation of a WFDB record.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[ann, aux]= wfdbReadAnnotations(record_name,annotator [,time_bounds])</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>record_name</term>

        <listitem>
          <para>A character string, the name of the WFDB database record
          (without extension)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>annotator</term>

        <listitem>
          <para>A character string, the extension of annotation file. Should
          be "atr","qrs",...</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>time_bounds</term>

        <listitem>
          <para>A two elements vector [begin_time, end_time] used to select a
          time range. If omitted or set to an empty array all the annotations
          are returned. The arguments begin_time and end_time must be given in
          seconds.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>ann</term>

        <listitem>
          <para>A floating point array, with 5 rows. The first row contains
          the annotation dates, the second row contains the annotation code
          (see <link linkend="annot2string">annot2string</link> function). The
          following rows contains the annotation subtyp, chan, and num
          fields;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>aux</term>

        <listitem>
          <para>A character string array,contains the auxiliary information
          strings</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function reads the annotation of a WFDB record.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <para>The following instructions:</para>

    <programlisting role="example"><![CDATA[
    record=CWApath()+"demos/datafiles/sel100";
    time_bounds=[0.5 1.4]; //in seconds
    ann=wfdbReadAnnotations(record,"pu0",time_bounds);
    [sig,fe,n]=wfdbReadSamples(record,%f,time_bounds);
    t=time_bounds(1)+(0:size(sig,2)-1)/fe;
    //shift the annotation dates
    Na=size(ann,2);
 
    clf; drawlater()
    plot(t',sig(1,:)');
    xsegs([ann(1,:);ann(1,:)],[4*ones(1,Na);7*ones(1,Na)],5)
    A=wfdbAnnot2String(ann(2,:));
    for k=1:Na
      xstring(ann(1,k),6,A(k));
    end
    drawnow()
     ]]></programlisting>

    <para>Produces this graph</para>

    <scilab:image><![CDATA[
    record=CWApath()+"demos/datafiles/sel100";
    time_bounds=[0.5 1.4]; //in seconds
    ann=wfdbReadAnnotations(record,"pu0",time_bounds);
    [sig,fe,n]=wfdbReadSamples(record,%f,time_bounds);
    t=time_bounds(1)+(0:size(sig,2)-1)/fe;
    //shift the annotation dates
    Na=size(ann,2);
 
    clf; drawlater()
    plot(t',sig(1,:)');
    xsegs([ann(1,:);ann(1,:)],[4*ones(1,Na);7*ones(1,Na)],5)
    A= wfdbAnnot2String(ann(2,:));
    for k=1:Na
      xstring(ann(1,k),6,A(k));
    end
    drawnow()
     ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="wfdbAnnot2String">wfdbAnnot2String</link> <link
      linkend="wfdbReadSamples">wfdbReadSamples</link></member>
    </simplelist>
  </refsection>

  <refsection>
     

    <title>Reference</title>

     This function is based on the 

    <ulink url="http://www.physionet.org/physiotools/wfdb.shtml">WFDB
    library</ulink>

    . 
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
</refentry>
