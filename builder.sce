mode(-1);
// This file is part of the Dynpeak toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function main_builder()
  version_numbers = getversion("scilab")
  TOOLBOX_NAME  = "CWA";
  TOOLBOX_TITLE = "Cardiovascular Wave analyser";
  toolbox_dir   = get_absolute_file_path("builder.sce");
  // Check modules_manager module availability
  // =============================================================================

  if ~isdef('tbx_build_loader') then
    error(msprintf(_("%s module not installed."), 'modules_manager'));
  end
  if version_numbers(1)<6 then
    error(msprintf(_("This module must be built with Scilab-6.0 or higher.")));
  end
  // Action
  // ========================================================================

  tbx_builder_macros(toolbox_dir);
  tbx_builder_src(toolbox_dir);
  tbx_builder_gateway(toolbox_dir);
  tbx_build_loader(toolbox_dir);
 
  //load functions to be able to run help examples
  CWAlib = lib(toolbox_dir+"macros");
  HRVASlib=lib(toolbox_dir+"macros/HRVAS");
  ieee(2);
  exec(pathconvert(toolbox_dir+"/src/c/loader_ecgsyn.sce", %f));
  exec(pathconvert(toolbox_dir+"sci_gateway/loader_gateway.sce", %f));
  tbx_builder_help(toolbox_dir);
  tbx_build_cleaner(toolbox_dir);
endfunction

main_builder();
clear main_builder; // remove main_builder on stack
