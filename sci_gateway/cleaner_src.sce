mode(-1)
// This file is part of the wfdb toolbox
// Copyright (C) 2013 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function cleaner_src()
  src_path = get_absolute_file_path("cleaner_src.sce");
  l=ls(src_path+"/c/lib*")
  for i=1:size(l,"*")
    deletefile(l(i))
  end
  deletefile(src_path+"/c/date_build")
endfunction

cleaner_src();
clear cleaner_src; // remove builder_src on stack


