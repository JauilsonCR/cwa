// This file is part of the CWA toolbox
// Copyright (C) 2014 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function builder_gw_c()
  path= get_absolute_file_path("builder.sce");
  srclib_path=strsubst(path,"sci_gateway","src");
  WITHOUT_AUTO_PUTLHSVAR = %t;
  
  src_files_levkov=["sci_Levkov.c","sci_Levkov_f.c"];
  src_files_wfdb=["sci_rdann.c" "sci_rdsamp.c" "sci_MIT2Ecgs.c"];
  src_files_Lomb="sci_Lomb.c"
  src_files_SampEn="sci_SampEn.c"
  inc_files_wfdb=["ecgcodes.h","ecgmap.h","wfdb.h","wfdblib.h"];
  libs="../../src/c/libCWA"
  CFLAGS = ilib_include_flag(srclib_path)
  src_files=[src_files_wfdb src_files_levkov src_files_Lomb src_files_SampEn];
  inc_files=inc_files_wfdb;
  if newest(path+["date_build","builder.sce",src_files,srclib_path+inc_files])<>1 then 
    curdir=pwd();
    try
      if getos()=="Windows" then
        srclib_path=strsubst(srclib_path,'\','/');
        libs=[libs SCI+"\bin\fileio"];
      end
      table =["wfdbReadAnnotations","sci_rdann";
              "wfdbReadSamples","sci_rdsamp";
              "MIT2Ecgs" "sci_MIT2Ecgs"
              "Levkov" "sci_Levkov";
              "Levkov_f" "sci_Levkov_f"
              "Lomb" "sci_Lomb"
              "SampEn" "sci_SampEn"];
      tbx_build_gateway("sciCWA",table,..
                        src_files, ..
                        path,libs,"",CFLAGS);
      //replace the generated loader by a custom one (independent of the
      //installation path)
      mputl(mgetl(path+"loader_ref.sce"),path+"loader.sce")
      mputl(sci2exp(getdate()),path+"date_build")
    catch
      cd(curdir);
      mprintf("%s\n",lasterror());
    end
  end
endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
