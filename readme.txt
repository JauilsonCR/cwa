Scilab Cardiovascular Wave Analysis toolbox.  
Version 1.0  april 2015

This toolbox replaces and extends the Cardiovascular Toolbox
(http://atoms.scilab.org/toolboxes/Cardiovascular)

It provides tools for cardiovascular signal analysis:
- ECG reading multi channel ECG files in various formats (ISHNE, MIT, TMS32)
- handling huge ECG files obtained through Holter devices
- multi channel ECG visualization
- ECG pretreatment (filtering, subsampling, power line interference
  removal, base line wander removal)
- ECG events detection (P, Q, R, S, T) peaks, onset and end
- Time frequency tools for frequency evolution estimation
- Cardiovascular signals (RR, SBP, Vt) analysis using complex demodulation
- Cardiovascular  signals (RR, SBP, Vt) analysis using 
  Smoothed pseudo Wigner Ville decomposition.
- Baroreflex analysis based on short term fourier transform
- Specialized plotting facilities
- Graphical user interface for analysis (HRVAS, ECGViewer, TimeFrequencyTool)
See the FunctionList file for more details

Requirements: 
  - Scilab 5.5.1 or newer 
  - stftb Scilab module 1.2.2 or newer
  - swt Scilab module 0.2.1-1  or newer
  
The toolbox has been developped and tested by
Serge Steer, INRIA
Claire Médigue, INRIA
Lisa Guigue, INRIA


It is partly  derived from the LARY_CR package
(https://hal.inria.fr/inria-00069915) developped of the following members of the 
INRIA team SISYPHE:
  Claire Medigue, 
  Alessandro Monti, 
  Qinghua Zhang,
  Michel Sorine 


Toolbox maintener:  Serge Steer, Serge.Steer@inria.fr


Thanks to François Cottin from "Unité de biologie intégrative des
 adaptations à l'exercice (INSERM 902/EA)"  for data, advices and tests.
