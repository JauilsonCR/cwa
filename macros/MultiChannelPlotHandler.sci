//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function MultiChannelPlotHandler(win, x, y, ibut)
//Utilitary function for MultiChannelPlot function
  if and(ibut<>[37 39 113]) then return,end
  fig=get_figure_handle(win)
  fig.event_handler_enable='off';
  ud=fig.user_data;
  zoom_box=[]
  C=fig.children;
  C=C(C.type=="Axes");
  drawlater()
  select ibut
 
  case 37 then //scroll left
    l=ud.scroll(2);
    count=ud.scroll(1);
    xbounds=ud.scroll(3:4);

    if count>0 then
      for k=1:size(C,'*')
        ax=C(k);
        P=ax.children(ax.children.type=="Polyline")
        if P<>[] then
          zb([1 3])=xbounds(1)+(count-1)*l+[0 l];
          zb([2 4])=selection_y_bounds(P,zb([1 3]))
          ax.zoom_box=zb;
          if ax.auto_ticks(1)=="off" then
            ax.auto_ticks(1)="on";//used to recompute grids positions 
            ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
          end
        end
      end
      ud.scroll(1)=count-1
    end
  case 39 then //scroll right
    l=ud.scroll(2);
    count=ud.scroll(1);
    xbounds=ud.scroll(3:4);
    if xbounds(1)+(count+1)*l<xbounds(2) then
      for k=1:size(C,'*')
        ax=C(k)
        P=ax.children(ax.children.type=="Polyline")
        if P<>[] then
          zb([1 3])=xbounds(1)+(count+1)*l+[0 l];
          zb([2 4])=selection_y_bounds(P,zb([1 3]))
          ax.zoom_box=zb;
          if ax.auto_ticks(1)=="off" then
            ax.auto_ticks(1)="on";//used to recompute grids positions 
            ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
          end
        end
      end
      ud.scroll(1)=count+1
      
    end
  case 113 then //initial view
    drawlater(); 
    for k=1:size(C,'*')
      ax=C(k)
      ax.zoom_box=[];
      if ax.auto_ticks(1)=="off" then
        ax.auto_ticks(1)="on";//used to recompute grids positions 
        ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
      end
    end
    drawnow()
    ud.scroll(1)=0
  end
  drawnow()
  set(fig,"user_data",ud)
  fig.event_handler_enable='on';
endfunction

function ax=findSubwin(x,y,fig)
  sz=fig.axes_size;
  ax=[];
  C=fig.children(fig.children.type=="Axes");
  for k=1:size(C,'*')
    ax=C(k)
    bounds=ax.axes_bounds.*[sz sz]
    if x>=bounds(1)&x<(bounds(1)+bounds(3))&y>=bounds(2)&y<(bounds(2)+bounds(4)) then
      break
    end
  end
endfunction


function y_bounds=selection_y_bounds(P,x_bounds)
  ymin=%inf
  ymax=-%inf
  for k=1:size(P,'*')
    d=P(k).data
    sel=find(d(:,1)>=x_bounds(1)&d(:,1)<=x_bounds(2))
    ymin=min(ymin,min(d(sel,2)))
    ymax=max(ymax,max(d(sel,2)))
  end
  ymin=max(ymin,ax.data_bounds(1,2))
  y_bounds=[max(ymin,ax.data_bounds(1,2)),min(ymax,ax.data_bounds(2,2))];
endfunction

