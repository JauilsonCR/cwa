// This file is part of the CardioVascular  wave analysis toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// Copyright (C) - INRIA - Alessandro Monti, Claire Médigue, Michel Sorine
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function options=check_npmcnssa_options(options,fname)
//internal function used to check and fill the options
  err_msg3=_("%s: Wrong type for field %s of input argument #%d: A real positive number expected.\n");
  err_msg5=_("%s: Wrong value for field %s of input argument #%d: A power of two expected.\n")
  err_msg6=_("%s: Wrong value for input argument #%d: A struct with fields %s expected.\n")

  //default values
  defaults.sectionlength=256
  defaults.sectionstep=[] //50% overlap
  defaults.smoothwindowlength=5;
  defaults.minimalcoherence=0.5;
  
  if typeof(options)=="st" then
    legal_fields=["sectionlength","sectionstep","smoothwindowlength","minimalcoherence"];
    fn=fieldnames(options)
    //check given options
    for k=1:size(fn,'*')
      select fn(k)
      case "sectionlength" then
        //length of batch section
        o=options(fn(k));
        if type(o)<>1|~isreal(o)|size(o,'*')<>1|o<1 then 
          error(msprintf(err_msg3,fname,fn(k),5))
        end
      case "sectionstep"  then
        //shift from batch to batch
        o=options(fn(k));
        if o==[] then continue,end
        if type(o)<>1|~isreal(o)|size(o,'*')<>1|o<1 then 
          error(msprintf(err_msg3,fname,fn(k),5))
        end
      case "smoothwindowlength"  then
        //smoothing window length
        o=options(fn(k));
        if type(o)<>1|~isreal(o)|size(o,'*')<>1|o<1 then 
          error(msprintf(err_msg3,fname,fn(k),5))
        end
      case "minimalcoherence"  then
        //minimal coherence
        o=options(fn(k));
        if type(o)<>1|~isreal(o)|size(o,'*')<>1|or(o<=0) then 
          error(msprintf(err_msg3,fname,fn(k),5))
        end
      end
    end
    //add default options
    for k=1:size(legal_fields,'*')
      if and(legal_fields(k)<>fn) then
        options(legal_fields(k))=defaults(legal_fields(k))
      end
    end
    if options.sectionstep==[] then options.sectionstep= options.sectionlength/2,end
  else
    error(msprintf(err_msg6,fname,5,strcat(legal_fields,",")))
  end
endfunction
