// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com
// This original version of this file (nonlinearHRV.m) is part of HRVAS
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License.
// If not, see <http://www.gnu.org/licenses/>.

function [e,A,B]=SampEn1(y,M,r)
//function e=sampen(y,M,r);
//
//Input Parameters
//
//   y:  input signal vector
//   M:  maximum template length (default m=5)
//   r:  matching threshold in standard deviations (default r=0.2)
//       the threshold used is r*stdev(y)
//
//Output Parameters
//
//   e sample entropy estimates for m=0,1,...,M-1
//   A number of matches for m=1,...,M
//   B number of matches for m=0,...,M-1
//   
//   Derived from sampen.m
//   Ref: http://www.physionet.org/physiotools/sampen/matlab/1.1/

  if type(y)<>1 then
     error(msprintf(_("%s: Wrong type for input argument #%d: Array of reals expected.\n"),"SampEn",1))
  end
  if argn(2)<3 then 
    r=0.2;
  else
    if type(r)<>1|size(r,"*")<>1|r<=0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive real expected.\n"),"SampEn",3))
    end
  end
  if argn(2)<2 then 
    r=0.2;
    M=5;
  else
    if type(M)<>1|size(M,"*")<>1|M<=0|int(M)<>M then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive integer expected.\n"),"SampEn",2))
    end
  end
  y=y(:);
  y(isnan(y))=[];
  n=size(y,'*');
  //normalize 
  r=r*stdev(y);
  //
  lastrun=zeros(1,n);
  run=zeros(1,n);
  A=zeros(M,1);
  B=zeros(M,1);

  for i=1:(n-1)
    nj=n-i;
    y1=y(i);
    jj=find(abs(y(i+1:n)-y(i))<r);
    if jj<>[] then
      run(jj)=run(jj)+1
      M1=min(M,run(jj));   
      for m=M1,A(1:m)= A(1:m)+1;end
      if jj($)==n-i then M1($)=[];end
      for m=M1,B(1:m)= B(1:m)+1;end 
      jj_ko=1:n;jj_ko(jj)=[] ;
      run(jj_ko)=0;
    else
      run=zeros(1,n);
    end
  end
  B=[n*(n-1)/2;B(1:(M-1))];

  kz=find(A==0)
  if kz<>[] then
    error(msprintf(_("%s: argument #%d is too small, there is no matche for template length greater  than %d\n"),"SampEn",3,kz(1)-1))
  end
  e=-log(A./B);

endfunction
