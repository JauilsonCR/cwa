function outliers=threshFilter(s,thresh)
//threshFilter: Locate outliers based on a threshold
  // Create a matrix of boolean, where %t indicate the location of outliers
    outliers = s < thresh(1)| s > thresh(2);

endfunction
