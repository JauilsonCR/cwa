// This file is part of the CardioVascular wave analysis toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// Copyright (C) - INRIA - Alessandro Monti, Claire Médigue, Michel Sorine
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [IFreq,IAmp,Iphase,delay,f0]=ComplexDemodulation(signal,sigref,LP)
//[IFreq,IAmp,Iphase,delay,f0]=ComplexDemodulation(signal,sigref [,LP])
//signal  : a real vector. The signal to be demodulated. It should be a
//          narrow band signal
//sigref  : a real vector. The signal which give the reference
//          instantaneous frequency. It should be a narrow band signal
//LP      : an optionnal  real vector. The coefficients of a
//            low pass FIR filter used to reject frequencies which are away
//            from reference. Filter coefficients should correspond to
//            normalized frequencies
//IFreq   : row vector of the instantaneous frequency. Its length is the
//          signal length plus (hforder-1)/2
//IAmp    : row vector of the instantaneous Amplitude. Its length is the
//          signal length plus (hforder-1)/2
  
//IPhase  : row vector of the instantaneous Phase difference. Its length is
//          the signal length plus (hforder-1)/2
//f0      : row vector of the instantaneous frequency of the refernec signal. 
//          Its length is the signal length plus (hforder-1)/2
//delay    : global delay  introduced by the hilbert, lowpass and derivator filters. 
//           IFreq, IAmp, IPhase,  f0 are delayed by delay time steps
//           with respect to the given signal.
  
  err_msg1=_("%s: Wrong type for input argument #%d: A real vector of float expected.\n");
  err_msg2=_("%s: Wrong dimension for input argument #%d: An odd value expected.\n");

  //Input signals check
  if type(signal)<>1|~isreal(signal)|and(size(signal)>1) then 
    error(msprintf(err_msg1,"ComplexDemodulation",1))
  end
  signal=matrix(signal,1,-1);
  if type(sigref)<>1|~isreal(sigref)|and(size(sigref)>1) then 
    error(msprintf(err_msg1,"ComplexDemodulation",2))
  end
  if size(sigref,'*')>1  then
    sigref=matrix(sigref,1,-1)
    if size(sigref,2)<>size(signal,2) then
      err_msg=_("%s: Incompatible length for input arguments #%d and #%d.\n")
      error(msprintf(err_msg1,"ComplexDemodulation",1,2))
    end
    refIsSignal=%t
  else
    refIsSignal=%f
  end


  //Default tuning parameters
  LPwl=195; //Low pass filter window length 
  LP=wfir("lp",LPwl,[0.01 0],"hm",[0 0]);
  LP=LP/sum(LP);//normalize to get a unit gain at 0 frequency 
  if argn(2)==3 then
    //Check user defined tunning parameters
  
    if type(LP)<>1|~isreal(LP)|and(size(LP)>1) then 
      error(msprintf(err_msg1,"ComplexDemodulation",3))
    end
    if modulo(size(LP,'*'),2)<>1 then
      error(msprintf(err_msg2,"ComplexDemodulation",3))
    end
    LP=LP/sum(LP);//normalize to get a unit gain at 0 frequency 

  end
  //Compute the signal delay introduced by the filters
  delay= (LPwl-1)/2;
  
  //extend the signal
  signal($+1:$+delay)=signal($);

  //form analytic signal 
  signal_z=hilbert(signal);
  
  
  if refIsSignal then
    //extract f0(t) out of the reference signal. f0(t) is supposed to
    //evolve slowly.
    sigref($+1:$+delay)=sigref($);
    sigref_z=hilbert(sigref);

    //phase of the slowly changing signal
    sigref_z(abs(sigref_z)==0)=1e-10; //to avoid a zero divide
    sigref_phi=phasemag(sigref_z,'c')*%pi/180;
    
    //instantaneous frequency of the reference signal filtered derivative
    //of the phase
    f0=filter(LP,1,diff([sigref_phi sigref_phi($)]))/(2*%pi);
    //shift signal frequency by -f0
    signal_z_shifted=%i*signal_z.*exp(-%i*sigref_phi);
 
  else //sigref contains f0 (in normalized frequency)
    f0=sigref;
    n=size(signal_z,'*');
    //shift signal frequency by -f0  
    signal_z_shifted=signal_z.*exp(-2*%i*%pi*f0*(0:(n-1)));
  end
  //low pass filtering of shifted signal
  signal_z_shifted=(filter(LP,1,real(signal_z_shifted))+%i*filter(LP,1,imag(signal_z_shifted)));

  signal_z_shifted(abs( signal_z_shifted)==0)=1e-10; //to avoid a zero divide
  //compute the instantaneous phase difference
  Iphase=phasemag(signal_z_shifted,'c')*%pi/180;
  Iphase=Iphase-Iphase(delay)
  //compute the instantaneous signal amplitude
  IAmp=abs(signal_z_shifted);
 //instantaneous frequency difference of the analyzed signal
  IFreq=f0+filter(LP,1,diff([Iphase Iphase($)]))/(2*%pi);
 

endfunction

