//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [s,f]=easyspec(x,fs)
//EASYSPEC Easy  spectrum estimate
//         S=EASYSPEC(X) will return a spectrum vector of
//         X. [S,F]=EASYSPEC(X,FS) will also return a frequency
//         axis in F, while the sample frequency is given in FS.
//         
//         Notes:
//         1. Spectrum values are in dB.
//         2. The frequency domain goes from DC to the Nyquist 
//            frequency. The "mirror part" of the spectrum is
//            omitted.
//         3. The sample segments, from which the spectrum is
//            calculated are picked by random. The function might
//            return significantly different results each time it's
//            called to if X isn't stationary.
//         4. EASYSPEC uses a hanning window and zero-pads by a
//            factor of 4. The spectrum vector will look smooth.

// Eli Billauer, 17.1.01 (Explicitly not copyrighted).
// This function is released to the public domain; Any use is allowed.

if argn(2)<2 then
  error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),"easyspec",2))
end

x=matrix(x,1,-1);
NFFT=16384; NWIN=NFFT/4;
LOOP=100;
win=window("hn",NWIN);

x=x*(17.127/NFFT/sqrt(LOOP));
n=length(x);
maxshift=n-NWIN;

if (n<2*NWIN)
  error(msprintf(_("%s: Wrong number of elements in input argument #%d: At least %d elements expected, but current number is %d.\n"),"easyspec",1,2*NWIN,n))
end

s=zeros(1,NFFT);

for i=1:LOOP
  zuz=floor(rand(1,1,'unif')*maxshift);
  s=s+abs(fft([win.*x(1+zuz:NWIN+zuz) zeros(1,NFFT-NWIN)])).^2;
end

s=10*log10(s(1:NFFT/2));
f=linspace(0,fs/2,NFFT/2);

endfunction
