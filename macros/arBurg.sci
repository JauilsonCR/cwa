function [a,v,rc,FPE,AIC]=arBurg(x,order);
// Burg method
// a: AR coefficients
// v: variance of white noise
// rc: reflection coeffients for use in lattice filter 
// FPE: final prediction error
// AIC: Akaike information criterion
  if argn(2)<>2 then 
    error(msprintf(_("%s: Wrong number of input argument: %d  expected.\n"),"pBurg",2))
  end
  if type(x)<>1|~isreal(x)|and(size(x)>1) then
    error(msprintf(_("%s: Wrong type for argument %d: Real vector expected.\n"),"pBurg",1))
  end
   if type(order)<>1|size(order,"*")>1|int(order)<>order|order<0 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A positive integer expected.\n"),"pBurg",2))
  end
  x=x(:);
  N=length(x);
  a=zeros(1,order); 
  FPE=zeros(1,order);
  AIC=zeros(1,order);
  
  b1=x; 
  b2=b1;
  aa=a;
  v = (x'*x)/N; 
  for m=1:order
    bb1=b1(2:$);
    bb2=b2(1:$-1);
    //next order reflexion coefficient
    a(m)= -2*bb1'*bb2/(bb1'*bb1+bb2'*bb2);
    rc(m)=a(m);
    //Update the forward and backward prediction errors
    b1=bb1+a(m)*bb2;
    b2=bb2+a(m)*bb1;
    if m>1 then
      k=1:m-1; a(k)=aa(k)+a(m)*aa(m-k);
    end
    v=v*(1-a(m)^2);
    aa = a;
    FPE(m)=(N+m+1)/(N-m-1)*v;
    AIC(m)=N*log(v)+2*m;

  end
  a=[1,a];
endfunction
