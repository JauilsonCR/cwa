//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [kTe,typ]=decide(Tval)
  
  global ok,ok=%t
  ntv=size(Tval,'*');
  nf=floor(ntv/10)
  typ=''
  [dum, maxind] = max(Tval);
  [duminv, maxindinv] = max(-Tval);
  [kTe,typ]=QZ_decide(Tval)
  traceD()
  return
  
  if maxind==1 then
     ok=%t;
     if nf>3 then
       dTval=sgolaydiff(Tval,1,3,nf);
     else
       dTval=diff(Tval)
     end
    //premier min local
    kmn=find(dTval(1:$-1)<0&dTval(2:$)>0,1);
    if kmn==[] then
      //tester passage de la derivée au voisinage de 0?
      //aller a l'algo QZ?
      kTe=ntv+ leftbound - 1
    elseif Tval(kmn)<0 then
      //peut etre le min asolu dans ce cas sel31 1/13
      kTe=kmn + leftbound - 1
    else
      //max local suivant peut etre ne tester que le passage de la derivée
    //au voisinage de 0 (0.02) 0.08? ou min local de la derivée
      kmx=find(dTval(kmn:$-1)>0&dTval(kmn+1:$)<0,1)
      if kmx==[] then
        //disp ici1;pause
        //non on est sur un min local la derivee est proche de 0 if faut
        //chercher d'abord un max local de la derivée
        kmx=find(dTval(kmn:$)<0.05,1)
        if kmx==[] then kmx=ntv-kmn+1;end
      end
      kmx=kmx+kmn-1 
      kTe=kmx + leftbound - 1
    end
  elseif maxindinv==1 then
    ok=%t;
    if nf>3 then
      dTval=sgolaydiff(Tval,1,3,nf);
    else
      dTval=diff(Tval)
    end
    //premier max local, 
    //peut etre ne tester que le passage de la derivée
    //au voisinage de 0 (0.02)
    kmx=find(dTval(1:$-1)>0&dTval(2:$)<0,1);
    if kmx==[] then
      //disp ici2;pause
      kmx=find(dTval<0.05,1)
      if kmx==[] then 
        //ou debrancher vers QZ
        kmx=ntv;
      end
    end
    kTe=kmx + leftbound - 1
  else
    ok=%f;
    [kTe,typ]=QZ_decide(Tval)
  end
  traceD()
  global ok;ok=%f
endfunction

