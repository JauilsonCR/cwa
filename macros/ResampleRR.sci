// This file is part of the CardioVascular  wave analysis toolbox
// Copyright (C)  - INRIA - Alessandro Monti
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [tn,RRn]=ResampleRR(RR,varargin)
  //Resamples the discrete signal (t(k),x(k)) using step as the step size
  //and cubic spline interpolation
  if type(RR)<>1|~isreal(RR)|and(size(RR)>1) then 
    error(msprintf(_("%s: Wrong type for argument %d: Real vector expected.\n"),"ResampleRR",1))
  end
  
  s=size(RR); RR=RR(:);
  t=matrix([0;cumsum(RR(1:$-1))],s)
  [tn,RRn]=Resample(t,RR,varargin(:))
endfunction
