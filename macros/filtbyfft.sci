//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function sig = filtbyfft(ftype,sig, fs, passband, tf)
//FILTBYFFT: filter by FFT
//
// FilteredSig = filtbyfft(ftype,Sig, Fs, Passband, TF)
//
// FilteredSig: filtered signal vector,
// Sig: signal vector,
// Fs: sampling frequency (in Hz),  
// Passband: passband interval (in Hz),
// TF: signal domain indicator, a string containg two characters, each being
//     of 't' or 'f'(time or frequency), the first one for SIG and the
//     second one for FilteredSig 
//
// Author: Qinghua Zhang
// Copyright 2008 INRIA
  nargin=argn(2);
  if nargin<4 then
    error(msprintf(_("%s: Wrong number of input argument: At least %d"+...
                     " expected.\n"),"filtbyfft",4))
  end
  if nargin<5 then
    tf = 'tt';
  elseif and(tf<>['tt' 'tf','ft','ff']) then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),"filtbyfft",5,"""tt"", ""tf"",""ft"",""ff"""))
  end
  

  if and(ftype<>["lp","hp","bp","sb"]) then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),"filtbyfft",1,"""lp"", ""hp"",""bp"",""sb"""))
  end

  if type(sig)<>1 then
    error(msprintf(_("%s: Wrong type for argument #%d: array of double expected.\n"),"filtbyfft",2))
  end
  if and(size(sig)>1) then
    error(msprintf(_("%s: Wrong size for argument #%d: Vector expected.\n"),"filtbyfft",2))
  end
  
  if length(passband)<>2
    error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),"filtbyfft",4,2))
  end
  if passband(1)>passband(2) then
    error(msprintf(_("%s: Wrong values for input argument #%d: Elements must be in increasing order.\n"),"filtbyfft",4))
  end
  passband(1) = max(passband(1), 0);
  passband(2) = min(passband(2), 0.5*fs);

  N = length(sig);

  if part(tf,1)=='t' then sig = fft(sig);end

  lowicut = round(passband(1)*N/fs);
  lowmirror = N-lowicut+2;

  highicut =  round(passband(2)*N/fs);
  highmirror = N-highicut+2;
  select ftype
  case "lp" 
    sig((highicut+1):(highmirror-1))=0;
  case "hp"
    sig(1:(lowicut-1))=0;sig((lowmirror+1):$)=0;
  case "bp"
    sig(1:(lowicut-1))=0;sig((lowmirror+1):$)=0;
    sig((highicut+1):(highmirror-1))=0;
  case "sb"
    sig(lowicut:highicut)=0
    sig(highmirror:lowmirror)=0
  end
  if part(tf,2)=='t' then sig = real(ifft(sig));end
endfunction
