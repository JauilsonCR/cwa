// This file is part of the CardioVascular wave analysis toolbox
// Copyright (C)  - INRIA - Alessandro Monti
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [tn,xn]=Resample(t,x,step,tbounds)
  //Resamples the discrete signal (t(k),x(k)) using step as the step size
  //and cubic spline interpolation
  if type(t)<>1|~isreal(t)|and(size(t)>1) then 
    error(msprintf(_("%s: Wrong type for argument %d: Real vector expected.\n"),"Resample",1))
  end
  if or(diff(t)<=0) then
     error(msprintf(_("%s: Elements of %dth argument must be in increasing order.\n"),"Resample",1))
  end
  if type(x)<>1|~isreal(x) then 
    error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),"Resample",2))
  end
  if and(size(x)>1) then //matrix case 
    if size(x,1)<>size(t,'*') then
       error(msprintf(_("%s: Wrong size for input arguments %d and %d: Incompatible dimensions.\n"),"Resample",1,2))
    end
  else
    if size(x,'*')<>size(t,'*') then
      error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Same sizes expected.\n"),"Resample",1,2))
    end
  end
 
  if type(step)<>1|~isreal(step)|or(size(step)<>1)|step<=0 then 
    error(msprintf(_("%s: Wrong type for argument %d: Real positive scalar expected.\n"),"Resample",3))
  end
  
  if argn(2)<4 then 
    tbounds=[t(1),t($)]
  else
    if type(tbounds)<>1|~isreal(tbounds)|size(tbounds,'*')<>2 then 
      error(msprintf(_("%s: Wrong type for argument %d: Real vector expected.\n"),"Resample",4))
    end
    if size(tbounds,'*')<>2 then 
      error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),"Resample",4,2))
    end
    if tbounds(1)<t(1)| tbounds(2)>t(2)|tbounds(1)>tbounds(2) then
      error(msprintf(_("%s: Wrong value for input argument #%d.\n"),"Resample",4))
    end
  end
  tn=tbounds(1):step:tbounds(2);
  szt=size(t)
  if and(size(x)>1) then
    t=matrix(t,-1,1);
    xn=zeros(size(tn,'*'),size(x,2))
    for j=1:size(x,2)
      d=splin(t,x(:,j));
      xn(:,j)=interp(tn,t,x(:,j),d);
    end
  else
    szt=size(t)
    x=matrix(x,1,-1);
    t=matrix(t,1,-1);
    d=splin(t,x);
    xn=interp(tn,t,x,d);
    if szt(1)<>1 then xn=matrix(xn,-1,1),end
  end
endfunction
