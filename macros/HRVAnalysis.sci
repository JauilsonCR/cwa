function HRVAnalysis(RR)
//Heart rate variation analysis by the ellipse method
//SD1 : standard deviation of points perpendicular to the axis of the
//      line of identity
//SD2:  standard deviation  of points along the axis of the
//      line of identity 
  m1=mean(RR(1:$-1));
  m2=mean(RR(2:$));
  sd=0.5*stdev(diff(RR))^2
  SD1=sqrt(sd)
  SD2=sqrt(2*stdev(RR)^2-sd);
endfunction
