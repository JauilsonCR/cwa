//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ECGViewer(ecg,LOC)
// ecg : file path or an array
  if argn(2)==0 then
    error(msprintf(_("%s: Wrong number of input argument(s): %d to %d expected.\n"),"ECGViewer",1,2))
  end
  if typeof(ecg)<>"sciecg"&(type(ecg)<>10|size(ecg,'*')<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d:\n a sciecg data structure or a path to a Scilab ECG file expected\n."),"ECGViewer",1))
  end
  if argn(2)<2 then 
    LOC=[];
  else
    if type(LOC)<>1|~isreal(LOC) then
      error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n","ECGViewer",2)))
    end
    if size(LOC,1)<>9 then
      error(msprintf(_("%s: Wrong size for input argument #%d: row dimension must be equal to %d.\n","ECGViewer",9)))
    end
  end
  ud= ECGViewerOpen(ecg,LOC)
  fig=ECGViewerCreateGui(ud,LOC)
 
  for k=2:size(ECGViewerMenuNames(),1)
    setmenu(fig.figure_id,"ECGViewer",k);
  end
  setmenu(fig.figure_id,">")
  setmenu(fig.figure_id,"<")
  setmenu(fig.figure_id,">>")
  setmenu(fig.figure_id,"<<")

endfunction

function fig=ECGViewerCreateGui(ud,LOC)
  fig=gcf();clf;
  delmenu(fig.figure_id,">")
  delmenu(fig.figure_id,">>")
  delmenu(fig.figure_id,"<<")
  delmenu(fig.figure_id,"<")
  delmenu(fig.figure_id,"ECGViewer")
  menus=ECGViewerMenuNames();
  addmenu(fig.figure_id,"ECGViewer",menus, list(2,"ECGViewerMenus"))
  for k=2:size(menus,1),unsetmenu(fig.figure_id,"ECGViewer",k);end
      
  addmenu(fig.figure_id,">>", list(2,"ECGViewerNext")) 
  unsetmenu(fig.figure_id,">>")
  addmenu(fig.figure_id,">", list(2,"ECGViewerMoveRight")) 
  unsetmenu(fig.figure_id,">")
  addmenu(fig.figure_id,"<<", list(2,"ECGViewerPrev")) 
  unsetmenu(fig.figure_id,"<<")
  addmenu(fig.figure_id,"<", list(2,"ECGViewerMoveLeft")) 
  unsetmenu(fig.figure_id,"<")
  fig.closerequestfcn="ECGViewerGUIClose";
  
  ax1=gca();
  ax1.y_label.text="mv";
  ax1.axes_visible="on";
  ax1.x_label.text="time (s)"
  ax1.axes_bounds=[0 0 1 1];
  ax1.tight_limits="on";
  //axes for segments length display
  ax2=newaxes();
  ax2.axes_visible='on'
  ax2.axes_bounds=[0 0.5 1 0.5];
  ax2.y_label.text="ms";
  ax2.visible="off";
  ax2.tight_limits="on";
  ud.ax1=ax1;
  ud.ax2=ax2;
  set(fig,"user_data",ud);
endfunction

version_numbers = getversion('scilab')

function ECGViewerNext(k,fig_id,k)
  fig=get_figure_handle(fig_id)
  ud=fig.user_data;
  ud.Index=min(ud.Index+ud.BlockLength,ud.Nsamples)
  set(fig,"user_data",ud)
  ECGViewerMenus(_("Show"),fig_id)
endfunction
function ECGViewerPrev(k,fig_id)
  fig=get_figure_handle(fig_id)
  ud=fig.user_data;
  ud.Index=max(1,ud.Index-ud.BlockLength)
  set(fig,"user_data",ud)
  ECGViewerMenus(_("Show"),fig_id)
endfunction
function ECGViewerMoveRight(k,fig_id)
  fig=get_figure_handle(fig_id)
  ud=fig.user_data;
  ns2=ud.BlockLength/2
  ud.Index=min(ud.Index+ns2,ud.Nsamples)
  set(fig,"user_data",ud)
  ECGViewerMenus(_("Show"),fig_id)
endfunction
function ECGViewerMoveLeft(k,fig_id)
  fig=get_figure_handle(fig_id)
  ud=fig.user_data;
  ns2=ud.BlockLength/2
  ud.Index=max(1,ud.Index-ns2)
  set(fig,"user_data",ud)
  ECGViewerMenus(_("Show"),fig_id)
endfunction

function ECGViewerGUIClose(win,x,y,ibut)
  fig=gcbo;
  ud=fig.user_data
  if type(ud)==16&ud.fid<>[] then mclose(ud.fid);end
  delete(fig)
endfunction




function ud=ECGViewerOpen(f,LOC)
  if type(f)==10 then
    [u,err]=mopen(f,'rb');
    if err<>0 then
      error(msprintf(_("%s: Cannot open file ''%s''.\n"),"ECGViewer",f))
    end
    if ascii(mget(9,'c',u))<>"ECGScilab" then
      mclose(u)
      error(_("%s: Wrong value for input argument #%d: Format not recognized.\n"),"ECGViewer",1)
    end
    t0=mget(6,"s",u);
    Nchannels=mget(1,'i',u);
    Nsamples=mget(1,'i',u);
    fe=mget(1,'f',u);
    EOH=mtell(u)
    sigs=[]
  else
    EOH=[];
    t0=[0 0 0 0 0 0]
    u=[]
    fe=f.fs
    sigs=f.sigs
    [Nsamples,Nchannels]=size(f.sigs)
  end
  if LOC<>[] then
    selS=4;
    d=[];
    if or(selS==[1 2]) then d=[d 9];end//Qo
    if or(selS==[2 3]) then d=[d 5];end//Te
    if or(selS==3) then d=[d 6];end//Tp
    if or(selS==4) then d=[d 1];end//Rp
    Ic=ECGContiguousDetections(LOC,d)
  else
    t0=zeros(1,6);
    Ic=[]
    selS=[];
   
  end
  ud=tlist(["ECGV","fid","EOH","Nchannels","Nsamples", ...
            "Channels","BlockLength","Index","fe",...
            "LOC","Ic","t0","Synthetic","Detections",...
            "Segments","ax1","ax2","sigs"],u,EOH,Nchannels,Nsamples, ...
            1:Nchannels,15000,1,fe,...
            LOC,Ic,t0,0,[1 4],...
            selS,[],[],sigs)
 
endfunction
function names=ECGViewerMenuNames(k)
  names=[_("Show info");
         _("Select channels");
         _("Set interval length");
         _("Set start index");
         _("Set options");
         _("Show");
         _("Save")];
  if argn(2)==1 then names=names(k);end
endfunction
