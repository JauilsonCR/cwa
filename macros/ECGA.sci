//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [D,Pos,Amp,Wl,F,P]=ECGApproximate(S,pos,amp,wl,f,np)
  kRp=ECGRpeaks(S);
  //kRp=kRp(1:100);
  fs=S.fs
  S=S.sigs;
  Scale=1/max(abs(S))
  S=S*Scale;
  T=(0:size(S,'*')-1)/fs;
  ng=size(pos,'*'); //number of gaussian
 
  X=[pos;amp;wl;f];
  //initialization of result arrays
  D=zeros(1,size(kRp,'*'))
  Pos=zeros(ng,size(kRp,'*'))
  Amp=Pos
  Wl=Pos;
  F=Pos;
  P=zeros(np,size(kRp,'*'))
  l=round(kRp(2)-kRp(1))/2;
  kf=max(1,kRp(1)-l)-1;
  p0=S(kf+1);
  for i=1:size(kRp,'*')
    kd=kf+1;
    if i==size(kRp,'*') then
      l=round((kRp(i)-kRp(i-1))/2);
      kf=min(kRp(i)+l,size(S,'*'));
    else
      l=round((kRp(i+1)-kRp(i))/2);
      kf=kRp(i)+l;
    end
    s=S(kd:kf)';
    t=T(kd:kf)-T(kd);
    t=t/t($);

    if np>=2 then
      //approximate baseline
      ds=sgolaydiff(s,1,3,10);
      sel=find(abs(ds)<0.0004&abs(s)<0.1);

      p=[p0,(s(sel)-p0)/(ones(np-1,1)*t(sel)).^((1:np-1)'*ones(t(sel)))];
      p0=sum(p);
      b=p*(ones(np,1)*t).^((0:np-1)'*ones(t));
      s=s-b;
    else
      p=s(1)
      s=s-p
    end
    //approximate R peak
    [m,k]=max(abs(s));m=s(k);
    X=[t(k);m;0.1;1];
    weight=zeros(s);weight(k-round(0.06*fs):k+round(0.06*fs))=1;
    pause
    [fR, XR,gR, work, iters, evals, flag] = optim(list(ECGWaveOptim2,1,t,s,weight),X,'ar',5000,1000,1e-12);
pause
    X=[pos([1 2 4 5]);amp([1 2 4 5]);wl([1 2 4 5]);f([1 2 4 5])];
    X(3)=XR(1)+0.12;//approximate pos value for the S peak
    X(4)=X(3)+0.2;//approximate pos value for the T peak
    
    X(2)=XR(1)-0.025;//approximate pos value for the Q peak
    X(1)=X(2)-0.08;//approximate pos value for the P peak
    weight=ones(s); weight(k-round(XR(3)*fs):k+round(XR(3)*XR(4)*fs))=0;
    [fopt, Xopt,gopt, work, iters, evals, flag] = optim(list(ECGWaveOptim2,4,t,s),X,'ar',5000,1000,1e-12);
    mprintf("i=%d,flag=%d,evals=%d,fopt=%.2g,||gopt||=%.2g\n",i,flag,evals,fopt,norm(gopt))
    Pos(:,i)=Xopt(1:ng);
    Amp(:,i)=Xopt(ng+1:2*ng);
    Wl(:,i)=abs(Xopt(2*ng+1:3*ng));
    F(:,i)=Xopt(3*ng+1:4*ng);
    P(:,i)=p';
    D(i)=T(kf)-T(kd);
    pause
  end
  //Ts($+1)=T();
  Amp=Amp/Scale
  P=P/Scale;
endfunction

function [f,g,ind]=ECGWaveOptim2(x,ind,ng,t,sr,weight)
  with_grad=or(ind==[3 4])
  np=size(x,'*')-4*ng;
  pos=x(1:ng);
  amp=x(ng+1:2*ng);
  w=x(2*ng+1:3*ng);
  fw=x(3*ng+1:4*ng);
  if or(pos<0.2|pos>0.9) then ind=-1,f=0,g=zeros(x);return;end
  if or(w<=0.0001|fw<=0.1) then ind=-1,f=0,g=zeros(x);return;end
 
  
  if with_grad then
    [s,dsdx]=ECGWaveGen(t,pos,amp,w,fw);
  else
    s=ECGWaveGen(t,pos,amp,w,fw);
  end
  alpha=0.5;//penalization factor
  e=(s-sr).*weight;
  f=e*e'+alpha*(fw-1)'*(fw-1)
  g=zeros(x);
  if with_grad then
    g(1:4*ng)=2*dsdx*(weight.*e)';
    g(3*ng+1:4*ng)=g(3*ng+1:4*ng)+alpha*(fw-1); //adding penalization effect
  end
endfunction
