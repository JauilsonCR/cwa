function output=DFA(data,n,breakpoint)
//calculates alpha from detrended fluctuation analysis
//
//Inputs:    data = inter-beat interval signal (s)
//           n = array of window sizes
//           breakpoint = value of n that determines where alpha1 ends and
//           alpha2 begins
//           
//Outputs:   a data structure with fields:
//           alpha = the equation of the line in log-log plot of
//                  integrated y vs all window sizes.
//           alpha1 =   the equation of the line in log-log plot of
//                  integrated y vs window sizes up to n(Breakpoint).
//           alpha2 =  the equation of the line in log-log plot of
//                  integrated y vs window sizes from  n(Breakpoint) to n($).

//
// Reference: Heart rate analysis in normal subjects of various age groups
// Rajendra Acharya U*, Kannathal N, Ong Wai Sing, Luk Yi Ping and
// TjiLeng Chua 
//http://www.ncbi.nlm.nih.gov/pmc/articles/PMC493278/
  select argn(2)
  case 2 then
    breakpoint=13;
  case 1 then
    n=4:300;//array of window sizes
    breakpoint=13;
  end
  
  data=data(:);
  n=n(:);
  nLen=size(n,"*");

  //preallocate memory
  F_n=zeros(1,nLen);        

  data=data-mean(data); //mean value
  N=length(data);
  for i=1:nLen
    ni=n(i)
    nWin=floor(N/ni); //number of windows
    if nWin==0 then i=i-1;break;end
    N1=nWin*ni; //length of data minus rem
    Yn=zeros(1,N1);
    yk=cumsum(data(1:N1)); //integrate     
    //compute linear trend
    for j=1:nWin
      //linear fit coefs
      p=polyfit(1:ni,yk(((j-1)*ni+1):j*ni),1);
      //create linear fit
      Yn(((j-1)*ni+1):j*ni)=horner(p,1:ni);
    end
    // RMS fluctuation of integrated and detrended series
    F_n(i) = sqrt( sum((yk-Yn(:)).^2)/N1 );
  end
 
  //fit all values of n
 
  F_n=F_n(1:i)
  n=n(1:i)
  output.alpha=polyfit(log10(n),log10(F_n),1); // total slope
  
  bp=find(n==breakpoint);
  //fit short term n=1:bp
  output.alpha1=polyfit(log10(n(1:bp)),log10(F_n(1:bp)),1);// short range scaling exponent
  //fit long term n=bp+1:$
  output.alpha2=polyfit(log10(n(bp+1:$)),log10(F_n(bp+1:$)),1); // long range scaling exponent

  output.F_n=F_n';
  output.n=n';
  
endfunction

