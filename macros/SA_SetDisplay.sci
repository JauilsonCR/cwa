function SA_SetDisplay(d,fig)
  if argn(2)==1 then fig=gcf();end
  fig_ud=fig.user_data
  select convstr(d)
  case "pseudocolor"  then 
    fig_ud.draw_type=d
    set(fig,"user_data",fig_ud)
    SA_draw(fig)
  case "3d"  then 
    fig_ud.draw_type=d
    set(fig,"user_data",fig_ud)
    SA_draw(fig)
  end
endfunction
