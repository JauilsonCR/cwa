// This file is part of the CardioVascular wave analysis toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// Copyright (C) - INRIA - Alessandro Monti, Claire Médigue, Michel Sorine
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function data=CDM_Analysis(RR,Vt,freq_sampling,BPfrequency_bounds,BPfilterlength) 

  err_msg1=_("%s: Wrong type for input argument #%d: A real vector of float expected.\n");
  err_msg2=_("%s: Wrong type for input argument #%d: A real positive number expected.\n");
  fname="CDM_Analysis";
  if type(RR)<>1|~isreal(RR)|and(size(RR)>1) then 
    error(msprintf(err_msg1,fname,1))
  end
  if type(Vt)<>1|~isreal(Vt)|and(size(Vt)>1) then 
    error(msprintf(err_msg1,fname,2))
  end
  if size(Vt,'*')>1  then
    if size(Vt,'*')<>size(RR,'*') then
      err_msg=_("%s: Incompatible length for input arguments #%d and #%d.\n")
      error(msprintf(err_msg,fname,1,2))
    end
    refIsSignal=%t
  else
    refIsSignal=%f
  end
  if type(freq_sampling)<>1|~isreal(freq_sampling)|size(freq_sampling,'*')<>1|freq_sampling<=0 then 
    error(msprintf(err_msg2,fname,3))
  end

  if type(BPfrequency_bounds)<>1|~isreal(BPfrequency_bounds) then 
    err_msg=_("%s: Wrong type for input argument #%d: A real vector expected.\n");
    error(msprintf(err_msg,fname,4))
  end
  if size(BPfrequency_bounds,'*')<>2 then 
    err_msg=_("%s: Wrong size for input argument %s: An array of size %d expected.\n");
    error(msprintf(err_msg,fname,4))
  end
  if or(BPfrequency_bounds<=0)|BPfrequency_bounds(1)>=BPfrequency_bounds(2) then 
    err_msg=_("%s: Wrong value for input argument %s: A positive increasing array expected.\n");
    error(msprintf(err_msg,fname,4))
  end
  if  or(frequency_bounds(2)>0.5*freq_sampling) then 
    err_msg=_("%s: Wrong values for input argument #%d: The elements must be less than Nyquist frequency %f.\n");
    error(msprintf(err_msg,fname,4,0.5*freq_sampling))
  end
  
  if type(BPfilterlength)<>1|~isreal(BPfilterlength)|size(BPfilterlength,'*')<>1|BPfilterlength<=0 then 
    error(msprintf(err_msg2,fname,5))
  end
  if modulo(size(BPfilterlength,'*'),2)<>1 then
    err_msg=_("%s: Wrong value for  input argument #%d: An odd number expected.\n")
    error(msprintf(err_msg,fname,5))
  end

  //narrow band filter
  BPdelay=(BPfilterlength-1)/2;
  BP=wfir("bp",BPfilterlength,BPfrequency_bounds/freq_sampling,"hm",[0  0]);

  //filter the input signals
  //extend the signals to take the band pass filter delay into account
  RR=matrix(RR,1,-1)
  sig=filter(BP,1,[RR RR($)*ones(1,BPdelay)]);
  
  if refIsSignal then 
    Vt=matrix(Vt,1,-1)
    ref=filter(BP,1,[Vt Vt($)*ones(1,BPdelay)]);
  else 
    ref=Vt/freq_sampling;
  end
 
  // Call the complex demodulation
  [IFreq,IAmp,IPhase,delay,f0e]=ComplexDemodulation(sig,ref);
  // Take sampling frequency into account
  IFreq=IFreq*freq_sampling;

  //Synchronize results
  sig=sig(BPdelay+1:$)';
  IFreq=IFreq(BPdelay+delay+1:$)';
  IAmp=IAmp(BPdelay+delay+1:$)';
  if refIsSignal then
    ref=Vt';
    IPhase=IPhase(BPdelay+delay+1:$)';
  else
     ref=[];
     IPhase=[];
  end
 
  //Create data structure of the result
  fn=["RRA","time","RR","Vt","RRfiltered","IFreq","IAmp","IPhase"]
  t=(0:(size(IFreq,'*')-1))'/freq_sampling;
  data=tlist(fn,t,RR',ref,sig,IFreq,IAmp,IPhase)
 
endfunction
