function %tda_p(tda)
  mprintf("   min:    %.2fs \t%.1fbpm\n   max:    %.2fs \t%.1fbpm\n   mean:   %.2fs \t%.1fbpm\n   median: %.2fs \t%.1fbpm\n",...
          tda.min/1000,60000/tda.min,tda.max/1000,60000/tda.max,tda.mean/1000,60000/tda.mean,tda.median/1000,60000/tda.median)
  mprintf("   SDNN:   %.2fms\n   SDANN:  %.2fms\n   SDNNi:  %.2fms\n",...
          tda.SDNN,tda.SDANN,tda.SDNNi)
  mprintf("   HRVTi:  %.2f\n   TINN:   %.2fms\n",...
          tda.HRVTi,tda.TINN)
  
  mprintf("   NNx:    %d \t\tpNNx:  %.2f%%\n",tda.NNx,tda.pNNx)
  mprintf("   SDSD:   %.2fms\n   RMSSD:  %.2fms\n",tda.SDSD,tda.RMSSD)
  mprintf("   SD1:    %.2fms\n   SD2:    %.2fms\n", tda.SD1,tda.SD2)
endfunction
