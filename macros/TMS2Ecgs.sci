// This file is part of the CWA toolbox
// Copyright (C) 2014 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function TMS2Ecgs(filein,fileout,sel)
//Transform POLY5/TMS32 file into Scilab ECG file
//  http://www.tmsi.com/downloads/tms_read.m
//  http://code.google.com/p/fieldtrip/ (sopen.m)
   BlockLength=5000;
  if argn(2)<2|fileout==[] then
    [path, fname, extension] = fileparts(filein)
    fileout=strsubst(filein,extension,".ecgs")
  end
 
  in=mopen(filein,'rb');
 
  //Get the header data
  [HDR,f]=readTMSHeader(in);
  Nchannels=HDR.Nchannels;
  if argn(2)<3 then 
    sel=1:Nchannels;
  else
    if min(sel)<1|max(sel)>Nchannels then
      mclose(in)
      error(msprintf(_("%s: Wrong values for input argument #%d: Elements must be in the interval ""[%s, %s].\n"),...
                     "TMS2Ecgs",3,"1",string(Nchannels)))
    end
  end
    
  fe=HDR.SampleRate;
  Nsamples=HDR.Nsamples;
  t0=HDR.Date([3:-1:1 5:7]);
  //write them
  out=writeEcgsFileInfo(fileout,size(sel,'*'),Nsamples,fe,t0)
  //get and write the channels data (Nsamples*Nchannels)
  SPR=f.SPR;
  GDFTYP=f.GDFTYP
  r=(f.UnitHigh - f.UnitLow)./(f.ADCHigh - f.ADCLow);
  winH=mywaitbar("open",_("Processing"));
  if and(GDFTYP==GDFTYP(1)) then
    for bc=1:f.NB
      w=mget(86,'uc',in);
      s =matrix(mget(Nchannels*SPR,gdfdatatype(GDFTYP(1)),in),Nchannels,-1);
      s=s(sel,:);
      for k = 1:size(sel,'*')
        k1=sel(k);
        s(:,k)=(s(:,k)-f.ADCLow(k1))*r(k1)+f.UnitLow(k1)
      end
      //convert to µv to mv and write down
      mput(s(:)/1000,'d',out);
      mywaitbar("update",bc/f.NB,winH);
    end;
  else
    for bc=1:f.NB
      for k1 = 1:SPR,
        for k2 = 1:NChannels,
          s(k2,k1) = mget(1,gdfdatatype(GDFTYP(k2)),in);
        end;
        if meof(u) then
          ns=k1-1
          break
        end
      end;
      s=s(sel,:);
      for k = 1:size(sel,'*')
        k1=sel(k);
        s(:,k)=(s(:,k)-f.ADCLow(k1))*r(k1)+f.UnitLow(k1)
      end
      //convert to µv to mv and write down
      mput(s(:)/1000,'d',out);
      mywaitbar("update",bc/f.NB,winH);
    end
  end
  mywaitbar("close",winH);
  mclose(in);
  mclose(out);
endfunction

