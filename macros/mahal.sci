//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [d,Rf] = mahal(U,R);
//Mahalanobis distance of each point in U from the sample in R. 
//obervations correspond to R and U rows 
//The Malanobis distance of U from the reference observations stored in R is defined by
//              
//d=(u-mu)*inv(Sigma)*(u-mu)'
//where 
//mu=mean(R,1) is the sample mean of R
//and Sigma is the sample covarianve of R which can be computed by 
//Sigma=(R-mu)'*(r-mu)/(size(R,1)-1).
//if q and r are the QR decomposition factors of Sigma, d can be evaluated
//as
//d= sum(((u-mu)/r).^2)*(size(R,1)-1)  
//The second output argument can be used to recall mahal with the same
//reference set.
  [mu,nu] = size(U);
  if typeof(R)=="st" then
    //the reference data are already factorized
    r=R.Rfact
    m=R.mean
    if size(m,2)<>size(U,2) then
          error(msprintf(_( "%s: Incompatible input arguments #%d and #%d: Same column dimensions ""expected.\n"),"mahal",1,2))
    end
  else
    [mr,nr] = size(R);
    if size(R,2)<>nu then
      error(msprintf(_( "%s: Incompatible input arguments #%d and #%d: Same column dimensions ""expected.\n"),"mahal",1,2))
    end

    if size(R,1)<size(R,2) then
       error(msprintf(_("%s: Wrong row size for argument %d: At least %d expected.\n"),"mahal",2,size(R,2)))
    end

    m = mean(R,1);
    //remove means column wise
    R=R-m(ones(mr,1),:);
    //factorize the unnormalized variance matrix of R.
    [q,r] = qr(R/sqrt((mr-1)),"e");
  end
  U=U-m(ones(mu,1),:);
  d = sum((U/r).^2,2);
  if argn(1)==2 then
    Rf=struct('Rfact',r,'mean',m)
  end
endfunction
