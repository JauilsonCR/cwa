//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ECGCutPLIBatch(filein,f,varargin)
//This function remoces power-lin interference from the ECG using
//annulation of frequency components around f
//  ECGcutPLIfile(S,fe,f,threshold)
//  f : pli frequency  

  in=mopen(filein,'rb');
  [path, fname, extension] = fileparts(filein)
  if size(varargin)==0 then
    
    fileout=strsubst(filein,extension,"_pli.ecgs")
    df=0.2;
  else
    df=0.2
    fileout=strsubst(filein,extension,"_pli.ecgs")
    if type(varargin(1))==1 then
      df=varargin(1)
    elseif type(varargin(1))==10 then
      fileout=varargin(1)
    end
    if size(varargin)==2 then
      if type(varargin(2))==1 then
        df=varargin(2)
      elseif type(varargin(2))==10 then
        fileout=varargin(2)
      end
    end
  end
  
  if ascii(mget(9,'c',in))<>"ECGScilab" then
    error("Unexpected file type")
  end
  out=mopen(fileout,'wb');
  t0=mget(6,"s",in);
  Nchannels=mget(1,'i',in);
  Nsamples=mget(1,'i',in);
  fe=mget(1,'f',in);
  
  mput(ascii("ECGScilab"),'c',out)
  mput(t0,'s',out);
  mput(Nchannels,'i',out)
  mput(Nsamples,'i',out)
  mput(fe,'f',out)
  
 
  BlockLength=30000;
  Nblock=ceil(Nsamples/BlockLength)
  winH=mywaitbar("open",_("Processing"));
  for kb=1:Nblock
    Sb=sciecg(fe,matrix(mget(Nchannels*BlockLength,'d',in),Nchannels,-1)');
    Sb=ECGCutPLI(Sb,f,df)
    mput(Sb.sigs','d',out)
    mywaitbar("update",kb/Nblock,winH);
  end
  mclose(in)
  mclose(out)
  mywaitbar("close",winH);
endfunction

