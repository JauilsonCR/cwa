//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function h=mywaitbar(op,varargin)
  withgui=and(sciargs()<>"-nwni");
  if withgui then
    select op
    case "open"
      h=waitbar(varargin(1))
    case "update"
      waitbar(varargin(1),varargin(2))
      h=varargin(2)
    case "close"
      close(varargin(1))
       h=varargin(1)
    end
  else
    select op
    case "open"
      h=list(varargin(1),0);
      mprintf(varargin(1))
    case "update"
      mprintf("\r %s: %d%%",varargin(2),round(100*varargin(1)))
      h=varargin(2)
    case "close"
      mprintf("\r")
      h=varargin(1)
    end
  end    
endfunction
