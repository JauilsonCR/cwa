// This file is part of the CWA toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function a=AddChannelPlot(signal,label,fig)
  if argn(2)<3 then fig=gcf();end
  ud=fig.user_data
  if typeof(ud)<>"st"|and(fieldnames(ud)<>"type")|ud.type<>"MultiChannelPlot" then
    error(msprintf(_("%s :Wrong graphic window type: %s expected.\n"),...
                   "AddChannelPlot","MultiChannelPlot"))
  end
  AX=fig.children(fig.children.type=="Axes")
  AX=AX($:-1:1);
  nplot=size(AX,'*')
  //recover the time
  P=AX(1).children(AX(1).children.type=="Polyline")
  t=P(1).data(:,1);
  nt=size(t,'*')
  
  if type(signal)<>1|~isreal(signal) then
    error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),...
                   "AddChannelPlot",1))
  end
  if and(size(signal)>1) then 
    if size(signal,1)<>nt then
      error(msprintf(_("%s: Wrong size for argument %d: A %d column expected.\n"),...
                   "AddChannelPlot",1,nt))
    end
  else
    if size(signal,'*')<>nt then
      error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),...
                     "AddChannelPlot",1,nt))
    end
  end
  if type(label)<>10|size(label,'*')<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),...
                       "AddChannelPlot",2))
  end
  //recover the options
  colors=ud.colors;nc=size(colors,'*')
  font_style=AX(1).font_style;
  font_size=AX(1).font_size;
  grid_color=AX(1).grid(1);
  low_margin=AX(1).margins(4);
  zoom_box=AX(1).zoom_box;
  tbounds=[min(t);max(t)];
  idraw=fig.immediate_drawing;
  fig.immediate_drawing = "off";

  //resize the already present subplots
  o=0.05; //same vamue as in MultiChannelPlot
  h=(1-2*o)/(nplot+1);
  for kplot=1:nplot
    AX(kplot).axes_bounds=[0,o+(kplot-1)*h,1,h];
  end
  AX(nplot).auto_ticks(1)="off";
  x_lab=AX(nplot).x_label.text;
  AX(nplot).x_label.text="";
  //Add the requested subplot
  kplot=kplot+1
  a=newaxes();
  a.axes_bounds=[0,o+(kplot-1)*h,1,h];
  a.tight_limits="on";
  a.margins(4)=low_margin;
  a.grid(1:2)=grid_color;
  a.font_style=font_style;
  a.font_size=font_size;
  a.axes_visible='on';
  a.clip_state="clipgrf";
  mny=min(signal);
  mxy=max(signal);
  dy=(mxy-mny)/10;
  a.data_bounds=[tbounds [mny-dy;mxy+dy]];
  a.y_label.text=label;
  a.x_label.text=x_lab;
  for i=1:size(signal,2)
    xpoly(t,signal(:,i));
    p=gce();
    p.foreground=colors(modulo(i-1,nc)+1);
  end
  //apply current zoom if any
  if zoom_box<>[] then
    w=MultiChannelPlot;//make selection_y_bounds known
    P=a.children(a.children.type=="Polyline");
    l=ud.scroll(2);
    count=ud.scroll(1);
    xbounds=ud.scroll(3:4);
    zb([1 3])=xbounds(1)+(count-1)*l+[0 l];
    zb([2 4])=selection_y_bounds(P,zb([1 3]));
    ax.zoom_box=zb;
  end
  fig.immediate_drawing=idraw
endfunction
