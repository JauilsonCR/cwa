// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com
// 
// This file is part of HRVAS
//
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
function [t2,y2] = replaceRROutliers(t,y,outliers, method, opt1)
//replaceOutliers: replaces artifacts/outliers from data series
//
//Inputs:    t = time
//           y = ibi values
//           outliers = logical array of outliers. 0=normal, 1=outlier
//           method = artifact replacement method to use.
//   methods:    'remove' = Outliers are removed
//               'mean' = Outliers are replaced with mean value from nearest +- m ibi values.
//               'spline' = Outliers are replaced by cubic spline interpolation
//               'median' = Outliers are replaced with median value from nearest +- m ibi values.
//Outputs:   t2, y2 = arrays with replaced artifacts
//Examples: 
//   remove outliers from ibi series
//       [t2 y2] = replaceOutlers(t,y, outlierArray,'remove')
// Reference:
// Thuraisingham, R. A. (2006). "Preprocessing RR interval time
// series for heart rate variability analysis and estimates of
// standard deviation of RR intervals." Comput.Methods
// Programs Biomed.  
  nargin=argn(2)
  if nargin < 4 then error('Not enough input arguments');end        
  if nargin < 5 then opt1=9;end        
  method= convstr(method)
  if method=='cubic' then method='spline';end

  select method
  case 'remove' //remove outliers
    y2=y;
    t2=t;
    y2(outliers) = [];
    t2(outliers) = [];
  case 'mean' //mean replacement            
    y2=y; //preallowcate newIBI series with old values
    t2=t;
    l=length(y); //number of ibi
    m=floor((opt1-1)/2); //calculate half window width
    i=find(outliers); //index location of outliers                        
    i=(i(i>m+1 & i<l-m)); //index location of outliers within range
    y(i)=%nan;
    for n=i;
      y2(n)=nanmean(y(n-m:n+m)); //replace with mean, ignore NaN values
    end
  case 'spline' //Cubic spline replacment            
    t2=t;            
    y(outliers) = []; 
    t(outliers) = [];
    y2=interp1(t,y,t2,'spline','extrap');
  case 'median' //medianFilter
    y2=y; //preallowcate newIBI series with old values
    t2=t;
    l=length(y); //number of ibi
    m=floor((opt1-1)/2); //calculate half window width
    i=find(outliers); //index location of outliers
    i=(i(i>m+1 & i<l-m)); //index location of outliers within range
    y(i)=%nan;
    for n=i;
      y2(n)=nanmedian(y(n-m:n+m)); //replace with median, ignore NaN values
    end
 
  else // do nothing
    y2=y;
    t2=t;            
  end

endfunction
