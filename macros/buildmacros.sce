function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
 // tbx_build_macros(TOOLBOX_NAME, macros_path);
 
 // tbx_build_macros(TOOLBOX_NAME, macros_path+"HRVAS");
  
  if (isdef("genlib") == %f) then
    exec(SCI+"/modules/functions/scripts/buildmacros/loadgenlib.sce");
  end

  genlib("CWAlib",macros_path,%f,%t);
  genlib("HRVASlib",macros_path+"HRVAS",%f,%t)
  
endfunction

buildmacros();
clear buildmacros; // remove buildmacros on stack

