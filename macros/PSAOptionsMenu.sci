function PSAOptionsMenu(k,win)
  fig=get_figure_handle(win)
  ud=fig.user_data
  fig.immediate_drawing = "off"
  select k
  case 1  then //Hide legends
    ud.LegendHandle.visible="off"
  case 2  then //Show legends
    ud.LegendHandle.visible="on"
  case 3 then //Phase in degree
    if  ud.PhaseHandle<>[] then
      ax=ud.PhaseHandle
      if ax.user_data=="rd" then
        P=ax.children(ax.children.type=="Polyline")
        for i=1:size(P,'*')
          P(i).data(:,2)=P(i).data(:,2)*180/%pi;
        end
        ax.data_bounds(:,2)= ax.data_bounds(:,2)*180/%pi;
        ax.y_label.text=_("RR->Vt Phase shift(°)")
        ax.user_data="degree"
      end
    end
  case 4 then //Phase in rd
    if  ud.PhaseHandle<>[] then
      ax=ud.PhaseHandle
      if ax.user_data=="degree" then
        P=ax.children(ax.children.type=="Polyline")
        for i=1:size(P,'*')
          P(i).data(:,2)=P(i).data(:,2)*%pi/180;
        end
        ax.data_bounds(:,2)= ax.data_bounds(:,2)*%pi/180;
        ax.y_label.text=_("RR->Vt Phase shift(rd)")
        ax.user_data="rd"
      end
    end
  end
  fig.immediate_drawing = "on"
endfunction
