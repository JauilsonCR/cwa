//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ECGSubsampleBatch(filein,fileout,f_ratio,nf)
  if argn(2)<4 then nf=64;end
  if type(f_ratio)<>1|~isreal(f_ratio)|size(f_ratio,"*")<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),"ECGSubsampleBatch",2))
  end
  if int(f_ratio)<>f_ratio then
    error(msprintf(_("%s incompatible arguments %d and %d: arg%d must be a multiple of arg%d\n"),"ECGSubsampleBatch",2,3,2,3))
  end
  if f_ratio<2  then
    error(msprintf(_("%s wrong value for argument %d: must be greater or equal to %f\n"),"ECGSubsampleBatch",2,2))
  end
  
  in=mopen(filein,'rb');
  if ascii(mget(9,'c',in))<>"ECGScilab" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"ECGSubsampleBatch"))
  end
  t0=mget(6,"s",in);
  Nchannels=mget(1,'i',in);
  Nsamples=mget(1,'i',in);
  fe_in=mget(1,'f',in);
  fe_out=fe_in/f_ratio;
  
  out=mopen(fileout,'wb');
  mput(ascii("ECGScilab"),'c',out)
  mput(t0,'s',out);
  mput(Nchannels,'i',out)
  mput(floor(Nsamples/f_ratio),'i',out)
  mput(fe_out,'f',out)
  
  //Anti aliasing filter cuts frequency components whose frequency is greater than fe_out
  F_lp=wfir("lp",nf,[0.5/f_ratio,0],'hn',[0 0]);

  BlockLength=floor(10*fe_in);
  BlockLength=BlockLength-modulo(BlockLength,f_ratio)
  winH=mywaitbar("open",_("Processing subsample"));
  //Boucle sur les blocs
  Nb=ceil(Nsamples/BlockLength);
  
  //first block
  S=matrix(mget(Nchannels*BlockLength,'d',in),Nchannels,-1);
  for k=1:Nchannels
    [Sf,state_lp(:,k)]=filter(F_lp,1,[S(k,nf:-1:2) S(k,:)]);
    S(k,:)=Sf(nf:$);
  end
  clear Sf
  S=S(:,1:f_ratio:$);
  mput(S,"d",out);
  mywaitbar("update",1/Nb,winH);
  
  for b=2:Nb
    S=matrix(mget(Nchannels*BlockLength,'d',in),Nchannels,-1);
    for k=1:Nchannels
      [S(k,:),state_lp(:,k)]=filter(F_lp,1,S(k,:),state_lp(:,k));
    end
    S=S(:,1:f_ratio:$);
    mput(S,"d",out);
    mywaitbar("update",b/Nb,winH);
  end
  mywaitbar("close",winH);
  mclose(in);
  mclose(out);
endfunction
