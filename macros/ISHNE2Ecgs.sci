//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ISHNE2Ecgs(filein,fileout,sel)
//http://thew-project.org/papers/Badilini.ISHNE.Holter.Standard.pdf
  if argn(2)<2 then
    [path, fname, extension] = fileparts(filein)
    fileout=strsubst(filein,extension,".ecgs")
  end

  in=mopen(filein,'rb');
  magicNumber = ascii(mget(8,"uc",in));

  if magicNumber<>"ISHNE1.0" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: a %s file format expected.\n"),...
                   "ISNHE2Ecgs",1,"ISHNE1.0"))
  end
 
  //Get the header data
  header=readISHNEHeader(in);
  cur=mtell(in);
  Nchannels=header.nbLeads;
  if argn(2)<3 then 
    sel=1:Nchannels;
  else
    if min(sel)<1|max(sel)>Nchannels then
      mclose(in)
      error(msprintf(_("%s: Wrong values for input argument #%d: Elements must be in the interval ""[%s, %s].\n"),...
                     "ISNHE2Ecgs",3,"1",string(Nchannels)))
    end
  end
  Resolution=header.Resolution(sel)/1000;//convert in micro volts
  fe= header.Sampling_Rate;
  t0= [header.Record_Date(3:-1:1) header.Start_Time];
  mseek(0,in, 'end');
  L=mtell(in);
  Nsamples=int((L-cur)/(2*Nchannels));
  mseek(cur,in, 'set');
  // write it
  out=writeEcgsFileInfo(fileout,size(sel,'*'),Nsamples,fe,t0)

  //get and write the channels data (Nsamples*Nchannels)
  BlockLength=50000;
  winH=mywaitbar("open",_("Processing"));
  Nb=floor(Nsamples/BlockLength);
  for k=1:Nb
    S=matrix(mget(Nchannels*BlockLength,"s",in),Nchannels,-1);
    mput(diag(1.0./Resolution)*S(sel,:),'d',out)
    mywaitbar("update",k/Nb,winH);
  end
  N=modulo(Nsamples,BlockLength);
  if N<>0 then
    S=mget(Nchannels*N,'s',in);
    n=size(S,'*');
    n=floor(n/Nchannels)*Nchannels
    S=matrix(S(1:n),Nchannels,-1);
    S=diag(1.0./Resolution)*S(sel,:);
    mput(S','d',out);
  end
  mywaitbar("close",winH);
  mclose(in);
  mclose(out);
endfunction
