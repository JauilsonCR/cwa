//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [ecg,c]=ECGDetrend(ecg,nsec)
//use sliding mean to remove the baseline variation
  if typeof(ecg)<>"sciecg" then
    error(msprintf(_("%s: Wrong type for argument %d: a real array or an sciecg data structure expected.\n"),...
                   "ECGDetrend",1))
  end
  if argn(2)<3 then nsec=4;end
  fs=ecg.fs
  ecg=ecg.sigs
  [nt,Nchannels]=size(ecg);
  //take nsec periods into account
  nf=nsec*fs;
  if int(nf/2)*2<>nf then nf=nf+1;end
  w=window("hn",nf);w=w/sum(w);

  for k=1:Nchannels
    v=ecg(:,k);
    v=[v(1)*ones(nf/2,1);v;v($)*ones(nf/2,1)];
    c=convol(w,v);
    c=c(nf+1:$-nf+1)';
    ecg(:,k)=ecg(:,k)-c;
  end
  ecg=sciecg(fs,ecg);
endfunction
