//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function index=ECGContiguousDetections(LOC,sel)
//Each column of Loc contains the index of the detections corresponding to a RSTQ
//complex [kRp;kRe;kTo;kTp;kTe;kPo;kPp;kPe;kQo]
//sel selects the requested events 
//the Rp detections are always present
//index : a two column array of indexes . The contiguous segments are given by LOC(:,index(i,1):index(i,2))
  if argn(2)<2 then
    sel=[4 5 9];//Tp,Te,Qo
  end
  
  n=size(LOC,2);
  exclude=[0, find(or(LOC(sel,:)==-1,1)),n+1];//index des detections intermediaires absentes
 
  exclude=unique(exclude);
  dd=find(diff(exclude)>3);
  if dd==[] then
    index=[]
    return
  end
  index=[exclude(dd)'+1 exclude(dd+1)'-1];
endfunction
