// This file is part of the  Cardiovascular Wawes Analysis toolbox
// Copyright (C) 2014 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function path=CWApath()
//this function returns the path of the CWA module
 t=string(CWAlib);
 path=part(t(1),1:length(t(1))-6)
endfunction

