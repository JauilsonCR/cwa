//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function traceD()
  //used by ECGTwaveEnds for debugging purposes
  global ok;
  if %opt(2)==0 then return;end
  if ~ok then return;end
  if knR>=size(Rpeaks,'*') then return;end
  if %f
    kTe_ann=find((Te_ann*fe>Rpeaks(knR))&(Te_ann*fe<Rpeaks(knR+1)));
    if kTe_ann==[] then return,end
    kTe_ann=Te_ann(kTe_ann)*fe
  end
 // if abs(abs(kTe_ann-kTe)-abs(kTe_ann-kTe_QZ))<2 then return;end
 //if abs(kTe_ann-kTe)<10 then return;end
   fig=gcf();fig.axes_size = [500,220]
  drawlater
  clf;subplot(211);ax1=gca();ax1.font_style=9;ax1.font_size=2;
  t=(kR-10:Rpeaks(knR+1)+10)';
  Sigs=S;sigsel=1;name=""
  plot(t,Sigs(t,:)),
  plot([leftbound-swin leftbound,rightbound]',Sigs([leftbound-swin leftbound,rightbound],:),'+r');
   if %f then plot(kTe_ann*ones(1,2),Sigs(kTe_ann,:),'og');end
  plot(kTe*ones(1,2),Sigs(kTe,:),'or')
  //plot(kTe_QZ*ones(1,2),Sigs(kTe_QZ,:),'*r')
  xgrid()
  ax1.margins(3:4)=[0.2 0]
  ax1.x_ticks.labels=emptystr(ax1.x_ticks.labels)
  ax1.title.text=name+' channel '+string(sigsel)+' #peak:'+string(knR)
  subplot(212);ax2=gca();ax2.font_style=9;ax2.font_size=2;
  plot((leftbound:rightbound),Tval),plot(kTe,Tval(kTe-leftbound+1),'or')
  if %f&kTe_ann-leftbound+1>0&kTe_ann-leftbound+1<=size(Tval,'*') then
    plot(kTe_ann,Tval(kTe_ann-leftbound+1),'og')
  end
  ax2.data_bounds(:,1)=ax1.data_bounds(:,1); xgrid()
  ax2.margins(3:4)=[0 0.2]
  disp(typ)
  drawnow
  r=input("conserver 0/1: ");
  if r==1 then 
    xs2svg(fig,name+'_'+string(sigsel)+"_"+string(knR)+".svg");
  elseif r==2 then
    ok=%f
  end
 
endfunction
