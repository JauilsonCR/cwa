// This file is part of the  Cardiovascular Wawes Analysis toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// Copyright (C) - INRIA - Alessandro Monti, Claire Médigue, Michel Sorine
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function result = Baroreflex_Analysis(RR,SBP,freq_sampling,frequency_band,varargin)
  err_msg1=_("%s: Wrong type for input argument #%d: A real vector of float expected.\n");
  err_msg2=_("%s: Wrong type for input argument #%d: A real positive number expected.\n");
  fname="Baroreflex_Analysis";
  if type(RR)<>1|~isreal(RR)|and(size(RR)>1) then 
    error(msprintf(err_msg1,fname,1))
  end

  if type(SBP)<>1|~isreal(SBP)|and(size(SBP)>1) then 
    error(msprintf(err_msg1,fname,1))
  end
   
  if size(SBP,'*')<>size(RR,'*') then
    err_msg=_("%s: Incompatible length for input arguments #%d and #%d.\n")
    error(msprintf(err_msg,fname,1,2))
  end
   if type(freq_sampling)<>1|~isreal(freq_sampling)|size(freq_sampling,'*')<>1|freq_sampling<=0 then 
    error(msprintf(err_msg2,fname,3))
  end
  nlimit=0.5*freq_sampling; //nyquist frequency
  
  if type(frequency_band)<>1|~isreal(frequency_band) then 
    err_msg=_("%s: Wrong type for input argument #%d: A real matrix expected.\n");
    error(msprintf(err_msg,fname,4))
  end
  nb=size(frequency_band,1)
  if size(frequency_band,2)<>2 then 
    err_msg=_("%s: Wrong number of columns for input argument #%d: %d expected.\n");
    error(msprintf(err_msg,fname,4,2))
  end
  if or(frequency_band<=0) then
    err_msg=_("%s: Wrong values for input argument #%d: Elements must be positive.\n");
    error(msprintf(err_msg,fname,4))
  end
  if  or(frequency_band(:,1)>=frequency_band(:,2)) then 
    err_msg=_("%s: Wrong values for input argument #%d: The second column must be greater than the first one.\n");
    error(msprintf(err_msg,fname,4))
  end
  if  or(frequency_band(:,2)>nlimit) then 
    err_msg=_("%s: Wrong values for input argument #%d: The elements must be than Nyquist frequency %f.\n");
    error(msprintf(err_msg,fname,4,nlimit))
  end
  if and(size(varargin)<>[1 3]) then 
    err_msg=_("%s: Wrong number of input arguments: %d or %d to %d expected.\n")
    error(msprintf(err_msg,fname,7,4,5))
  else
    options=check_npmcnssa_options(varargin($),"Baroreflex_Analysis")
    if size(varargin)>1 then
      BPfrequency_bounds=varargin(1)
      BPfilterlength=varargin(2)
      if type(BPfrequency_bounds)<>1|~isreal(BPfrequency_bounds) then 
        err_msg=_("%s: Wrong type for input argument #%d: A real vector expected.\n");
        error(msprintf(err_msg,fname,5))
      end
      if size(BPfrequency_bounds,'*')==0 then
        BPfrequency_bounds=[min(frequency_band(:,1))*0.9 min(nlimit,max(frequency_band(:,2))*1.1)]
      end
      if size(BPfrequency_bounds,'*')<>2 then 
        err_msg=_("%s: Wrong size for input argument %s: An array of size %d expected.\n");
        error(msprintf(err_msg,fname,5))
      end
      if or(BPfrequency_bounds<=0)|BPfrequency_bounds(1)>=BPfrequency_bounds(2) then 
        err_msg=_("%s: Wrong value for input argument %s: A positive increasing array expected.\n");
        error(msprintf(err_msg,fname,5))
      end
      if  or(BPfrequency_bounds(2)>nlimit) then 
        err_msg=_("%s: Wrong values for input argument #%d: The elements must be less than Nyquist frequency %f.\n");
        error(msprintf(err_msg,fname,5,nlimit))
      end
      if min(frequency_band(:,1))<BPfrequency_bounds(1) then
        err_msg= _("%s: Incompatible input arguments: min value of argument #%d expected less than min value of argument #%d.\n")
        error(msprintf(err_msg,fname,5,4))
      end
      if max(frequency_band(:,2))>BPfrequency_bounds(2) then
        err_msg= _("%s: Incompatible input arguments: max value of argument #%d expected less than max value of argument #%d.\n")
        error(msprintf(err_msg,fname,4,5))
      end
 
      if type(BPfilterlength)<>1|~isreal(BPfilterlength)|size(BPfilterlength,'*')<>1|BPfilterlength<0 then 
        error(msprintf(err_msg2,fname,5))
      end
      if BPfilterlength>0&modulo(size(BPfilterlength,'*'),2)<>1 then
        err_msg=_("%s: Wrong value for  input argument #%d: An odd number expected.\n")
        error(msprintf(err_msg,fname,5))
      end
    else
      BPfrequency_bounds=[min(frequency_band(:,1))*0.9 min(nlimit, max(frequency_band(:,2))*1.1)]
      BPfilterlength=355
    end
 
  end
  if options.sectionlength<ceil(freq_sampling/min(frequency_band(:,1))) then
    warning(msprintf(_("%s: %s is too small compared to lowest frequency bounds.\n"),fname,"sectionlength"))
  end
  fn=["BRA","datatime","RR","RR_filtered","SBP","SBP_filtered","resulttime","RR_energy","SBP_energy","RR_fmin","SBP_fmin",..
        "RR_SPSDmin","SBP_SPSDmin","RR_dispmin","SBP_dispmin","SBP_RR_gain","SBP_RR_coherence","CoherenceThreshold"];
  RR=matrix(RR,1,-1);
  SBP=matrix(SBP,1,-1);
 
  frequency_band=frequency_band/freq_sampling;
  stp=options.sectionstep
  if BPfilterlength>0 then
    BPdelay=(BPfilterlength-1)/2;
    BP=wfir("bp",BPfilterlength, BPfrequency_bounds/freq_sampling,"hn",[0 0]);
    RR_filtered=filter(BP,1,[RR RR($)*ones(1,BPdelay+stp)]);
    SBP_filtered=filter(BP,1,[SBP SBP($)*ones(1,BPdelay+stp)]);
  else
    BPdelay=0;
    RR_filtered=RR
    SBP_filtered=SBP
  end
  
  [signals_energy,signals_dispersion,inout_gain,inout_coherence]= ...
      np_mc_nss_analysis([RR_filtered;SBP_filtered],frequency_band, ...
                         options);
  //generate the result, 
  skp=floor(BPdelay/stp);
  n=size(signals_energy,2)-skp
  t=(0:(size(RR,2)-1))/freq_sampling;
  result=tlist(fn,...
               t,...
               RR,...
               RR_filtered(BPdelay+1:$-stp),...
               SBP,...
               SBP_filtered(BPdelay+1:$-stp),...
               (0:(n-1))*options.sectionlength/(2*freq_sampling), ...
               matrix(signals_energy(1,skp+1:$,:),n,nb),...
               matrix(signals_energy(2,skp+1:$,:),n,nb), ...
               matrix(signals_dispersion.fmin(1,skp+1:$,:),n,nb)*freq_sampling,...
               matrix(signals_dispersion.fmin(2,skp+1:$,:),n,nb)*freq_sampling,...
               matrix(signals_dispersion.pmin(1,skp+1:$,:),n,nb),...
               matrix(signals_dispersion.pmin(2,skp+1:$,:),n,nb),...
               matrix(signals_dispersion.dmin(1,skp+1:$,:),n,nb),...
               matrix(signals_dispersion.dmin(2,skp+1:$,:),n,nb),...
               matrix(inout_gain(1,skp+1:$,:),n,nb),...
               matrix(inout_coherence(1,skp+1:$,:),n,nb),...
               options.minimalcoherence)
endfunction
