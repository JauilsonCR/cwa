//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [datatyp] = gdfdatatype(GDFTYP)
  if GDFTYP==0
    datatyp='uc';
  elseif GDFTYP==1
    datatyp='c';
  elseif GDFTYP==2
    datatyp='uc';
  elseif GDFTYP==3
    datatyp='s';
  elseif GDFTYP==4
    datatyp='us';
  elseif GDFTYP==5
    datatyp='i';
  elseif GDFTYP==6
    datatyp='ui';
  elseif GDFTYP==7
    datatyp='l';
  elseif GDFTYP==8
    datatyp='ul';
  elseif GDFTYP==16
    datatyp='f';
  elseif GDFTYP==17
    datatyp='d';
  else
    error(msprintf(_("%s: Invalid channel type\n"),"gdfdatatype"));
  end;
endfunction

