//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ecg=sciecg(fs,s)

  if type(s)<>1|~isreal(s) then
    error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),...
                   "sciecg",2))
  end
   
  if type(fs)<>1|~isreal(fs)|size(fs,"*")<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                   "sciecg",1))
  end
  if fs<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                   "sciecg",1))
  end

  if or(size(s)==1) then s=s(:);end
  
  ecg=mlist(["sciecg","fs","sigs"],fs,s)
endfunction
