//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [T,H]= sphericalParametrization(T,H)
//compute spherical parametrization of a Cholesky factor
//Pinheiro, J.C. and Bates., D.M. (1996) "Unconstrained Parametrizations
//for Variance-Covariance Matrices", Statistics and Computing, 6,
//289-296.
// R : a n by n positive definite upper triangular matrix (a Cholesky
// factor
// T : a vector of size (n*(n-1))/2, the value are in ]0 +%pi]
// H : a vector of size n with positive values
//R is decomposed as S*diag(H) where S is an upper triangular
//matrix such as the 
//  S(1,1)=1 
//  and ith column of S(1:i,i)  for i>1 is given by
//S(1,i)   = cos(T(j))
//S(2,i)   = sin(T(j))*cos(T(j+1))
//S(3,i)   = sin(T(j))*sin(T(j+1))*cos(T(j+2))
// ...
//S(i-1,i) = sin(T(j))*sin(T(j+1))*...*sin(T(j+i-3))*cos(T(j+i-2))
//S(i,i)   = sin(T(j))*sin(T(j+1))*...*sin(T(j+i-3))*sin(T(j+i-2))
//where j=i*(i-1)/2  
  if argn(2)==1 then //Cholesky factor->spherical parametrization
    R=T;
    n=size(R,1)
    T=zeros((n*(n-1))/2)
    H=zeros(n,1);
    for k=1:n, H(k)=norm(R(1:k,k));end
    j=1;
    for k=2:n
      c=1;
      for i=k:-1:2
        T(j-2+i)=atan(R(i,k)/c,R(i-1,k));
        c=cos(T(j-2+i))
        
      end
      j=j+k-1
    end
  else//spherical parameters -> Cholesky factor
    if argn(1)<>1 then
      error(msprintf(_("%s: Wrong number of output argument: %d expected.\n"),...
                     "sphericalParametrization",1))
    end
    n=size(H,'*')
    R=zeros(n,n)
    R(1,1)=1;
    j=1;
    for k=2:n
      R(1,k)=cos(T(j))
      s=sin(T(j));
      j=j+1;
      for i=2:k-1
        R(i,k)=s*cos(T(j))
        s=s*sin(T(j));
        j=j+1
      end
      R(k,k)=s;
    end
    T=R*diag(H)
  end
endfunction
