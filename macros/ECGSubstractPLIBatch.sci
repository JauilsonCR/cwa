//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ECGSubstractPLIBatch(filein,f,Threshold,fileout)
//This function remoces power-lin interference from the ECG using
//substraction procedure:
//  Sf=ECGremovePLIBatch(S,fe,f,threshold)
//  f : pli frequency  
//references :
//- "Substraction of 50Hz interference from the electrocardiogram" C.levkov,
//  G. Micov, R.Ivanov and I. K. Daskalov
//  Medical and Biological Engineering and Computing
//  July 1984, Volume 22, Issue 4, pp 371-373
//  http://link.springer.com/article/10.1007%2FBF02442109#page-1
//
//- "Removal of power-line interference from the ECG: a review of the
//  subtraction procedure"  
//  Chavdar Levkov, Georgy Mihov, Ratcho Ivanov,
//  Ivan Daskalov, Ivaylo Christovand Ivan Dotsinsky
//  BioMedical Engineering OnLine  2005,4:50 
//  http://www.biomedical-engineering-online.com/content/pdf/1475-925X-4-50.pdf

  if type(Threshold)<>1|~isreal(Threshold) then
    error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),"ECGSubstractPLIBatch",4))
  end

  in=mopen(filein,'rb');
  if argn(2)<4 then
    [path, fname, extension] = fileparts(filein)
    fileout=strsubst(filein,extension,"_pli.ecgs")
  end
  
  if ascii(mget(9,'c',in))<>"ECGScilab" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"ECGSubstractPLIBatch"))
  end

  t0=mget(6,"s",in);
  Nchannels=mget(1,'i',in);
  Nsamples=mget(1,'i',in);
  fe=mget(1,'f',in);
  if size(Threshold,'*')==1 then
    Threshold=Threshold*ones(Nchannels,1)
  elseif size(Threshold,'*')<>Nchannels then
    mclose(in)
    error(msprintf(_("%s: incompatible input arguments %d and %d\n"),"ECGSubstractPLIBatch",1,3))
  end
  N=fe/f;//number of samples for an interference period
  if N<>round(N) then
    mclose(in)
    error(msprintf(_("%s: Sampling frequency must be a multiple of the PLI frequency\n"),"ECGSubstractPLIBatch"))
  end
  out=mopen(fileout,'wb');
  mput(ascii("ECGScilab"),'c',out)
  mput(t0,'s',out);
  mput(Nchannels,'i',out)
  mput(Nsamples,'i',out)
  mput(fe,'f',out)

  Levkov_f(in,out,fe,f,Threshold,Nchannels,Nsamples)
 
  mclose(in)
  mclose(out)
endfunction

