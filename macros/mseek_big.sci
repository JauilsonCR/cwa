//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function mseek_big(pos,u)
//setting file pointer to pos for huge files pos may be too big to be
//stored in an int, so below the huge absolute positionning is
//translated in a sequence of relative move.
  bsz=2^20;
  n1=floor(pos/bsz);
  if n1==0 then
    mseek(pos,u,"set");
  else
    mseek(bsz,u,"set");
    for k=2:n1
      mseek(bsz,u,"cur");
    end
    mseek(modulo(pos,bsz),u,'cur');
  end 
endfunction
