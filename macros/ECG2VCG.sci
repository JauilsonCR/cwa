//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [vcg,s]=ECG2VCG(ecg,fe)
  nvoies=size(ecg,2);
  ecg=detrendECG(ecg,fe)
  [vcg,s,v]=svd(ecg,'e');
  vcg=vcg(:,1:min(3,nvoies));
  s=diag(s);s=s(1:min(3,nvoies));
  vcg=vcg*diag(s);
endfunction
