// This file is part of the CardioVascular wave analysis toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function %BRA_p(res)
  f     = getfield(1,res)
  ll    = lines()
  t     = []
  for k=2:size(f,"*")
    var=f(k)
    value=res(var)

    sz=size(value,'*')
    if sz(1)<10 then // This line can avoid some stacksize error when field contains a big matrix
      str=sci2exp(value,0)
    else
      str="["+strcat(string(size(value)),"x")+"]"
    end
    t=[t;var+": "+str]
  end
  mprintf("  %s\n",t)
endfunction

