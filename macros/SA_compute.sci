function SA_compute(fig)
  fig_ud=fig.user_data
  signal=fig_ud.signal
  fs=fig_ud.fs
  N=2^nextpow2(round(1/(fig_ud.freq_step)));
  n=size(signal,'*');
  timestep=round(n/N);
  T=[0:timestep:n-1 n];
  if modulo(fig_ud.twl,2)==0 then fig_ud.twl=fig_ud.twl+1,end
  if modulo(fig_ud.fwl,2)==0 then fig_ud.fwl=fig_ud.fwl+1,end
  Ns2=int(N/2)
 
  select fig_ud.method
  case "sp"
    H=window('hm',fig_ud.fwl)
    [SP,T,F] = Ctfrsp(hilbert(signal-mean(signal)),T,N,H);
    Ns2=N/2
  case "spwv"
    H=window('hm',fig_ud.fwl);
    G=window('hm',fig_ud.twl);
    [SP,T,F]=Ctfrspwv(hilbert(signal-mean(signal)),T,N,G,H);
    disp(size(SP))
    Ns2=N
  case "zam"
    H=window('hm',fig_ud.fwl);
    G=window('hm',fig_ud.twl);
    [SP,T,F]=Ctfrzam(hilbert(signal-mean(signal)),T,N,G,H);
    Ns2=N
  case "bj"
    H=window('hm',fig_ud.fwl);
    G=window('hm',fig_ud.twl);
    [SP,T,F]=Ctfrbj(hilbert(signal-mean(signal)),T,N,G,H);
    Ns2=N
  else
    error(_("Invalid method"))
  end
  
  fig_ud.T=T/fs
  fig_ud.F=F(1:Ns2)*fs
  fig_ud.SP=SP(1:Ns2,:)'
  set(fig,"user_data",fig_ud)
endfunction
