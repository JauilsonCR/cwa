//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [S,fe]=getValuesFromEcgsFile(f,indsel,sigsel)
  u=mopen(f,"rb");
  if ascii(mget(9,'c',u))<>"ECGScilab" then
    mclose(u)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"getValuesFromEcgsFile"))
  end
  
  t0=mget(6,"s",u);
  Nchannels=mget(1,'i',u);
  Nsamples=mget(1,'i',u);
  fe=mget(1,'f',u);
  if type(indsel)<>1|~isreal(indsel) then
    mclose(u)
    error(msprintf(_("%s: Wrong type for argument %d: Real vector expected.\n"),"getValuesFromEcgsFile",2))
  end
  if or(indsel<1|indsel>Nsamples) then
      mclose(u)
      error(msprintf(_("%s: Wrong value for input argument #%d: Elements must be in [%d %d].\n"),"getValuesFromEcgsFile",2,1,Nsamples))
  end
  if argn(2)<3|or(size(sigsel)==-1) then 
    sigsel=1:Nchannels;
  else
    if type(sigsel)<>1|~isreal(sigsel) then
      mclose(u)
      error(msprintf(_("%s: Wrong type for argument %d: Real vector expected.\n"),"getValuesFromEcgsFile",3))
    end
    if or(sigsel<1|sigsel>Nchannels) then
      mclose(u)
      error(msprintf(_("%s: Wrong value for input argument #%d: Elements must be in [%d %d].\n"),"getValuesFromEcgsFile",3,1,Nchannels))
    end
  end
  //Boucle sur les blocs
  pos=mtell();
  [indsel,ksel]=gsort(indsel,"g","i")
 
  N=size(indsel,'*');
  winH=mywaitbar("open",_("Processing data acquisition"));
  S=zeros(N,size(sigsel,'*'));
  prev=0;
  for k=1:N
    mseek((indsel(k)-1-prev)*Nchannels*8,u,"cur");
    s=matrix(mget(Nchannels,'d',u),1,Nchannels);
    S(ksel(k),:)=s(sigsel);
    prev=indsel(k);
    mywaitbar("update",k/N,winH);

  end
  mclose(u)
  mywaitbar("close",winH); 
endfunction
