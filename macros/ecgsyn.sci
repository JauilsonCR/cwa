function [s, ipeaks] = ecgsyn(varargin)
// [s, ipeaks] = ecgsyn(sfecg,N,Anoise,hrmean,hrstd,lfhfratio,sfint,ti,ai,bi)
// Produces synthetic ECG with the following outputs:
// s: ECG (mV)
// ipeaks: labels for PQRST peaks: P(1), Q(2), R(3), S(4), T(5)
// A zero lablel is output otherwise ... use R=find(ipeaks==3); 
// to find the R peaks s(R), etc. 
// 
// Operation uses the following parameters (default values in []s):
// sfecg: ECG sampling frequency [256 Hertz]
// N: approximate number of heart beats [256]
// Anoise: Additive uniformly distributed measurement noise [0 mV]
// hrmean: Mean heart rate [60 beats per minute]
// hrstd: Standard deviation of heart rate [1 beat per minute]
// lfhfratio: LF/HF ratio [0.5]
// Order of extrema:            [P    Q   R    S   T   ]
// ti = angles of extrema       [-70 -15  0    15  100 ] degrees
// ai = z-position of extrema   [1.2 -5   30  -7.5 0.75]
// bi = Gaussian width of peaks [0.25 0.1 0.1  0.1 0.4 ]
// Copyright (c) 2003 by Patrick McSharry & Gari Clifford, All Rights Reserved  
// See IEEE Transactions On Biomedical Engineering, 50(3), 289-294, March 2003.
// Contact P. McSharry (patrick@mcsharry.net) or G. Clifford (gari@mit.edu)

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// The original ecgsyn.m file and its dependents are freely available from Physionet - 
// http://www.physionet.org/ - please report any bugs to the authors above.

// set parameter default values
  [sfecg,N,Anoise,hrmean,hrstd,lfhfratio,ti,ai,bi,aresp,fresp]=ecgsyn_par(varargin)

  // adjust extrema parameters for mean heart rate 
  hrfact =  sqrt(hrmean/60);
  hrfact2 = sqrt(hrfact);
  bi = hrfact*bi;
  ti = [hrfact2 hrfact 1 hrfact hrfact2].*ti;


  // define frequency parameters for rr process 
  // flo and fhi correspond to the Mayer waves and respiratory rate respectively
  flo = 0.1;
  fhi = 0.25;
  flostd = 0.01;
  fhistd = 0.01;


  // calculate time scales for rr and total output
  sampfreqrr = 1;
  trr = 1/sampfreqrr; 
  tstep = 1/sfecg;
  rrmean = (60/hrmean);	 
  Nrr = 2^(ceil(log2(N*rrmean/trr)));

  // compute rr process
  rr0 = rrprocess(flo,fhi,flostd,fhistd,lfhfratio,hrmean,hrstd,sampfreqrr,Nrr);
  t0=cumsum(rr0);t0=t0-t0(1);
 
  
  // integrate system 
  x0 = [1;0;0.04];//intial value

  ts=t0(1);
  X=[];
  pars=[2*%pi/rr0(1);aresp;fresp;ti(:);ai(:);bi(:)];
  for k=1:size(t0,'*')-1
     tt=ts:1/sfecg:t0(k+1);
     pars(1)=2*%pi/rr0(k);
     X=[X, ode(x0,ts,tt,list("ecgsyn_ode",pars))];
     ts=tt($)+1/sfecg;
     x0=X(:,$);
  end

  X=X';
  if argn(1)>1 then
    // extract R-peaks times
    ipeaks = detectpeaks(X, ti, sfecg);
  end
  // Scale signal to lie between -0.4 and 1.2 mV
  z = X(:,3);
  zmin = min(z);
  zmax = max(z);
  zrange = zmax - zmin;
  z = (z - zmin)*(1.6)/zrange -0.4;

  // include additive uniformly distributed measurement noise 
  eta = 2*rand(length(z),1)-1;
  s = z + Anoise*eta;
endfunction

function rr = rrprocess(flo, fhi, flostd, fhistd, lfhfratio, hrmean, hrstd, sfrr, n)
  w1 = 2*%pi*flo;
  w2 = 2*%pi*fhi;
  c1 = 2*%pi*flostd;
  c2 = 2*%pi*fhistd;
  sig2 = 1;
  sig1 = lfhfratio;
  rrmean = 60/hrmean;
  rrstd = 60*hrstd/(hrmean*hrmean);

  df = sfrr/n;
  w = [0:n-1]'*2*%pi*df;
  dw1 = w-w1;
  dw2 = w-w2;

  Hw1 = sig1*exp(-0.5*(dw1/c1).^2)/sqrt(2*%pi*c1^2);
  Hw2 = sig2*exp(-0.5*(dw2/c2).^2)/sqrt(2*%pi*c2^2);
  Hw = Hw1 + Hw2;
  Hw0 = [Hw(1:n/2); Hw(n/2:-1:1)];
  Sw = (sfrr/2)*sqrt(Hw0);

  ph0 = 2*%pi*rand(n/2-1,1);
  ph = [ 0; ph0; 0; -ph0($:-1:1) ]; 
  SwC = Sw .* exp(%i*ph);
  x = (1/n)*real(ifft(SwC));

  rr = rrmean + x*(rrstd/stdev(x));
endfunction

function ind = detectpeaks(X, thetap, sfecg)
  N = size(X,1);
  irpeaks = zeros(N,1);

  theta = atan(X(:,2),X(:,1));
  ind0 = zeros(N,1);
  for i=1:N-1
    a = ( (theta(i) <= thetap) & (thetap <= theta(i+1)) );
    j = find(a,1);
    if ~isempty(j) then
      d1 = thetap(j) - theta(i);
      d2 = theta(i+1) - thetap(j);
      if d1 < d2 then
        ind0(i) = j;
      else 
        ind0(i+1) = j;
      end  
    end
  end

  d = ceil(sfecg/64);
  d = max([2 d])
  ind = zeros(N,1);
  z = X(:,3);
  zmin = min(z);
  zmax = max(z);
  zext = [zmin zmax zmin zmax zmin];
  sext = [1 -1 1 -1 1];

  for i=1:5
    clear ind1 Z k vmax imax iext;
    ind1 = find(ind0==i);
    n = length(ind1);
    Z = ones(n,2*d+1)*zext(i)*sext(i);
    for j=-d:d
      k = find( (1 <= ind1+j) & (ind1+j <= N) );
      Z(k,d+j+1) = z(ind1(k)+j)*sext(i);
    end
    [vmax, ivmax] = max(Z,'c');
    iext = ind1 + ivmax'-d-1;
    ind(iext) = i;
  end
endfunction

function [fs,n,anoise,hrmean,hrstd,lfhfratio,ti,ai,bi,aresp,fresp]= ecgsyn_par(args)
//DEFAULT VALUES
  fs = 256;
  n = 256;
  anoise = 0;
  hrmean = 60;
  hrstd = 1;
  lfhfratio = 0.5;
  aresp=0.005;
  fresp=0.25;
  ti = [-70 -15 0 15 100]*%pi/180;
  ai = [1.2 -5   30 -7.5 0.75];
  bi = [0.25 0.1 0.1 0.1 0.4];
  //Check arguments
  if size(args)==0 then return,end
  if modulo(size(args),2)<>0 then 
    error(msprintf(_("%s: Wrong number of input argument(s): An even number expected.\n"),"ecgsyn"))
  end
  for k=1:2:size(args)
    ak=args(k+1);
    if type(ak)<>1|~isreal(ak) then
      error(msprintf_("%s: Wrong type for input argument #%d: Array of reals expected.\n"),"ecgsyn",k+1)
    end
    
    select convstr(args(k))
    case "fs"
      if size(ak,'*')<>1 then
        error(msprintf_("%s: Wrong size for input argument #%d: A real expected.\n"),"ecgsyn",k+1)
      end
      if ak<=0 then
        error(msprintf_("%s: Wrong value for input argument #%d: Must be > %d.\n"),"ecgsyn",k+1,0)
      end
      fs=ak
    case "n"
       if size(ak,'*')<>1 then
        error(msprintf_("%s: Wrong size for input argument #%d: An integer expected.\n"),"ecgsyn",k+1)
      end
      if ak<=0 then
        error(msprintf_("%s: Wrong value for input argument #%d: Must be > %d.\n"),"ecgsyn",k+1,0)
      end
      n=round(ak)
    case "anoise"
      if size(ak,'*')<>1 then
        error(msprintf_("%s: Wrong size for input argument #%d: A real expected.\n"),"ecgsyn",k+1)
      end
      if ak<=0 then
        error(msprintf_("%s: Wrong value for input argument #%d: Must be > %d.\n"),"ecgsyn",k+1,0)
      end
      anoise=ak
    case "hrmean"
      if size(ak,'*')<>1 then
        error(msprintf_("%s: Wrong size for input argument #%d: A real expected.\n"),"ecgsyn",k+1)
      end
      if ak<=0 then
        error(msprintf_("%s: Wrong value for input argument #%d: Must be > %d.\n"),"ecgsyn",k+1,0)
      end
      hrmean=ak
    case "hrstd"
      if size(ak,'*')<>1 then
        error(msprintf_("%s: Wrong size for input argument #%d: A real expected.\n"),"ecgsyn",k+1)
      end
      if ak<=0 then
        error(msprintf_("%s: Wrong value for input argument #%d: Must be > %d.\n"),"ecgsyn",k+1,0)
      end
      hrstd=ak

    case "lfhfratio"
      if size(ak,'*')<>1 then
        error(msprintf_("%s: Wrong size for input argument #%d: A real expected.\n"),"ecgsyn",k+1)
      end
      if ak<=0 then
        error(msprintf_("%s: Wrong value for input argument #%d: Must be > %d.\n"),"ecgsyn",k+1,0)
      end
      lfhfratio=ak;
    case "ti"
      if size(ak,'*')<>5 then
        error(msprintf_("%s: Wrong size for input argument #%d: %d expected.\n"),"ecgsyn",k+1,5)
      end
      ti=matrix(ak,1,-1)
    case "ai"
      if size(ak,'*')<>5 then
        error(msprintf_("%s: Wrong size for input argument #%d: %d expected.\n"),"ecgsyn",k+1,5)
      end
      ai=matrix(ak,1,-1)
    case "bi"
      if size(ak,'*')<>5 then
        error(msprintf_("%s: Wrong size for input argument #%d: %d expected.\n"),"ecgsyn",k+1,5)
      end
      bi=abs(matrix(ak,1,-1))
    case "aresp" 
      if size(ak,'*')<>1 then
        error(msprintf_("%s: Wrong size for input argument #%d: A real expected.\n"),"ecgsyn",k+1)
      end
      if ak<=0 then
        error(msprintf_("%s: Wrong value for input argument #%d: Must be > %d.\n"),"ecgsyn",k+1,0)
      end
      aresp=ak;
     case "fresp" 
      if size(ak,'*')<>1 then
        error(msprintf_("%s: Wrong size for input argument #%d: A real expected.\n"),"ecgsyn",k+1)
      end
      if ak<=0 then
        error(msprintf_("%s: Wrong value for input argument #%d: Must be > %d.\n"),"ecgsyn",k+1,0)
      end
      fresp=ak;
    else
       error(msprintf_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),"ecgsyn",k,"""fs"",""n"",""anoise"",""hrmean"",""hrstd"",""lfhfratio"",""ti"",""ai"",""bi"",""aresp"",""fresp""")
    end
  end
endfunction
