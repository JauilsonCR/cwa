//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function s1=%sciecg_f_sciecg(s1,s2)
  if s1.fs<>s2.fs then
    error(msprintf(_("Sampling frequencies differ\n"),"[a;b]"))
  end
  if size(s1,2)<>size(s2,2) then
    error(msprintf(_("%s: Incompatible number of channels.\n"),"[a;b]"))
  end
  s1.sigs=[s1.sigs; s2.sigs]
endfunction
