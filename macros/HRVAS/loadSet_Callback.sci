function loadSet_Callback()
// load settings
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
  h=ud.h
  f=uigetfile('*',pwd(),'Select HRV Settings File');
  if f<>"" then
    settings=loadSettings(f);
    setSettings(settings);
    ud.settings=settings
    set(c,"user_data","settings")
  end
endfunction 
