function output = loadSettings(f)
// loadSettings: Loads any saved settings. If the file is not present or any settings
// are not present then defaults will be loaded.
  if isfile(f) then
    tmp=load(f);
    output=settings;
  else
    output=HRVAS_DefaultSettings()
  end
  
  ////// Preprocessing //////
  //ArtLocate
  if ~isfield(output,'ArtLocatePer') | isempty(output.ArtLocatePer)
    output.ArtLocatePer=get(h.chkArtLocPer,'value');
  end
  if ~isfield(output,'ArtLocatePerVal') | isempty(output.ArtLocatePerVal)
    output.ArtLocatePerVal=evstr(get(h.txtArtLocPer,'string'));
  end 
  if ~isfield(output,'ArtLocateSD') | isempty(output.ArtLocateSD)
    output.ArtLocateSD=get(h.chkArtLocSD,'value');
  end 
  if ~isfield(output,'ArtLocateSDVal') | isempty(output.ArtLocateSDVal)
    output.ArtLocateSDVal=evstr(get(h.txtArtLocSD,'string'));
  end 
  if ~isfield(output,'ArtLocateMed') | isempty(output.ArtLocateMed)
    output.ArtLocateMed=get(h.chkArtLocMed,'value');
  end
  if ~isfield(output,'ArtLocateMedVal') | isempty(output.ArtLocateMedVal)
    output.ArtLocateMedVal=evstr(get(h.txtArtLocMed,'string'));
  end 
  //ArtReplace
  if ~isfield(output,'ArtReplace') | isempty(output.ArtReplace) then
    for rb=["radioArtReplaceNone","radioArtReplaceMean","radioArtReplaceMed","radioArtReplaceSpline"]
      if h(rb).Enable=="on" then
        output.ArtReplace= h(rb).string
        break;
      end
    end
  end
  if ~isfield(output,'ArtReplaceMeanVal') | isempty(output.ArtReplaceMeanVal)
    output.ArtReplaceMeanVal=evstr(get(h.txtArtReplaceMean,'string'));
  end 
  if ~isfield(output,'ArtReplaceMedVal') | isempty(output.ArtReplaceMedVal)
    output.ArtReplaceMedVal=evstr(get(h.txtArtReplaceMed,'string'));
  end 
  //Detrend
  if ~isfield(output,'Detrend') | isempty(output.Detrend)
    tmp=get(h.listDetrend,'string');
    output.Detrend=tmp(get(h.listDetrend,'value'));
  end
  // //Matlab Smooth: method
  // if ~isfield(output,'SmoothMethod') | isempty(output.SmoothMethod)
  //   tmp=get(h.listSmoothMethod,'string');
  //   output.SmoothMethod=tmp(get(h.listSmoothMethod,'value'));
  // end
  
  // Smooth: LoessAlpha
  if ~isfield(output,'SmoothLoessAlpha') | isempty(output.SmoothLoessAlpha)
    output.SmoothLoessAlpha=evstr(get(h.txtSmoothLoessAlpha,'string'));
  end
  //Smooth: LoessOrder
  if ~isfield(output,'SmoothLoessOrder') | isempty(output.SmoothLoessOrder)
    output.SmoothLoessOrder=evstr(get(h.txtSmoothLoessOrder,'string'));
  end
  // Smooth:SgolayLen
 if ~isfield(output,'SmoothSgolayLen') | isempty(output.SmoothSgolayLen)
    output.SmoothSgolayLen=evstr(get(h.txtSmoothSgolayLen,'string'));
  end
  // Smooth: Degree
  if ~isfield(output,'SmoothSgolayDegree') | isempty(output.SmoothSgolayDegree)
    output.SmoothSgolayDegree=evstr(get(h.txtSmoothSgolayDegree,'string'));
  end
  
  //Poly: order
  if ~isfield(output,'PolyOrder') | isempty(output.PolyOrder)
    output.PolyOrder=get(h.listPoly,'Value');
  end
  //Wavelet: Type
  if ~isfield(output,'WaveletType') | isempty(output.WaveletType)
    tmp=get(h.listWaveletType,'string');
    output.WaveletType(1)=tmp(get(h.listWaveletType,'value'));
  end
  //Wavelet: type2
  if ~isfield(output,'WaveletType2') | isempty(output.WaveletType2)
    output.WaveletType(2)=get(h.txtWaveletType2,'string');
  end
  //Wavelet: Levels
  if ~isfield(output,'WaveletLevels') | isempty(output.WaveletLevels)
    output.WaveletLevels=evstr(get(h.txtWaveletLevels,'string'));
  end
  //Smoothness Priors: fc
  if ~isfield(output,'PriorsFc') | isempty(output.PriorsFc)
    output.PriorsFc=evstr(get(h.txtPriorsFc,'string'));
  end
  
  ////// time //////                
  //pNNx
  if ~isfield(output,'pNNx') | isempty(output.pNNx)
    output.pNNx=evstr(get(h.txtPNNx,'string'));
  end
  //SDNNi
  if ~isfield(output,'SDNNi') | isempty(output.SDNNi)
    output.SDNNi=evstr(get(h.txtSDNNi,'string'));
  end                
  
  ////// Freq Domain //////
  //VLF
  if ~isfield(output,'VLF') | isempty(output.VLF)
    output.VLF(1)=evstr(get(h.txtVLF1,'string'));
    output.VLF(2)=evstr(get(h.txtVLF2,'string'));
  end 
  //LF
  if ~isfield(output,'LF') | isempty(output.LF)
    output.LF(1)=evstr(get(h.txtLF1,'string'));
    output.LF(2)=evstr(get(h.txtLF2,'string'));
  end 
  //HF
  if ~isfield(output,'HF') | isempty(output.HF)
    output.HF(1)=evstr(get(h.txtHF1,'string'));
    output.HF(2)=evstr(get(h.txtHF2,'string'));
  end 
  //Interp
  if ~isfield(output,'Interp') | isempty(output.Interp)
    output.Interp=evstr(get(h.txtInterp,'string'));
  end 
  //Points
  if ~isfield(output,'Points') | isempty(output.Points)
    output.Points=evstr(get(h.txtPoints,'string'));
  end
  //WinWidth
  if ~isfield(output,'WinWidth') | isempty(output.WinWidth)
    output.WinWidth=evstr(get(h.txtWinWidth,'string'));
  end
  //WinOverlap
  if ~isfield(output,'WinOverlap') | isempty(output.WinOverlap)
    output.WinOverlap=evstr(get(h.txtWinOverlap,'string'));
  end
  //AROrder
  if ~isfield(output,'AROrder') | isempty(output.AROrder)
    output.AROrder=evstr(get(h.txtAROrder,'string'));
  end        

  ////// Nonlinear //////                                       
  //m
  if ~isfield(output,'m') | isempty(output.m)
    output.m=evstr(get(h.txtSampEnM,'string'));
  end
  //r
  if ~isfield(output,'r') | isempty(output.r)
    output.r=evstr(get(h.txtSampEnR,'string'));
  end
  //n1, n2
  if ~isfield(output,'nRange') | isempty(output.nRange)
    output.nRange(1)=evstr(get(h.txtDFAn1,'string'));
    output.nRange(2)=evstr(get(h.txtDFAn2,'string'));
    ;
  end

  //breakpoint
  if ~isfield(output,'breakpoint') | isempty(output.breakpoint)
    output.breakpoint=evstr(get(h.txtDFAbp,'string'));
  end
  
  ////// Time-Freq //////
  //winSize
  if ~isfield(output,'tfWinSize') | isempty(output.tfWinSize)
    output.tfWinSize=evstr(get(h.txtTFwinSize,'string'));
  end
  //overlap
  if ~isfield(output,'tfOverlap') | isempty(output.tfOverlap)
    output.tfOverlap=evstr(get(h.txtTFoverlap,'string'));
  end
  //TSW
  if ~isfield(output,'tftsw') | isempty(output.tftsw)
    output.tftsw=evstr(get(h.txtTFTimeSmooth,'string'));
  end
  if ~isfield(output,'tffsw') | isempty(output.tffsw)
    output.tffsw=evstr(get(h.txtTFFreqSmooth,'string'));
  end

endfunction
