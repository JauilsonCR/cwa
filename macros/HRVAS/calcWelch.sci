function [PSD,F]=calcWelch(t,y,windowl,noverlap,nfft,fs)
//calFFT - Calculates the PSD using Welch method.
//
//Inputs:
//Outputs:
  
//Prepare y
  t2 = t(1):1/fs:t($);//time values for interp.
  y=interp(t2,t,y,splin(t,y))'; //cubic spline interpolation
  y=y-mean(y); //remove mean
  //Calculate PSD
  [PSD,F] = pWelch(y,windowl,noverlap,(nfft*2)-1,fs,'onesided');    
endfunction
 
