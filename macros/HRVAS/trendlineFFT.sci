function trendlineFFT()
// function to plot FFT of the trendline. It's purpose is to evaluate
// the freq. response of any detrending methods
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
  h=ud.h
  trend=ud.trend
  if trend<>[] then
    t=trend(:,1);
    y=trend(:,2);
    fs=evstr(get(h.txtInterp,'string'));
    t2 = t(1):1/fs:t($); //time values for interp.
    y2=interp1(t,y,t2,'spline')'; //interpolation
    L=length(y2);
    NFFT = 2^nextpow2(L); // Next power of 2 from length of y
    Y = fft([y2;zeros(NFFT-length(y2),1)])/L;
    f = fs/2*linspace(0,1,NFFT/2+1)';
    cur=gcf();
    scf(max(winsid())+1);
    plot(f,2*abs(Y(1:(NFFT/2+1))),'r')
    title('FFT of Trendline')
    xlabel('Freq (Hz)')
    ylabel('Magnitude')
    scf(cur)
  end
endfunction
