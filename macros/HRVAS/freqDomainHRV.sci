// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com
// 
// This file is part of HRVAS
//
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
function output = freqDomainHRV(ibi,VLF,LF,HF,AR_order,windowl,noverlap,nfft,fs,methods,flagPlot)
//freqDomainHRV - calculates freq domain HRV using FFT, AR, and Lomb-Scargle
//methods
//
//Inputs:    ibi = 2Dim array of time (s) and inter-beat interval (s)
//           AR_order = order of AR model
//           windowl = # of samples in window
//           noverlap = # of samples to overlap
//           fs = cubic spline interpolation rate / resample rate (Hz)
//           nfft = # of points in the frequency axis
//           methods = methods of calculating freqDomain. Default is all 3
//               methhods
//Outputs:   output is a structure containg all HRV. One field for each PSD method
//           Output units include:
//               peakHF,LF,VLF (Hz)
//               aHF,aLF,aVLF (ms^2)
//               pHF,pLF,pVLF (%)
//               nHF,nLF,nVLF (%)
//               PSD (ms^2/Hz)
//               F (Hz)
//Usage:

//check input
  nargin=argn(2)
  if nargin<9
    error('Not enough input arguments!')
  elseif nargin<10
    methods=['welch','ar','lomb'];
    flagPlot=%f;
  elseif nargin<11
    flagPlot=%f;
  end    
  
  flagWelch=%f; flagAR=%f; flagLomb=%f;
  for m=1:size(methods,'*')
    if methods(m)=='welch' then
      flagWelch=%t;
    elseif methods(m)=='ar' then
      flagAR=%t;
    elseif methods(m)=='lomb' then
      flagLomb=%t;
    end
  end 
  
  t=ibi(:,1); //time (s)
  y=ibi(:,2); //ibi (s)     
             
  maxF=fs/2;
             
  //prepare y
  y=detrend(y,'linear');
  y=y-mean(y);
             
  //Welch FFT
  if flagWelch
    [output.welch.psd,output.welch.f]=calcWelch(t,y,windowl,noverlap,nfft,fs);
    output.welch.hrv=calcAreas(output.welch.f,output.welch.psd,VLF,LF,HF);
  else
    output.welch=emptyData(nfft,maxF);
  end
             
  //AR
  if flagAR
    [output.ar.psd,output.ar.f]=calcAR(t,y,fs,nfft,AR_order);
    output.ar.hrv=calcAreas(output.ar.f,output.ar.psd,VLF,LF,HF);
  else
    output.ar=emptyData(nfft,maxF);
  end
             
  //Lomb
  if flagLomb
    [output.lomb.psd,output.lomb.f]=calcLomb(t,y,nfft,maxF);
    output.lomb.hrv=calcAreas(output.lomb.f,output.lomb.psd,VLF,LF,HF,%t);
  else
    output.lomb=emptyData(nfft,maxF);
  end
             
  //plot all three psd
  if flagPlot
    figure;
    h1=subplot(3,1,1);
    plotPSD(h1,output.welch.f,output.welch.psd,VLF,LF,HF,[0 0.6],[]);
    legend('welch')
    h2=subplot(3,1,2);
    plotPSD(h2,output.ar.f,output.ar.psd,VLF,LF,HF,[0 0.6],[]);
    legend('AR')
    h3=subplot(3,1,3);
    plotPSD(h3,output.lomb.f,output.lomb.psd,VLF,LF,HF,[0 0.6],[]);
    legend('Lomb-Scargle')
  end
endfunction






