function plotPSD(aH,F,PSD,VLF,LF,HF,limX,limY,flagVLF,flagLS)
// plotPSD: plots PSD in the given axis handle
//
// Inputs:
//   aH: axis handle to use for plotting
//   T,F,PSD: time, freq, and psd arrays
//   VLF, LF, HF: VLF, LF, HF freq bands
//   limX, limY:
//   flagVLF: (%t/%f) determines if VLF is area is shaded
//   flagLS: set to (%t) if ploting normalized PSD from LS  
  colors.vlf=[.5 .5 1];
  colors.lf=[.7 .5 1];//[.7 .6 1];//[1 1 .5];
  colors.hf=[.5 1 1];
  F=F(:);PSD=PSD(:)
  nargin=argn(2)
  if nargin<10; flagLS=%f; end
  if nargin<9; flagVLF=%t; end                
  if isempty(flagVLF); flagVLF=%t; end
  if isempty(flagLS); flagLS=%f; end
  
  sca(aH);
  fig=gcf();
  if fig.immediate_drawing=="on" then
    fig.immediate_drawing="off"
    id_change=%t
  else
    id_change=%f
  end 
  if ~flagLS //LS PSD units are normalized...don't convert
//    PSD=PSD./(1e6); //convert to s^2/hz or s^2
  end       
  
  // find the indexes corresponding to the VLF, LF, and HF bands
  iVLF= find( (F>=VLF(1)) & (F<VLF(2)) );
  iLF = find( (F>=LF(1)) & (F<LF(2)) );
  iHF = find( (F>=HF(1)) & (F<HF(2)) );
        
  //shade areas under PSD curve
  aH.auto_clear="on"
  area(aH,F(:),PSD(:),'FaceColor',[.8 .8 .8]); //shade everything grey  
  aH.auto_clear="off"
  if flagVLF //shade vlf
    area(aH,F(iVLF(1):iVLF($)+1),PSD(iVLF(1):iVLF($)+1),...
         'FaceColor',colors.vlf);
  end
  //shade lf
  area(aH,F(iLF(1):iLF($)+1),PSD(iLF(1):iLF($)+1),'FaceColor',colors.lf);
  //shade hf
  if (iHF($)+1)>size(PSD,1)
    area(aH,F(iHF(1):iHF($)),PSD(iHF(1):iHF($)),'FaceColor',colors.hf);         
  else
    area(aH,F(iHF(1):iHF($)+1),PSD(iHF(1):iHF($)+1),'FaceColor',colors.hf);
  end
  limX=[0 (HF($)*1.1)];
  //set axes limits
  if isempty(limX)
    dx=(max(F)-min(F))*0.01;
    limX=[0 max(F)+dx]
  end
  if isempty(limY)
    dy=(max(PSD)-min(PSD))*0.01;
    limY=[0 max(PSD)+dy]
  end
  aH.data_bounds=[limX(:) limY(:)];
  if id_change then fig.immediate_drawing="on";end
        //set event to copy fig on dblclick
         //set(aH,'ButtonDownFcn',"copyAxes");
endfunction
