function menu_exportHRV()
// Export HRV
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
 
  if ud.HRV<>[] then
    outfile=uiputfile('*.xlsx',pwd(),'Save HRV As');
    if outfile<>"" then
      exportHRV(outfile,ud.HRV,ud.settings)
    end
  end    
endfunction
