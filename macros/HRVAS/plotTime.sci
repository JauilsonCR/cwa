function plotTime(h,ibi,opt)
// plotTIme: plots time-doman related figures
  ibi=ibi(:,2);
  hr=60 ./ ibi
  //calculate number of bins to use in histogram    
  //  dt=mx-mn
  //  binWidth=1/128*1000; 
  // 1/128 seconds is recomended bin width for humans. 
  // Reference: (1996) Heart rate variability: standards of 
  // measurement, physiological interpretation and clinical use.        
  //    nBins=round(dt/binWidth);
  
  //temporay overide of number of bins.
  nBins=32;
  
  //plot histogram of ibi
  sca(h.axesHistIBI);
  mn=min(ibi); mx=max(ibi);
  x=linspace(mn,mx,nBins+1)
  histplot(x,ibi,normalization=%f);hHist=gce();//plot    
  h.axesHistIBI.data_bounds(:,1)=[mn;mx];
  hHist.children.fill_mode="on";
  hHist.children.background=addcolor([.5 .5 .9]);
  h.axesHistIBI.tight_limits='on';//set axis limits
 
  //plot histogram of bpm
  sca(h.axesHistBPM)
  mn=min(hr); mx=max(hr);
  x=linspace(mn,mx,nBins+1)
  histplot(x,hr,normalization=%f);hHist=gce();//plot 
  h.axesHistBPM.data_bounds(:,1)=[mn;mx];
  hHist.children.fill_mode="on";
  hHist.children.background=addcolor([.5 .5 .9]);
  h.axesHistBPM.tight_limits='on';//set axis limits

endfunction
