// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com
// 
// This file is part of HRVAS
//
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
function output = timeFreqHRV(ibi,nibi,VLF,LF,HF,AR_order,winSize,overlap,nfft,fs,tftsw,tffsw,methods)
//timeFreqHRV - calculates time-freq HRV using ar, lomb, and CWT methods
//
// Inputs:   ibi = 2Dim array of [time (s) ,inter-beat interval (s)]
//           nibi = IBI with trend still present (non-detrended). Used for CWT.
//           VLF,LF,HF = arrays containing limits of VLF, LF, and HF freq. bands
//           winSize = # of samples in window
//           noverlap = # of samples to overlap
//           fs = cubic spline interpolation rate / resample rate (Hz)
//           nfft = # of points in the frequency axis
//           methods = cell array containing UP TO three strings that tell
//           the function what methods to include in calculating HRV
//           {'ar','lomb','wavelet'}
// Outputs:  output is a structure containg all HRV. One field for each PSD method
//           Output units include:
//               peakHF,peakLF,peakVLF (Hz)
//               aHF,aLF,aVLF (ms^2)
//               pHF,pLF,pVLF (%)
//               nHF,nLF,nVLF (%)
//               lfhf,rlfhf
//               PSD (ms^2/Hz)
//               F (Hz)
//               T (s)
// Usage:   n/a     


//check input
  nargin=argn(2)
  if nargin<10
    error('Not enough input arguments!')
  elseif nargin<11
    methods=['ar','lomb','wavelet' ,'spwv'];
  end    
  flagSPWV=or(methods=='spwv')
  flagAR=or(methods=='ar')
  flagLomb=or(methods=='lomb')
  flagWelch=%f; 
  flagWavelet=or(methods=='wavelet')
  
  //assumes ibi units are seconds
  t=ibi(:,1); //time (s)
  y=ibi(:,2); //ibi (s)     

  clear ibi; //don't need it anymore
  
  maxF=fs/2;
  
  //AR

  if flagAR
    [output.ar.psd,output.ar.f,output.ar.t]= calcAR_TF(t,y,fs,nfft,AR_order,winSize,overlap);
    output.ar.hrv=calcHRV(output.ar.f,output.ar.psd,VLF,LF,HF);
    //global psd
    output.ar.global.f=output.ar.f;
    globalPSD=mean(output.ar.psd,2);
    output.ar.global.psd=globalPSD;
    output.ar.global.hrv=calcAreas(output.ar.global.f,globalPSD,VLF,LF,HF);
  else
    output.ar=emptyData_TF(t,nfft,maxF);
  end
  
  
  //Lomb
  
  
  if flagLomb
    output.lomb.t=t;
    [output.lomb.psd,output.lomb.f,output.lomb.t]= ...
        calcLomb_TF(t,y,nfft,maxF,winSize,overlap);
    output.lomb.hrv=calcHRV(output.lomb.f,output.lomb.psd,VLF,LF,HF);
    //global psd
    output.lomb.global.f=output.lomb.f;
    globalPSD=mean(output.lomb.psd,2);
    output.lomb.global.psd=globalPSD;
    output.lomb.global.hrv=calcAreas(output.lomb.global.f,globalPSD,VLF,LF,HF);
  else
    output.lomb=emptyData_TF(t,nfft,maxF);
  end    

  //Wavelet
  if flagWavelet
    //y=nibi(:,2);
    clear nibi; // don't need it anymore
    t2 = t(1):1/fs:t($); //time values for interp.
    y2=interp1(t,y,t2,'spline')'; //cubic spline interpolation
      
    [power,f,Scale,Cdelta,n,dj,dt,var]=calcWavelet_TF(y2,fs);
    output.wav.t=t2;
    output.wav.psd=power;
    output.wav.f=f;
    var=variance(y2);
    n=length(y2);
    output.wav.hrv=calcWavHRV(f,power,Scale,Cdelta,var,n,dj,dt,VLF,LF,HF);
    
    // Global wavelet power spectrum
    global_ws=var*(sum(power,2)/n);
    output.wav.global.psd=global_ws;
    output.wav.global.f=f;
    output.wav.global.hrv=calcAreas(f,global_ws,VLF,LF,HF);
  else
    output.wav=emptyData_TF(t,nfft,maxF);
  end
  
  //Smoothed Pseudo Wigner Ville
  if flagSPWV&exists("Ctfrspwv")==1 then
    t2 = t(1):1/fs:t($); //time values for interp.
    y2=interp1(t,y,t2,'spline')'; //cubic spline interpolation
     
    N=size(t2,'*')
    nG=round(tftsw*N/100),if modulo(nG,2)==0 then nG=nG+1;end
    nH=round(tffsw*N/100),if modulo(nH,2)==0 then nH=nH+1;end
    
    G=window("hm",nG)
    H=window("hm",nH)
    [output.spwv.psd,output.spwv.t,output.spwv.f]=Ctfrspwv(hilbert(y2-mean(y2)),...
                                                  t(1):winSize-overlap:t($),nfft,G,H);
    

    disp([min(output.spwv.psd) max(output.spwv.psd)])
    //in some cases Ctfrspwv returns negative values 
    output.spwv.psd=output.spwv.psd.^2;
    //output.spwv.psd=max(output.spwv.psd,%eps);
    output.spwv.psd=output.spwv.psd/fs;
    output.spwv.f=fs*output.spwv.f
    output.spwv.hrv=calcHRV(output.spwv.f,output.spwv.psd,VLF,LF,HF);
    output.spwv.global.f=output.spwv.f;
    globalPSD=mean(output.spwv.psd,2);
    output.spwv.global.psd=globalPSD;

    output.spwv.global.hrv=calcAreas(output.spwv.global.f,globalPSD,VLF,LF,HF);
  else
    output.spwv=emptyData_TF(t,nfft,maxF);
  end
 
endfunction
