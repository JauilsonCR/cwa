function [output,ok] = getSettings()
// getSettings: gets current analysis settings from GUI  
  ok=%t
  //output.file=stripblanks(get(h.txtFile,'string'));
  //preprocessing
  //artifact detection
  output.ArtLocatePer=get(h.chkArtLocPer,'value');


  v=isvalid(h.txtArtLocPer,0,100,"real");
  output.ArtLocatePerVal=v;
  output.ArtLocateSD=get(h.chkArtLocSD,'value');
  v=isvalid(h.txtArtLocSD,0,%inf,"real");
  output.ArtLocateSDVal=v;
  output.ArtLocateMed=get(h.chkArtLocMed,'value');
  v=isvalid(h.txtArtLocMed,0,%inf,"real");
  output.ArtLocateMedVal=v;
  //artifact correction
  for rb=["radioArtReplaceNone","radioArtReplaceMean","radioArtReplaceMed","radioArtReplaceSpline"]
    if h(rb).Enable=="on" then
      output.ArtReplace= h(rb).string
      break;
    end
  end
  
  v=isvalid(h.txtArtReplaceMean,3,%inf,"odd");
  output.ArtReplaceMeanVal=v;
  v=isvalid(h.txtArtReplaceMed,3,%inf,"odd");        
  output.ArtReplaceMedVal=v;
  //detrending
  tmp=get(h.listDetrend,'string');
  output.Detrend=tmp(get(h.listDetrend,'value'));
  v=isvalid(h.txtSmoothLoessAlpha,%eps,100,"real");//?
  output.SmoothLoessAlpha=v;
  v=isvalid(h.txtSmoothLoessOrder,1,%inf,"int");
  output.SmoothLoessOrder=v;
  
  v=isvalid(h.txtSmoothSgolayDegree,1,%inf,"int");
  output.SmoothSgolayDegree=v;
  v=isvalid(h.txtSmoothSgolayLen,output.SmoothSgolayDegree+1,%inf,"int");
  output.SmoothSgolayLen=v;

  output.PolyOrder=get(h.listPoly,'value');
  tmp=get(h.listWaveletType,'string');
  output.WaveletType(1)=tmp(get(h.listWaveletType,'value'));
  select output.WaveletType
  case "db" then
    v=isvalid(h.txtWaveletType2,1,20,"int");
  case "sym"
    v=isvalid(h.txtWaveletType2,2,20,"int");
  case "coif"
    v=isvalid(h.txtWaveletType2,1,5,"int");
  case "leg"
    v=isvalid(h.txtWaveletType2,1,9,"int");
  end
  output.WaveletType(2)=string(v);
  v=isvalid(h.txtWaveletLevels,1,%inf,"int");
  output.WaveletLevels=v;
  v=isvalid(h.txtPriorsFc,0,0.5,"real");     
  output.PriorsFc=v;
  //time
  v=isvalid(h.txtPNNx,0,%inf,"real");
  output.pNNx=v;
  v=isvalid(h.txtSDNNi,0,%inf,"real");                
  output.SDNNi=v;
  //freq
  v=isvalid(h.txtVLF1,0,%inf,"real");
  output.VLF(1)=v;
  v=isvalid(h.txtVLF2,0,%inf,"real");
  output.VLF(2)=v;
  v=isvalid(h.txtLF1,0,%inf,"real");
  output.LF(1)=v;
  v=isvalid(h.txtLF2,0,%inf,"real");
  output.LF(2)=v;
  v=isvalid(h.txtHF1,0,%inf,"real");
  output.HF(1)=v;
  v=isvalid(h.txtHF2,0,%inf,"real");
  output.HF(2)=v;
  v=isvalid(h.txtInterp,0,%inf,"real");
  output.Interp=v;
  v=isvalid(h.txtPoints,1,%inf,"int");
  output.Points=v;
  v=isvalid(h.txtWinWidth,1,%inf,"int");
  output.WinWidth=v;
  v=isvalid(h.txtWinOverlap,1,output.WinWidth,"int");
  output.WinOverlap=v;
  v=isvalid(h.txtAROrder,1,%inf,"int");        
  output.AROrder=v;
  //nonlinear
  v=isvalid(h.txtSampEnM,1,%inf,"int");
  output.m=v;
  v=isvalid(h.txtSampEnR,0,%inf,"real");
  output.r=v;
  v=isvalid(h.txtDFAn1,1,%inf,"int");
  output.nRange(1)=v;
  v=isvalid(h.txtDFAn2,2,%inf,"int");
  output.nRange(2)=v;
  v=isvalid(h.txtDFAbp,output.nRange(1)+1,output.nRange(2)-1,"int");
  output.breakpoint=v;
  //time-freq
  v=isvalid(h.txtTFwinSize,0,%inf,"real");
  output.tfWinSize=v;
  v=isvalid(h.txtTFoverlap,0,%inf,"real");
  output.tfOverlap=v;
  
  v=isvalid(h.txtTFTimeSmooth,0,100,"real");
  output.tftsw=v;
  v=isvalid(h.txtTFFreqSmooth,0,100,"real");
  output.tffsw=v;
  
endfunction
