function graph_open(axname)
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
  h=ud.h 
  ax=h(axname)
  f=scf(max(winsid())+1)
  f.color_map=c.color_map
  nax=gca();
  props=["axes_visible","axes_reverse","grid","grid_position","grid_thickness",...
         "grid_style","x_location","y_location",...
         "rotation_angles","view","cube_scaling" ...
         "x_ticks", "y_ticks", "z_ticks", "auto_ticks", "box","filled","font_style",...
         "font_size","font_color","fractional_font","isoview","log_flags","tight_limits",...
         "data_bounds"];
  for p=props
    nax(p)=ax(p)
  end
  lbl=["title","x_label","y_label","z_label"]
  props=["visible","text","font_foreground","foreground","background","fill_mode","font_style","font_size","fractional_font","font_angle","position","auto_position","auto_rotation"];
  for l=lbl
    nax(l).text=ax(l).text
     for p=props
       nax(l)(p)=ax(l)(p);
     end
  end
  for k=size(ax.children,'*'):-1:1
    e=copy(ax.children(k),nax);e.visible="on";
  end
endfunction

  
