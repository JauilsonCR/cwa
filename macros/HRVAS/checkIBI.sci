function ibi=checkIBI(ibi)
  //check ibi dimentions
  if or(size(ibi)==1) then
    ibi=ibi(:)
    ibi=[[0;cumsum(ibi(1:$-1))],ibi];
  else
    if size(ibi,1)<size(ibi,2) then ibi=ibi';end
    if size(ibi,2)<>2 then
       messagebox('Invalid data in file: '+f,"modal")
       ibi=[]
       return
    end
  end
  if mean(ibi(:,2))>100 then //convert to seconds
    ibi=ibi/1e3
  end
endfunction
