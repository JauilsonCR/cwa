// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com 
// The original version of this file is part of HRVAS
// Port and adpatation for Scilab: Serge Steer, INRIA
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License.
// If not, see <http://www.gnu.org/licenses/>.
function output = timeDomainHRV(t,RR,win,xx)
//timeDomainHRV: calculates time-domain hrv of RR interval series
//"Heart Rate Variability Standards of Measurement, Physiological
//   Interpretation, and Clinical Use"
//http://circ.ahajournals.org/content/93/5/1043.full
// RR = RR array
// win = window size to use for sdnni (s) default 5*60
// xx = value to use for NNx and pNNx (ms) default 50

  if argn(2)<4 then xx=50;end //50 ms
  if argn(2)<3 then win=5*60;end //5 mn

    //check inputs
    RR=RR(:)*1000; //convert RR from s to ms
   
    dRR=diff(RR);
    NNx=nansum(abs(dRR)>xx);
    SDSD=nanstdev(dRR)    
    hr=60.0 ./ (RR/1000); //heart rate

    sd=0.5*SDSD^2;
 
    //GEOMETRIC HRV
    
    //calculate number of bins to use in histogram    
    dt=max(RR)-min(RR);
    binWidth=1/128*1000; //1/128 seconds. Reference: (1996) Heart rate variability: standards of measurement, physiological interpretation and clinical use.        
    nBins=round(dt/binWidth);
    nBins=32;
    output=tlist(["tda","max","min","mean","median","SDNN","sdHR","SDANN","NNx","pNNx",...
                   "RMSSD","SDNNi","SDSD","SD1","SD2","HRVTi","TINN"],...
                  max(RR),...
                  min(RR),...
                  nanmean(RR),...
                  nanmedian(RR),...
                  nanstdev(RR),...//SDNN
                  nanstdev(60000/RR),...//sdHR
                  SDANN(t,RR,win*1000),...
                  NNx,...
                  100*NNx/(size(~isnan(dRR),'*')-1),...//pNNx
                  stdev(abs(dRR(~isnan(dRR))),"*",0),...//RMSSD
                  SDNNi(t,RR,win*1000),...
                  SDSD,... //SDSD
                  sqrt(sd),... //SD1
                  sqrt(2*nanstdev(RR)^2-sd),... //SD2
                  hrvti(RR,nBins),...
                  tinn(RR,nBins));
endfunction

function output = SDANN(t,RR,win)
//SDANN: SDANN index is the std of all the mean NN intervals from each 
//segment of lenght t.
  ksplit=find(diff(modulo(t,win))<0)
  if ksplit==[] then
    output=0
  else
    ksplit=ksplit+1;
    i1=1;
    tmp=zeros(ksplit);
    for i2=1:length(ksplit)
      tmp(i2)=nanmean(RR(i1:ksplit(i2)));
      i1=ksplit(i2)+1;
    end
    output=stdev(tmp);
  end
endfunction

function output = SDNNi(t,RR,win)
//SDNNi: SDNN index is the mean of all the standard deviations of
//NN (normal RR) intervals for all windows of length t.
  ksplit=find(diff(modulo(t,win))<0); 
  if ksplit==[] then
    output=0
  else
    ksplit=ksplit+1
    i1=1
    tmp=zeros(ksplit);
    for i2=1:length(ksplit)
      tmp(i2)=nanstdev(RR(i1:ksplit(i2)));
      i1=ksplit(i2)+1;
    end
    output=mean(tmp);
  end
endfunction

function output=hrvti(RR,nbin)
//hrvti: HRV triangular index    

  xout=linspace(min(RR),max(RR),nbin+1)
  [i,n]=dsearch(RR(~isnan(RR)),xout)
  output=length(RR(~isnan(RR)))/max(n); //hrv ti
    
endfunction

function output=tinn(RR,nbin)
//tinn: triangular interpolation of NN interval histogram
//Reference: Standards of Measurement, Physiological Interpretation, and Clinical Use
//           Circulation. 1996; 93(5):1043-1065.
  
//calculate histogram of RR using nbin bins
//[nout,xout]=hist(RR,nbin);        
  xout=linspace(min(RR),max(RR),nbin+1)
  [i,D]=dsearch(RR(~isnan(RR)),xout)
  xout=xout+(xout(2)-xout(1))/2
  peaki=find(D==max(D));
  if length(peaki)>1
    peaki=round(mean(peaki));
  end

  mn=%inf;I=[];
  for m=(peaki-1):-1:1
    for n=(peaki+1):nbin
      //define triangle that fits the histogram
      q=zeros(1,length(D));            
      q(m:peaki)=linspace(0,D(peaki),peaki-m+1);
      q(peaki:n)=linspace(D(peaki),0,n-peaki+1);
      //integrate squared difference
      q=(D-q).^2;s= sum(q(1:$-1)+q(2:$))/2
      if s<mn then mn=s;I=[m,n];end
    end
  end
  //calculate TINN in (ms)
  output=abs(xout(I(2))-xout(I(1)));
endfunction
