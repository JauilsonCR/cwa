function plotWaterfall(aH,T,F,PSD,VLF,LF,HF,plotType,flagWavelet)
// plotWaterfall: creates a waterfall plot of consecutive PSD
//
// Inputs:
//   aH: axis handle to use for plotting
//   T,F,PSD: time, freq, and psd arrays
//   VLF, LF, HF: VLF, LF, HF freq bands
//   plotType: type of plot to produce (waterfall or surf)
//   flagWavelet: (%t/%f) determines if psd is from wavelet
//   transform. Wavelet power spectrum requires log scale
  nargin=argn(2)
  if (nargin < 9), flagWavelet=%f; end
  if (nargin < 8), plotType = 'surf'; end
  if flagWavelet; F=1.0./F; end //convert to period, see wavelet code for reason
  
  cla(aH)        
  //PSD=PSD./(1000^2); //convert to s^2/hz or s^2
  aH.auto_clear="on",
 
  //plot waterfall
  sca(aH)
  if flagWavelet; F=log2(F); PSD=PSD./1e6;end
  xlim=[min(T) max(T)];
  mn=min(PSD);mx=max(PSD)
  zlim=[mn mx];
  ylim=[min(F) max(F)];
 
  [T,F,PSD] = genfac3d(T,F,PSD');

  plot3d(T,F,list(PSD,1+(PSD-mn)*(127/(mx-mn))));e=gce();
  clear PSD
  e.hiddencolor=0; // to avoid painting the hidden facets
  e.color_flag=4; 
  e.cdata_mapping = "direct"
  e.color_mode=-1;
  aH.box='off';aH.grid=[1 1 1]*addcolor([0.8 0.8 0.8])
  aH.cube_scaling="on";
  aH.auto_clear="off",

  //determin axes limits
  xrange=abs(xlim(2)-xlim(1)); dx=0.01*xrange;
  xlim=xlim+[-2*dx dx];  
  if ~flagWavelet
    ylim=[0 (HF($)*1.1)];
  end 
  zrange=abs(zlim(2)-zlim(1)); dz=0.01*zrange;
  zlim=zlim+[-dz,dz]; 
  
  //draw lines for vlf, lf, and hf bands along bottom
  x=[xlim(1);xlim(2)];x=[x,x,x];
  y=[VLF(2),LF(2),HF(2)];y=[y;y];
  z=zlim(1); z=[z,z,z;z,z,z];
  if flagWavelet; y=log2(1.0./y); end //log2 of period
  xsegs(x,y,z,[-1 -1 -1]);set(gce(),"thickness",2.5);
  
  //draw vert lines for vlf, lf, and hf bands along back
  x=[xlim(2);xlim(2)];x=[x,x,x];
  y=[VLF(2),LF(2),HF(2)];y=[y;y];
  z=zlim'; z=[z,z,z];
  if flagWavelet
    y=log2(1.0./y); //log2 of period

  end        
  xsegs(x,y,z,[-1 -1 -1]);set(gce(),"thickness",2.5);
  aH.rotation_angles =[50 370];
  aH.data_bounds=[xlim(:),ylim(:),zlim(:)]
  aH.axes_reverse=["on" "off" "off"];

endfunction
