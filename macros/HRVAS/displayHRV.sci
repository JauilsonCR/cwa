function displayHRV(h,ibi,dibi,hrv,opt)                       
// displayHRV: displays all HRV results and figures
  
  if isempty(hrv) //if no hrv data present break function
    return
  end
  
  showStatus('Plotting Results');
  drawlater
  //update hrv results tables
  updateTimeTbl(h,hrv,opt);
  updateFreqTbl(h,hrv,opt);        
  updateNLTbl(h,hrv,opt);
  updateTFTbl(h,hrv,opt);
  
  //plot hrv
  plotTime(h,ibi,opt);   
  plotFreq(h,hrv,opt);
  plotPoincare(h,nIBI,hrv,opt);
  plotNL(h,hrv,opt);
  plotTF(h,hrv,opt);        
  drawnow
  showStatus('');
endfunction
