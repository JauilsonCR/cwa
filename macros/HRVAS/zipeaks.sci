function [pks,locs]=zipeaks(y)
//zippeaks: finds local maxima of input signal y
//Usage:  peak=zipeaks(y);
//Returns 2x(number of maxima) array
//pks = value at maximum
//locs = index value for maximum
//
//Reference:  2009, George Zipfel (Mathworks File Exchange #24797)

//check dimentions
  if isempty(y)
    messagebox('Empty input array',"modal")
    pks=[]; locs=[];
    return
  end
  [rows cols] = size(y);
  if cols==1 & rows>1 //all data in 1st col
    y=y';
  elseif cols==1 & rows==1 
    messagebox('Short input array',"modal")
    pks=[]; locs=[];
    return    
  end         
  
  //Find locations of local maxima
  //yD=1 at maxima, yD=0 otherwise, end point maxima excluded
  N=length(y)-2;
  yD=[0 (sign(sign(y(2:N+1)-y(3:N+2))-sign(y(1:N)-y(2:N+1))-.1)+1) 0];
  //Indices of maxima and corresponding values of y
  Y=yD==1;
  I=1:length(Y);
  locs=I(Y);
  pks=y(Y);
endfunction
