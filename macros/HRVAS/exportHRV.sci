// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com
// Copyright 2015, Serge Steer, INRIA
// This file is part of HRVAS
//
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
function exportHRV(outFile,hrv,opt)
//exportHRV - exports hrv data to an XML worksheet compatible with Excel
//and Libreoffice
//
//INPUTS:
//   outFile: full path and name of output file
//   subjects: cell array of subject names. This will be the header name for
//       subjects
//   hrv: array of hrv structures containing hrv data
//
//   TODO: Include options used in HRV analysis.
//subjects argument removed, subject names assumed to be given by hrv.name

  varIBI=['ibi','outliers'];  //vars for # of ibi and # of artifacts
  varIBIunits=["count" "count"];
  lenIBI=size(varIBI,"*");
  
  // time domain vars
  varT=     ["max","min","mean","median","SDNN","sdHR","SDANN","NNx",  "pNNx","RMSSD","SDNNi","SDSD","SD1","SD2","HRVTi","TINN"];
  varTunits=["ms", "ms", "ms",  "ms",    "ms",  "bpm", "ms",   "count","%",   "ms",   "ms",   "ms",  "ms", "ms", "ms",   "ms"];
  //[w,k]=intersect(varT,fieldnames(hrv(1).time));k=gsort(k,"g","i")
  //varT=varT(k);varTunits=varTunits(k)
  lenT=size(varT,'*');
  
  // freq domain vars
  varFreqMethod=fieldnames(hrv(1).freq);
  varF=     ["aVLF","aLF", "aHF", "aTotal","pVLF","pLF","pHF","nLF","nHF","LFHF","peakVLF","peakLF","peakHF"];
  varFunits=["ms^2","ms^2","ms^2","ms^2",  "%",   "%",  "%",  "%",  "%",  ""     "Hz",     "Hz",    "Hz"];
  lenF=size(varF,'*');
  
 
  // nonlinear vars
  varNL=['sampen','alpha','alpha1','alpha2'];
  varNLunits=["","","",""]
  lenNL=size(varNL,'*');
  
  //time-freq vars
  varTFMethod=fieldnames(hrv(1).tf);
  varTFMethod(varTFMethod=="poincare")=[];
  //varTF=fieldnames(hrv(f).tf.ar.global.hrv);
  varTF=     ["aVLF","aLF", "aHF", "aTotal","pVLF","pLF","pHF","nLF","nHF","LFHF","peakVLF","peakLF","peakHF","rLFHF"];
  varTFunits=["ms^2","ms^2","ms^2","ms^2",  "%",   "%",  "%",  "%",  "%",  "",    "Hz",     "Hz",    "Hz",    ""];
  lenTF=size(varTF,'*');
 
   
  // Textual headers 
  H="HRV Method";
  V="HRV Variables";
  U="Subjects";

  H=[H "IBI Info",""]; V=[V varIBI]; U=[U varIBIunits];
  H=[H "Time Domain",emptystr(1,lenT-1)];V=[V,varT];U=[U,varTunits];
  for fn=varFreqMethod'
    H=[H "Freq Domain: "+fn ,emptystr(1,lenF-1)];
    V=[V varF];
    U=[U varFunits];
  end
 
  H=[H "Nonlinear" "" "" ""];V=[V,varNL];U=[U,varNLunits];
  for fn=varTFMethod'
    H=[H "Time-Freq Domain: "+fn ,emptystr(1,lenTF-1)];
    V=[V,varTF];
    U=[U,varTFunits];
  end
  Ncols=size(H,'*')
  T=[H;V;U];
  
  //Numerical data
  V=[]
  Nrows=size(hrv,'*')
  Names=[]
  for f=1:Nrows
    HRV=hrv(f)
    //subject name
    if HRV.name=="" then 
      Names=[Names;"S"+string(f)];
    else
      Names=[Names;HRV.name];
    end
    //IBI info
    D=[size(HRV.pre.dIBI,1) sum(HRV.pre.art)];      
    //Time domain
    for v=varT,D=[D HRV.time(v)];end
    //Freq domain
    for fn=varFreqMethod'
      Dfreq=HRV.freq(fn).hrv;
      for v=varF,D=[D Dfreq(v)];end
    end
    //Nonlinear
    for v=varNL,
      DTF=HRV.nl;
      if v=="sampen" then
        D=[D DTF(v)($)];
      else
        D=[D coeff(DTF.dfa(v),1)];
      end
    end
    
    //Time frequency
    for fn=varTFMethod'
      DTF=HRV.tf(fn);
      for v=varTF,
        if v=='rLFHF' then
          D=[D DTF.hrv(v)];
        else
           D=[D DTF.global.hrv(v)];
        end
      end
    end
    V=[V;%nan D]
  end
  sheet=mlist(["xlssheet","name","text","value"],"HRVAS",[T;Names emptystr(Nrows,Ncols-1)],[%nan(ones(3,Ncols));V])
  writeXmlExcel(outFile, sheet, "HRVAS", "HRVAS")
  
endfunction

