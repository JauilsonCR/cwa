function closeGUI(src,evnt)
//function to close gui
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  c.visible="off";
  ud=c.user_data;
  h=ud.h
  delete(h.lblStatus)
  saveSettings(ud.settings);                               
  delete(c);
endfunction
