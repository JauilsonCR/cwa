function setSettings(in)
// setSettings: sets the current analysis settings within GUI
//preprocessing
//artifact detection
  set(h.chkArtLocPer,'value',in.ArtLocatePer)
  set(h.txtArtLocPer,'string',string(in.ArtLocatePerVal))
  set(h.chkArtLocSD,'value',in.ArtLocateSD)
  set(h.txtArtLocSD,'string',string(in.ArtLocateSDVal))
  set(h.chkArtLocMed,'value',in.ArtLocateMed)
  set(h.txtArtLocMed,'string',string(in.ArtLocateMedVal))        
  //artifact Correction
  if %f then //AFAIRE
    select in.ArtReplace            
    case 'Mean'
      set(h.radioArtReplaceMean,'value',1)
    case 'Median'
      set(h.radioArtReplaceMed,'value',1)
    case 'Spline'
      set(h.radioArtReplaceSpline,'value',1)
    case 'Remove'
      set(h.radioArtReplaceRem,'value',1)
    else
      set(h.radioArtReplaceNone,'value',1)
    end
  end
  set(h.txtArtReplaceMean,'string',string(in.ArtReplaceMeanVal))
  set(h.txtArtReplaceMed,'string',string(in.ArtReplaceMedVal))        
  //Detrending        
  tmp=get(h.listDetrend,'string');
  i=find(tmp==settings.Detrend);
  set(h.listDetrend,'value',i)
//  tmp=get(h.listSmoothMethod,'string');
//  i=find(tmp==settings.SmoothMethod);
//  set(h.listSmoothMethod,'value',i)
  set(h.txtSmoothLoessAlpha,'string',string(in.SmoothLoessAlpha))
  set(h.txtSmoothLoessOrder,'string',string(in.SmoothLoessOrder))   
  
  set(h.txtSmoothSgolayLen,'string',string(in.SmoothSgolayLen))
  set(h.txtSmoothSgolayDegree,'string',string(in.SmoothSgolayDegree))   
  
  set(h.listPoly,'value',in.PolyOrder);
  tmp=get(h.listWaveletType,'string');
  i=find(tmp==settings.WaveletType(1));
  set(h.listWaveletType,'value',i)

  set(h.txtWaveletType2,'string',string(in.WaveletType(2)))
  set(h.txtWaveletLevels,'string',string(in.WaveletLevels))
  set(h.txtPriorsFc,'string',string(in.PriorsFc))
  showDetrendOptions();
  
  //time
  set(h.txtPNNx,'string',string(in.pNNx))
  set(h.txtSDNNi,'string',string(in.SDNNi))              
  //freq
  set(h.txtVLF1,'string',string(in.VLF(1)))
  set(h.txtVLF2,'string',string(in.VLF(2)))
  set(h.txtLF1,'string',string(in.LF(1)))
  set(h.txtLF2,'string',string(in.LF(2)))
  set(h.txtHF1,'string',string(in.HF(1)))
  set(h.txtHF2,'string',string(in.HF(2)))
  set(h.txtInterp,'string',string(in.Interp))
  set(h.txtPoints,'string',string(in.Points))
  set(h.txtWinWidth,'string',string(in.WinWidth))
  set(h.txtWinOverlap,'string',string(in.WinOverlap))
  set(h.txtAROrder,'string',string(in.AROrder))        
  //nonlinear
  set(h.txtSampEnM,'string',string(in.m));
  set(h.txtSampEnR,'string',string(in.r));
  set(h.txtDFAn1,'string',string(in.nRange(1)));
  set(h.txtDFAn2,'string',string(in.nRange(2)));
  set(h.txtDFAbp,'string',string(in.breakpoint));
  //Time-Freq
  set(h.txtTFwinSize,'string',string(in.tfWinSize))
  set(h.txtTFoverlap,'string',string(in.tfOverlap))
  set(h.txtTFTimeSmooth,'string',string(in.tftsw))
  set(h.txtTFFreqSmooth,'string',string(in.tffsw))
endfunction
