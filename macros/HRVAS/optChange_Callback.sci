function optChange_Callback()
// Callback function run when HRV options change
  c=gcbo();
  p=c;while p.type<>"Figure" then p=p.parent;end
  ud=p.user_data;
  h=ud.h
  [settings,ok]=getSettings()
  if ok then 
    ud.settings=settings
    set(p,"user_data",ud)
  end
endfunction
