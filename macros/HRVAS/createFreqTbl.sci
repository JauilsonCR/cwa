//// GUI: CreateFreqTbl
function tH = createFreqTbl(aH)
  x=[0.05 0.33+0.12*(0:4) ];nx=size(x,'*');                                                    
  aH.data_bounds=[0 0;1 1];
  H=0.063
  y=(1-3*H)-(1:12)*H     
  //Horizontal Lines
  xsegs([0 1],[1 1]*(1-2.5*H))
  //Header
   hn=['Frequency','Peak', 'Power', 'Power', 'Power',  'LF/HF'
       'band',     '(Hz)', '(ms^2)','(%)',   '(n.u.)', '(ratio)'];
  for l=1:size(hn,2)
    xstring(x(l),1-2.5*H,hn(:,l))
  end
  //Column 1

  tH=gce();
  Vn=['Welch PSD','Burg PSD','Lomb-Scargle PSD'];
  vn=['VLF' 'LF','HF']
  i=0
  for L=1:size(Vn,'*')
    i=i+1
    xstring(x(1)-0.02,y(i),Vn(L));e=gce();e.font_size=0.8;e.font_style=8;tH(i,1)=e;
    for k=2:nx
      xstring(x(k)-0.02,y(i),"");e=gce();e.font_size=0.8;tH(i,k)=e;
    end
    xstring(x(nx),y(i+1),"0.0");e=gce();e.font_size=0.8;tH(i+1,nx)=e;
    for l=1:size(vn,'*')
      i=i+1
      xstring(x(1)-0.02,y(i),vn(l));e=gce();e.font_size=0.8;tH(i,1)=e;
      for k=2:nx-1
        xstring(x(k),y(i),"0.0");e=gce();e.font_size=0.8;tH(i,k)=e;
      end
    end
  end
  tH(2,5).text=""
  tH(6,5).text=""
  tH(10,5).text=""
  tH(10:12,2).text=""
endfunction
