function output=emptyData_TF(t,nfft,maxF)
//create output structure of zeros
  
//iStart = 1:(winSize-overlap):(N-winSize+1);
  nPSD=1;
  
  //PSD with all zeros
  output.psd=[];
  output.t=[]
  deltaF=maxF/nfft;
  output.f = [];
  
  //HRV with zeros
  for p=1:nPSD      
    //create output structure
    output.hrv.aVLF(p)=0;
    output.hrv.aLF(p)=0;
    output.hrv.aHF(p)=0;   
    output.hrv.aTotal(p)=0;
    output.hrv.pVLF(p)=0;
    output.hrv.pLF(p)=0;
    output.hrv.pHF(p)=0;
    output.hrv.nLF(p)=0;
    output.hrv.nHF(p)=0;
    output.hrv.LFHF(p)=0;
    output.hrv.peakVLF(p)=0;
    output.hrv.peakLF(p)=0;
    output.hrv.peakHF(p)=0;        
  end
  output.hrv.rLFHF=0;

  //global
  output.global.psd=output.psd(:,1);
  output.global.f=output.f;
  output.global.hrv.aVLF=0;
  output.global.hrv.aLF=0;
  output.global.hrv.aHF=0;   
  output.global.hrv.aTotal=0;
  output.global.hrv.pVLF=0;
  output.global.hrv.pLF=0;
  output.global.hrv.pHF=0;
  output.global.hrv.nLF=0;
  output.global.hrv.nHF=0;
  output.global.hrv.LFHF=0;
  output.global.hrv.peakVLF=0;
  output.global.hrv.peakLF=0;
  output.global.hrv.peakHF=0;                

endfunction
