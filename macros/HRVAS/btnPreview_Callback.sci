function btnPreview_Callback()
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
  h=ud.h
  //f=stripblanks(get(h.txtFile,'String'));
  IBI=ud.IBI
  if IBI<>[] then         
    
    [settings,ok]=getSettings(); //get HRV options from gui
    if ~ok then return,end
    //////////////////////////////////////////////////////////////////////////////////////////////
    // LOAD IBI
    //////////////////////////////////////////////////////////////////////////////////////////////
//    showStatus('Loading IBI');        
    nIBI=[]; dIBI=[];
//    IBI=loadIBI(f,settings);
//    if IBI==[] then return;end
    ////////////////////////////////////////////////////////////////////////////////////////////
    //Preprocess Data
    ////////////////////////////////////////////////////////////////////////////////////////////
    showStatus('Preprocessing');

    //build cell array of locate artifacts methods
    methods=[]; methInput=[];
    if settings.ArtLocatePer
      methods=[methods,'percent'];
      methInput=[methInput,settings.ArtLocatePerVal];
    end
    if settings.ArtLocateSD
      methods=[methods,'sd'];
      methInput=[methInput,settings.ArtLocateSDVal];
    end
    if settings.ArtLocateMed
      methods=[methods,'median'];
      methInput=[methInput,settings.ArtLocateMedVal];
    end
    //determine which window/span to use
    if convstr(settings.ArtReplace)=='mean' then
      replaceWin=settings.ArtReplaceMeanVal;
    elseif convstr(settings.ArtReplace)=='median' then 
      replaceWin=settings.ArtReplaceMedVal;
    else
      replaceWin=0;
    end

    //Note: don't need to use all the input arguments. Will let the
    //function handle all inputs
    [dIBI,nIBI,trend,art] = preProcessIBI(IBI, ...
                                          'locateMethod', methods, 'locateInput', methInput, ...
                                          'replaceMethod', settings.ArtReplace, ...
                                          'replaceInput',replaceWin, 'detrendMethod', settings.Detrend, ...
                                          'smoothLoessAlpha', settings.SmoothLoessAlpha, ...
                                          'smoothLoessOrder', settings.SmoothLoessOrder, ...
                                          'smoothSgolayLen', settings.SmoothSgolayLen, ...
                                          'smoothSgolayDegree', settings.SmoothSgolayDegree, ...
                                          'polyOrder', settings.PolyOrder, ...
                                          'waveletType', settings.WaveletType, ...
                                          'waveletLevels', settings.WaveletLevels, ...
                                          'fc', settings.PriorsFc,...
                                          'resampleRate',settings.Interp);
    
    if sum(art)>0
      set(h.lblArtLocate,'string', ...
          msprintf('Ectopic Detection [%.2f%s]',sum(art)/size(IBI,1)*100),"%")
    else
      set(h.lblArtLocate,'string','Ectopic Detection')
    end
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    //Plot IBI
    ////////////////////////////////////////////////////////////////////////////////////////////
    plotIBI(h,settings,IBI,dIBI,nIBI,trend,art);
    
    showStatus('');
    drawnow 
   

    ud.settings=settings;
    ud.art=art;
    //ud.IBI=IBI
    ud.dIBI=dIBI;
    ud.nIBI=nIBI;
    ud.trend=trend;
    set(c,"user_data",ud)
  end
endfunction
