function plotFreq(h,hrv,opt)
// plotFreq: plots freq-domain related figures
  
  k=find(h.btngrpFreqPlot.children.style=="radiobutton");
  k=k(find(h.btngrpFreqPlot.children(k).value==1))
  meth=h.btngrpFreqPlot.children(k).string
 
  if meth=='Welch' then
    psd=hrv.freq.welch.psd;
    f=hrv.freq.welch.f;
    ylbl='PSD (s^2/Hz)';
    flagLS=%f;
  elseif meth=='Burg' then
    psd=hrv.freq.ar.psd;
    f=hrv.freq.ar.f;
    ylbl='PSD (s^2/Hz)';
    flagLS=%f;
  else
    psd=hrv.freq.lomb.psd;
    f=hrv.freq.lomb.f;
    ylbl='PSD (normalized)';
    flagLS=%t;
  end
  //drawlater()
  if f<>[] then
    plotPSD(h.axesFreq,f,psd,opt.VLF,opt.LF,opt.HF,[],[],%t,flagLS);
    xlabel(h.axesFreq,'Freq (Hz)'); 
    ylabel(h.axesFreq,ylbl);
  end
  //drawnow()
endfunction
