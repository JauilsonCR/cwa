
function updateTFTbl(h,hrv,opt)
    // updateTFTbl: updates time-freq results table with hrv data    
        ar=hrv.tf.ar.global.hrv;
        lomb=hrv.tf.lomb.global.hrv;
        wav=hrv.tf.wav.global.hrv;
        spwv=hrv.tf.spwv.global.hrv;

        tH=h.text.tf; //handles of text objects
        
        //column 2            
        set(tH(2,2),'text',msprintf('%0.2f',ar.peakVLF))
        set(tH(3,2),'text',msprintf('%0.2f',ar.peakLF))
        set(tH(4,2),'text',msprintf('%0.2f',ar.peakHF))
        set(tH(6,2),'text',msprintf('%0.2f',lomb.peakVLF))
        set(tH(7,2),'text',msprintf('%0.2f',lomb.peakLF))
        set(tH(8,2),'text',msprintf('%0.2f',lomb.peakHF))
        set(tH(10,2),'text',msprintf('%0.2f',wav.peakVLF))      
        set(tH(11,2),'text',msprintf('%0.2f',wav.peakLF))
        set(tH(12,2),'text',msprintf('%0.2f',wav.peakHF))
        set(tH(14,2),'text',msprintf('%0.2f',spwv.peakVLF))      
        set(tH(15,2),'text',msprintf('%0.2f',spwv.peakLF))
        set(tH(16,2),'text',msprintf('%0.2f',spwv.peakHF))

        //Column 3
        set(tH(2,3),'text',msprintf('%0.1f',ar.aVLF*1E6))
        set(tH(3,3),'text',msprintf('%0.1f',ar.aLF*1E6))
        set(tH(4,3),'text',msprintf('%0.1f',ar.aHF*1E6))
        set(tH(6,3),'text',msprintf('%0.1f',lomb.aVLF*1E6))
        set(tH(7,3),'text',msprintf('%0.1f',lomb.aLF*1E6))
        set(tH(8,3),'text',msprintf('%0.1f',lomb.aHF*1E6))
        set(tH(10,3),'text',msprintf('%0.1f',wav.aVLF*1E6))
        set(tH(11,3),'text',msprintf('%0.1f',wav.aLF*1E6))
        set(tH(12,3),'text',msprintf('%0.1f',wav.aHF*1E6))
                
        set(tH(14,3),'text',msprintf('%0.1f',spwv.aVLF*1E6))
        set(tH(15,3),'text',msprintf('%0.1f',spwv.aLF*1E6))
        set(tH(16,3),'text',msprintf('%0.1f',spwv.aHF*1E6))
     //Column 4
        set(tH(2,4),'text',msprintf('%0.1f',ar.pVLF))
        set(tH(3,4),'text',msprintf('%0.1f',ar.pLF))
        set(tH(4,4),'text',msprintf('%0.1f',ar.pHF))        
        set(tH(6,4),'text',msprintf('%0.1f',lomb.pVLF))
        set(tH(7,4),'text',msprintf('%0.1f',lomb.pLF))
        set(tH(8,4),'text',msprintf('%0.1f',lomb.pHF))        
        set(tH(10,4),'text',msprintf('%0.1f',wav.pVLF))      
        set(tH(11,4),'text',msprintf('%0.1f',wav.pLF))
        set(tH(12,4),'text',msprintf('%0.1f',wav.pHF))
      
        set(tH(14,4),'text',msprintf('%0.1f',spwv.pVLF))      
        set(tH(15,4),'text',msprintf('%0.1f',spwv.pLF))
        set(tH(16,4),'text',msprintf('%0.1f',spwv.pHF))
               
        //Column 5)
        set(tH(3,5),'text',msprintf('%0.3f',ar.nLF))
        set(tH(4,5),'text',msprintf('%0.3f',ar.nHF))        
        set(tH(7,5),'text',msprintf('%0.3f',lomb.nLF))
        set(tH(8,5),'text',msprintf('%0.3f',lomb.nHF))        
        set(tH(11,5),'text',msprintf('%0.3f',wav.nLF))
        set(tH(12,5),'text',msprintf('%0.3f',wav.nHF))
        set(tH(15,5),'text',msprintf('%0.3f',spwv.nLF))
        set(tH(16,5),'text',msprintf('%0.3f',spwv.nHF))
          
        //Column 6
        set(tH(2,6),'text',msprintf('%0.3f',ar.LFHF))
        set(tH(3,6),'text',msprintf('%0.3f',hrv.tf.ar.hrv.rLFHF))
        set(tH(6,6),'text',msprintf('%0.3f',lomb.LFHF))
        set(tH(7,6),'text',msprintf('%0.3f',hrv.tf.lomb.hrv.rLFHF))
        set(tH(10,6),'text',msprintf('%0.3f',wav.LFHF))
        set(tH(11,6),'text',msprintf('%0.3f',hrv.tf.wav.hrv.rLFHF))
        set(tH(14,6),'text',msprintf('%0.3f',spwv.LFHF))
        set(tH(15,6),'text',msprintf('%0.3f',sum(spwv.LFHF>1)/sum(spwv.LFHF<=1)))
        
endfunction
