
function showMenubar(src,evnt)
// function to show/hide menubar    
  global h
  state=convstr(get(h.MainFigure,'menubar_visible'));
  if state=='on' then
    set(h.MainFigure,'menubar_visible','off')
  else
    set(h.MainFigure,'menubar_visible','on')
  end
endfunction
