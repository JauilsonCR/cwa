function plotSpectrogram(aH,T,F,PSD,VLF,LF,HF,limX,limY,flagWavelet) 
// plotSpectrogram: plots a spectrogram in the given axis handle (aH)
//
// Inputs:
//   aH: axis handle to use for plotting
//   T,F,PSD: time, freq, and psd arrays
//   VLF, LF, HF: VLF, LF, HF freq bands
//   plotType: type of plot to produce (mesh, surf, or image)
//   flagWavelet: (%t/%f) determines if psd is from wavelet
//   transform. Wavelet power spectrum requires log scale   
  aCB=h.axesTFCB;
  nargin=argn(2)
  if (nargin < 10), flagWavelet=%f; end    

  if flagWavelet then
    F=log2(1.0./F);  //convert to period, see wavelet code for reason
    PSD=PSD./(1000^2); //convert to s^2/hz or s^2   
  end
  cla(aH)   

  mn=min(PSD);mx=max(PSD)

  e=-floor(log10(mx)-0.5)
  //update colorbar
  aCB.ticks_st(1,2)=10^(e);
  aCB.data_bounds=[0 mn;1 mx];
  if e==0 then
     aCB.y_label.text=""
  else
    aCB.y_label.text="$\times 10^{"+string(-e)+"}$"
  end
  aCB.y_label.position=[0 mx+0.001*(mx-mn)];
  aCB.y_label.font_angle=0;
  aCB.ticks_format(2)="%.0f";

  y = linspace(mn,mx,128)
  col=(1:128);
  sca(aCB)
  Sgrayplot([0 1],y,[col;col],colminmax=[1 128])
  
  //draw PSD
  sca(aH);aH.auto_clear="on";
  //aH.data_bounds=[min(T),min(F);max(T) max(F)];
  Sgrayplot(T,F,PSD',colminmax=[1 128]);
  aH.tight_limits = "on"
  aH.auto_clear="off";
  //draw lines for vlf, lf, and hf bands
  x=[min(T) max(T)]'; x=[x,x,x];
  y=[VLF(2),LF(2),HF(2)]; y=[y;y];
  if flagWavelet
    aH.axes_reverse=["off","on","off"];
    y=log2(1.0./y); //log2 of period
    aH.y_ticks.labels=msprintf("%0.3f\n",2.0.^(-aH.y_ticks.locations));
  else
    aH.axes_reverse=["off","off","off"];
  end 
  xsegs(x,y,[-2 -2 -2])
  
  //axis lables
  xlabel('Time (s)')
  ylabel('F (Hz)')

endfunction
