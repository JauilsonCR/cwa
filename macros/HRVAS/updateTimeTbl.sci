function updateTimeTbl(h,hrv,opt)
// updateTimeTbl: updates time-domain table with hrv data
  hrv=hrv.time;   //time domain hrv
  tH=h.text.time; //handles of text objects
  
  set(tH(1,3),'text',msprintf('%0.1f',hrv.mean))
  set(tH(2,3),'text',msprintf('%0.1f',hrv.SDNN))
  set(tH(3,3),'text',msprintf('%0.1f',60000/hrv.mean))
  set(tH(4,3),'text',msprintf('%0.1f',hrv.sdHR))
  set(tH(5,3),'text',msprintf('%0.1f',hrv.RMSSD))
  set(tH(6,3),'text',msprintf('%0.0f',hrv.NNx))
  set(tH(7,3),'text',msprintf('%0.1f',hrv.pNNx))      
  set(tH(8,3),'text',msprintf('%0.1f',hrv.SDNNi))
  set(tH(10,3),'text',msprintf('%0.1f',hrv.HRVTi))
  set(tH(11,3),'text',msprintf('%0.1f',hrv.TINN))
endfunction
