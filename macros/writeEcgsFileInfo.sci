//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function out=writeEcgsFileInfo(fileout,Nchannels,Nsamples,fe,t0)
  if argn(2)<4 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),"writeEcgsFileInfo",4,5))
  end
  if type(fileout)==10&size(fileout,"*")==1 then
    out=mopen(fileout,"wb");
  elseif type(fileout)==1&size(fileout,"*")==1&isreal(fileout) then
    out=fileout
    mseek(0,out)
  else
    error(msprintf(_("%s: Wrong value for input argument #%d: A valid file descriptor expected.\n"),"writeEcgsFileInfo",1))
  end
  if type(Nchannels)<>1|size(Nchannels,"*")<>1|~isreal(Nchannels)|int(Nchannels)<=0  then
     error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected"),"writeEcgsFileInfo",2))
  end
  if type(Nsamples)<>1|size(Nsamples,"*")<>1|~isreal(Nsamples)|int(Nsamples)<=0  then
     error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected"),"writeEcgsFileInfo",3))
  end
  if type(fe)<>1|size(fe,"*")<>1|~isreal(fe)|fe<=0  then
     error(msprintf(_("%s: Wrong value for input argument #%d: A positive scalar expected"),"writeEcgsFileInfo",4))
  end
  if argn(2)<5 then 
    t0=zeros(1,6);
  elseif type(t0)<>1|size(t0,"*")<>6|~isreal(t0) then
     error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),"writeEcgsFileInfo",5,6))
  end
    
  mput(ascii("ECGScilab"),'c',out)
  mput(t0,'s',out);
  mput(Nchannels,'i',out)
  mput(Nsamples,'i',out)
  mput(fe,'f',out)
endfunction
