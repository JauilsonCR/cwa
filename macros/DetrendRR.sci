function [trend,RRd]=DetrendRR(RR,method,varargin)
//RR: a regularly sampled RR signal
  if argn(2)<2 then
    method="SPA"
    varargin=list(0.02/mean(RR));//a vérifier
  end
  t=cumsum(RR)
 
 
  select method
  case "SPA" //Smoothness Prior Approach
    fc=varargin(1)
    trend=gp_smooth(t,RR-mean(RR),fc)
  case "wavelet" //swt module needed     
    if exists("wavedec")==0|exists("wrcoef")==0 then
      error("The swt module is requested")
    end
    levels=varargin(1)
    wavelettype=varargin(2)
    if size(varargin)==3 then fs=varargin(3);else fs=1/4;end
    [ts,RRs]=ResampleRR(RR,fs)
    meanRR=mean(RRs);
    RRs=RRs-meanRR

  
    // Perform wavelet decomposition
    [c,l] = wavedec(RRs,levels,wavelettype);
    // Reconstruct approximation at lowest freq level
    trend = wrcoef('a',c,l,wavelettype,levels)';  
    trend=interp(t,ts(:),trend,splin(ts(:),trend))
  case "loess"
    alpha=varargin(1)
    if size(varargin)==2 then order=varargin(2);else order=2;end
    trend = loess(t,RR-mean(RR),alpha,order)
  case "polynomial" //linear detrend
    order=varargin(1)
    //center and normalize t for a better conditionning
    t1=(t-mean(t))./stdev(t);
    p= polyfit(t1,RR-mean(RR),order);
    trend = horner(p,t1);
  case "sgolay" then
    len=varargin(1);len=len+1-modulo(len,2)
    if size(varargin)==2 then order=varargin(2);else order=2;end
    trend=sgolayfilt(RR-mean(RR),order,len)
  else
    error(msprintf(_("%s: Wrong value for input argument %s: invalid method %s\n"),"RRDetrend",2,method))
  end
 
  trend=matrix(trend,size(RR))
  RRd=(RR-trend)

endfunction
